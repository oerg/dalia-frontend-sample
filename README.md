# DALIA Frontend

This project is developed by *Learning and Skill Analytics Group* at 
[*Leibniz Information Center for Science and Technology*](https://www.tib.eu/) 
base on the [OER Recommender](https://gitlab.com/oerg/oer-recommender-frontend)
source code.

This project is build based on the requirements of [DALIA](https://dalia.education) project and was showcased 
in the first phase of the project.

### License

This project is published under the [CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/).


## Gallery

<img src="screenshots/DALIA01.png" alt="DALIA"/>
<img src="screenshots/DALIA02.png" alt="DALIA"/>
<img src="screenshots/DALIA03.png" alt="DALIA"/>

## How to use

**Requirements**: You should have [NodeJs 16](https://nodejs.org/en/blog/release/v16.16.0) and [Yarn](https://yarnpkg.com/) installed on your system.

To be able to run and compile please follow these steps:

1. Clone the current repo using `git`
2. Copy `env.sample` to `.env` file and enter the required configurations.
3. Install the dependency packages using `yarn install`

Then you will have the following commands at hand:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**
