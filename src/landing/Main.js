import { Backdrop } from '@material-ui/core';
import React, {useCallback, useContext, useEffect, useRef, useState} from 'react';
import { Route, Switch, useHistory, useLocation, useRouteMatch } from 'react-router-dom';
import { useLanguage } from '../i18n';
import Explore from './components/explore/Explore';
import Footer from "./components/footer/Footer";
import Home from './components/home/Home';
import NavBar from "./components/navigation/NavBar";
import Register from './components/register/Register';
import ChangePasswordDialog from './components/register_login/ChangePasswordDialog';
import LoginDialog from './components/register_login/LoginDialog';
import { TermsOfUse } from './components/tos';
import {UserContext} from "./User";
import {useTranslation} from "react-i18next";

function Main() {
    const match = useRouteMatch();
    const [exploreOpen, setExploreOpen] = useState(false);
    const [selectedItem, setSelectedItem] = useState(null);
    const [loginOpen, setLoginOpen] = useState(false);
    const [forgotPasswordOpen, setForgotPasswordOpen] = useState(false);
    const backdropRef = useRef(null);
    const locationState = useLocation().state;
    const history = useHistory();
    const language = useLanguage();

    const user = useContext(UserContext);
    const { t } = useTranslation(["landing"]);

    const handleGuestLogin = useCallback(
        () => {
            user.guestLogin(response => {
                if (response.logged_in) {
                    history.push(`/${language}/dashboard`)
                } else {
                    if (typeof response === 'object' && !("logged_in" in response))
                        alert(t("There is a problem with logging in. Please try again."))
                }
            })
        },
        [language, user, t]
    )

    // open login if location state has open === true
    useEffect(
        () => {
            if (locationState && locationState.login === true) {
                setLoginOpen(true);
            } else if (locationState && locationState.loginAsGuest === true) {
                handleGuestLogin();
                history.replace(history.location.pathname)
            }
        },
        [ locationState, history ]
    );

    const handleCloseBackdrop = useCallback(
        (e) => {
            if (e.target === backdropRef.current) {
                setExploreOpen(false);
            }
        },
        []
    );

    const handleJobClick = useCallback(
        () => {
            setExploreOpen(true)
            setSelectedItem(0)
        },
        [],
    )

    const handleSkillClick = useCallback(
        () => {
            setExploreOpen(true)
            setSelectedItem(1)
        },
        [],
    )

    const handleTopicClick = useCallback(
        () => {
            setExploreOpen(true)
            setSelectedItem(2)
        },
        [],
    )

    const handleCloseLogin = useCallback(
        () => {
            history.replace(`/${language}/`)
            setLoginOpen(false)
        },
        [history]
    )

    const handleForgotPassword = useCallback(
        () => {
            setLoginOpen(false)
            setForgotPasswordOpen(true)
        },
        []
    )

    const handleCloseForgotPassword = useCallback(
        () => {
            setForgotPasswordOpen(false)
            setLoginOpen(true)
        },
        []
    )

    return (<div style={{
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
        }}>
            <NavBar
                exploreOpen={exploreOpen}
                handleExploreOpen={setExploreOpen}
            />
            <Switch>
                <Route exact path={`${match.url}/register`}>
                    <Register />
                    <Footer />
                </Route>
                <Route path={`${match.url}/tos`}>
                    <TermsOfUse />
                    <Footer />
                </Route>
                {/*<Route path={`${match.url}/aboutus`}>*/}
                {/*    <AboutUs />*/}
                {/*    <Footer />*/}
                {/*</Route>*/}
                {/*<Route path={`${match.url}/events`}>*/}
                {/*    <Events />*/}
                {/*    <Footer />*/}
                {/*</Route>*/}
                <Route exact path={match.url} >
                    <Home
                        exploreOpen={exploreOpen}
                        onExploreOpenChange={setExploreOpen}
                        selectedItem={selectedItem}
                        onJobClick={handleJobClick}
                        onSkillClick={handleSkillClick}
                        onTopicClick={handleTopicClick}
                    />
                    <Footer attribute />
                </Route>
            </Switch>
            <Backdrop
                style={{
                    position: "absolute",
                    zIndex: 99,
                    justifyContent: 'flex-start',
                    paddingTop: 64,
                }}
                ref={backdropRef}
                open={exploreOpen}
                onClick={handleCloseBackdrop}
                unmountOnExit
            >
                <Explore
                    open={exploreOpen}
                    defaultSelectedItem={selectedItem}
                    mt="20px"
                />
            </Backdrop>
            <LoginDialog
                open={loginOpen}
                onClose={handleCloseLogin}
                openChangePasswordDialog={handleForgotPassword}
            />
            <ChangePasswordDialog
                open={forgotPasswordOpen}
                onClose={handleCloseForgotPassword}
            />
        </div>
    )
}

export default Main
