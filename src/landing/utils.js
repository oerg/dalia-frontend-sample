import axios from "axios";
import { requestAddHandlers } from "../dashboard/views/helpers";

export function loadGeneralReport(onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(`/jobs/get-general-reports`),
        onLoad,
        onError,
        onEnd
    );
}