import { Typography } from '@material-ui/core';
import axios from 'axios';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { useParam } from '../../../dashboard/views/helpers';

function UserActivator({ location }) {
    const token = useParam('token');
    const email = useParam('email');
    const history = useHistory();
    const { t } = useTranslation(['landing']);

    const [output, setOutput] = useState(null);

    const activate = useCallback((token, email) => {
        axios.post('/members/activate', { token, email })
            .then(res => {
                if (res.data.done) { // activated
                    setTimeout(() => {
                        history.push(location); // go home
                    }, 3000);
                    setOutput(
                        <Typography variant="h4">
                            {t('Your account has been activated successfully. Please wait a few seconds to be redirected...')}
                        </Typography>
                    )
                } else { // not activated
                    setOutput(
                        <Typography variant="h4">
                            {t("This link is not valid.")}
                        </Typography>
                    )
                }
            })
            .catch(err => {
                setOutput(
                    <Typography variant="h4">
                        {t("This link is not valid.")}
                    </Typography>
                )
            })
    }, [history, location, t])

    useEffect(() => {
        if (token && email) {
            activate(token, email);
        } else {
            history.push("/")
        }
    }, [token, email, activate, history])

    return output;
}

export default UserActivator