import {Box, Grid, TextField, Typography, withStyles} from "@material-ui/core";
import React, {useCallback, useRef} from "react";
import {useTranslation} from "react-i18next";
import useSearch from "../../../dashboard/views/Search/useSearch";

const styles = (theme) => ({
    card: {
        // boxShadow: theme.shadows[4],
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        padding: `${theme.spacing(7)}px ${theme.spacing(3)}px`,
        // backgroundColor: 'rgba(255, 255, 255, 0.9)',
        background: 'transparent',
        // color: '#00006E'
        color: '#EEE',
    },
    image: {
        maxWidth: "100%",
        verticalAlign: "middle",
        borderRadius: theme.shape.borderRadius,
        boxShadow: theme.shadows[4],
    },
    searchText: {
        height: 40,
        fontSize: "1.5em",
        // fontFamily: "Inter",
        textAlign: "center",
        color: "#FFFFFF",
        "&::placeholder": {
            opacity: 1,
        }
    },
    searchBox: {
        border: "1px solid #FFF",
        borderRadius: 5,
        width: 450,
    },
    searchResult: {
        minWidth: 700,
    },
    statsWrapper: {
        textAlign: "center",
        height: "100%",
        fontFamily: "Inter",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    }
});

function HeadSection(props) {
    const { classes } = props;
    const searchRef = useRef(null);
    const [openSearch] = useSearch()

    const { t } = useTranslation(["landing"]);

    const handleSearch = useCallback(
        () => {
            openSearch(
                null,
                {
                    noActions: true,
                },
                false
            )
        },
        [openSearch]
    )

    return (
        <div className={classes.card}>
            <Grid container>
                <Grid item xs={12}>
                    <Box style={{
                        marginTop: 30,
                        display: 'flex',
                        justifyContent: 'center',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}>
                        <img src={`${process.env.PUBLIC_URL}/images/dalia/header.png`} width={500} alt={'DALIA'} />
                        <TextField
                            placeholder={t("Search...")}
                            variant="outlined"
                            ref={searchRef}
                            autoFocus
                            onClick={handleSearch}
                            className={classes.searchBox}
                            inputProps={{
                                readOnly: true,
                                className: classes.searchText,
                            }}
                        />
                        <Typography style={{ width: '100%', textAlign: 'center', fontSize: 31, marginTop: 30 }}>
                            Data Literacy Alliance
                        </Typography>
                    </Box>
                </Grid>
            </Grid>
        </div>
    );
}

export default withStyles(styles, { withTheme: true })(HeadSection);
