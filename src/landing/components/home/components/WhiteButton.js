import { Button, makeStyles } from "@material-ui/core";
import clsx from 'classnames';
import React from 'react';

const useStyles = makeStyles({
    root: {
        color: "white",
        borderColor: "white",
        "&:hover": {
            borderColor: "#CCC",
            color: "#CCC",
        }
    }
})

function WhiteButton({ className, ...props }) {
    const classes = useStyles()

    return <Button
        {...props}
        className={clsx(classes.root, className)}
    />
}

export default WhiteButton
