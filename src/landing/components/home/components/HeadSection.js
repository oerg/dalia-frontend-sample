import { makeStyles } from '@material-ui/core'
import React from 'react'
import SearchBox from '../SearchBox'

const useStyles = makeStyles((theme) => ({
    wrapper: {
        // backgroundImage: "linear-gradient(#d6e5e3, #00006E)",
        backgroundImage: `url("${process.env.PUBLIC_URL}/images/dalia/top_bg.jpg")`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        color: "#111111",
        width: "100%",
        margin: 0,
        paddingTop: 85,
        height: '100%',
    },
    innerWrapper: {
        width: "100%",
        height: "100%",
        // backgroundImage: `url("${process.env.PUBLIC_URL}/images/top-image.png")`,
        // backgroundRepeat: "no-repeat",
        // backgroundPosition: "right -300px bottom 0px",
        // backgroundSize: "auto 620px",
    },
    container: {
        margin: "auto",
        height: "100%",
        [theme.breakpoints.down('sm')]: {
            width: theme.breakpoints.values.sm
        },
        [theme.breakpoints.down('md')]: {
            width: theme.breakpoints.values.md
        },
        [theme.breakpoints.only('lg')]: {
            width: 1000
        },
        [theme.breakpoints.up('xl')]: {
            width: theme.breakpoints.values.lg
        },
    }
}))

function HeadSection({ onJobClick, onSkillClick, onTopicClick }) {
    const classes = useStyles()

    return (
        <header className={classes.wrapper}>
            <div className={classes.innerWrapper}>
                <div className={classes.container}>
                    <SearchBox
                        onJobClick={onJobClick}
                        onSkillClick={onSkillClick}
                        onTopicClick={onTopicClick}
                    />
                </div>
            </div>
        </header>
    )
}

export default HeadSection
