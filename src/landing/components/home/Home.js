import React from "react";
import HeadSection from "./components/HeadSection";

function Home({ onJobClick, onSkillClick, onTopicClick }) {
    return (<div style={{ position: "relative", flexGrow: 1, }}>
        <HeadSection
            onJobClick={onJobClick}
            onSkillClick={onSkillClick}
            onTopicClick={onTopicClick}
        />
    </div>);
}

export default Home;
