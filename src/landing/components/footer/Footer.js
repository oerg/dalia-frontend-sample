import {Box, Grid, List, ListItem, Paper, Typography, withStyles, withWidth} from "@material-ui/core";
import transitions from "@material-ui/core/styles/transitions";
import PropTypes from "prop-types";
import React from "react";
import {useTranslation} from "react-i18next";
import ChangeLanguageButton from "../../../dashboard/components/ChangeLanguageButton";
import BMBFLogo from '../../../dashboard/views/Footer/images/bmbf.jpg';
import EULogo from '../../../dashboard/views/Footer/images/euflag.jpg';
import EULogo2 from '../../../dashboard/views/Footer/images/euflag2.png';
import TibLogo from '../../../dashboard/views/Footer/images/TIB_Logo.png';
import UALogo from '../../../dashboard/views/Footer/images/UA_Logo.svg';

const styles = theme => ({
    footerInner: {
        backgroundColor: theme.palette.common.darkBlack,
        paddingTop: theme.spacing(5),
        paddingLeft: theme.spacing(1),
        paddingRight: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    brandText: {
        fontFamily: "'Baloo Bhaijaan', cursive",
        fontWeight: 400,
        color: theme.palette.common.white
    },
    footerLinks: {
        marginTop: theme.spacing(2.5),
        marginBot: theme.spacing(1.5),
        color: theme.palette.common.white
    },
    infoIcon: {
        color: `${theme.palette.common.white} !important`,
        backgroundColor: "#33383b !important"
    },
    socialIcon: {
        fill: theme.palette.common.white,
        backgroundColor: "#33383b",
        borderRadius: theme.shape.borderRadius,
        "&:hover": {
            backgroundColor: theme.palette.primary.light
        }
    },
    link: {
        cursor: "Pointer",
        color: theme.palette.common.white,
        transition: transitions.create(["color"], {
            duration: theme.transitions.duration.shortest,
            easing: theme.transitions.easing.easeIn
        }),
        "&:hover": {
            color: theme.palette.primary.light
        }
    },
    whiteBg: {
        backgroundColor: theme.palette.common.white
    },
    list: {
        marginBottom: "0",
        padding: "0",
        marginTop: theme.spacing(2),
    },
    inlineBlock: {
        display: "inline-block",
        padding: "0px",
        width: "auto",
        margin: "5px 10px",
        textAlign: "center",
        '& a': {
            color: '#DDD',
            '&:hover': {
                color: '#8f9296',
            }
        },
    },
});

function Footer(props) {
    const { classes } = props;
    const { t } = useTranslation(["landing"]);

    return (
        <footer>
            <div className={classes.footerInner}>
                <Grid container spacing={2} justifyContent="center">
                    <Grid item>
                        <Paper elevation={1} style={{ padding: '5px', height: 70 }}>
                            <a href="https://www.uva.nl/en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("University of Amsterdam")} height={60}
                                    title={t("University of Amsterdam")} src={UALogo} />
                            </a>
                        </Paper>
                    </Grid>

                    <Grid item>
                        <Paper elevation={1} style={{ padding: '5px', height: 70 }}>
                            <a href="https://www.tib.eu/en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("Technische Informationsbibliothek (TIB)")} height={60}
                                    title={t("Technische Informationsbibliothek (TIB)")} src={TibLogo} />
                            </a>
                        </Paper>
                    </Grid>

                    <Grid item>
                        <Paper elevation={1} style={{ padding: '5px', height: 70 }}>
                            <a href="https://eacea.ec.europa.eu/homepage_en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("European Education and Culture Executive Agency")} height={60}
                                    title={t("European Education and Culture Executive Agency")} src={EULogo2} />
                            </a>
                        </Paper>
                    </Grid>

                    <Grid item>
                        <Paper elevation={1} style={{ padding: '5px', height: 70 }}>
                            <a href="https://eacea.ec.europa.eu/homepage_en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("European Education and Culture Executive Agency")} height={60}
                                    title={t("European Education and Culture Executive Agency")} src={EULogo} />
                            </a>
                        </Paper>
                    </Grid>

                    <Grid item>
                        <Paper elevation={1} style={{ padding: '5px', height: 70 }}>
                            <a href="https://www.bmbf.de/bmbf/en" rel="noopener noreferrer" target="_blank">
                                <img alt={t("Federal Ministry of Education and Research")} height={60}
                                    title={t("Federal Ministry of Education and Research")} src={BMBFLogo} />
                            </a>
                        </Paper>
                    </Grid>
                </Grid>
                <Box textAlign="center">
                    <List className={classes.list}>
                        <ListItem className={classes.inlineBlock}>
                            <a href="https://www.tib.eu/en/service/imprint/" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Imprint")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <a href="https://labs.tib.eu/edoer/tos" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Terms of Use")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <a href="https://www.tib.eu/en/service/data-protection/" rel="noopener noreferrer" target="_blank" className={classes.block}>
                                {t("Data Protection")}
                            </a>
                        </ListItem>
                        <ListItem className={classes.inlineBlock}>
                            <ChangeLanguageButton white style={{
                                height: 30,
                                width: 40,
                            }} />
                        </ListItem>
                    </List>
                    <div style={{ marginTop: 15 }}>
                        <a href={'https://www.edoer.eu/'} target={'_blank'}>
                        <img src={`${process.env.PUBLIC_URL}/images/logo2.png`} width={175} title={t('Powered by eDoer')} alt={'eDoer Logo'} />
                        <Typography style={{ color: "#8f9296" }} paragraph>
                            {t("Powered by eDoer")}
                            {/*<a rel="license noopener noreferrer" href="http://creativecommons.org/licenses/by-nc/4.0/" target="_blank">*/}
                            {/*    <img alt="Creative Commons License" style={{ borderWidth: 0 }} src="https://i.creativecommons.org/l/by-nc/4.0/80x15.png" />*/}
                            {/*</a>*/}
                        </Typography>
                        </a>
                    </div>
                </Box>
            </div>
        </footer>
    );
}

Footer.propTypes = {
    theme: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired,
    width: PropTypes.string.isRequired,
    isStatic: PropTypes.bool,
};

Footer.defaultProps = {
    isStatic: false,
}

export default withWidth()(withStyles(styles, { withTheme: true })(Footer));
