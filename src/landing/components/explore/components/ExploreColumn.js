import { Paper } from '@material-ui/core';
import React from 'react';

function ExploreColumn({ width, style, ...props }) {
    return <Paper style={{ width, height: "100%", overflowY: "auto", overflowX: "hidden", ...style }} square {...props} />
}

export default ExploreColumn;
