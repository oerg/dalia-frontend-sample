import axios from "axios";
import { MultiCall, requestAddHandlers } from "../../../dashboard/views/helpers";

export function loadReports(language, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(`/jobs/get-general-reports/?lang=${language}`),
        onLoad,
        onError,
        onEnd
    )
}

const addr = {
    "goals": "/jobs/get-all?",
    "skills": "/skills/get-all/?",
    "topics": "/skills/topics/get-all/?",
}

const ALL_CODE = "LLA"
export function getAll(section, learnable, language, onLoad, onError, onEnd) {
    const mc = MultiCall.getInstance(ALL_CODE);
    return mc.call(
        `${addr[section]}lang=${language}&${learnable ? 'learnable=True': ''}`,
        onLoad,
        onError,
        onEnd
    );
}

export function cancelGetAll() {
    MultiCall.getInstance(ALL_CODE).cancel();
}

export function getFirstChars(items, key = "title") {
    const chars = new Set();
    if (items) {
        for (const item of items) {
            chars.add(
                item[key].trim().charAt(0).toLowerCase()
            );
        }
    }
    return [...chars];
}

export function getViewTitle(item) {
    return item ? item.title : '';
}

export function emptyString() { return "" }

export function getDescription(item) {
    return item ? item.description : "";
}