import { Box, CircularProgress, Grid, makeStyles, Tab, Tabs } from '@material-ui/core';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useLanguage } from '../../../i18n';
import SearchItem from '../home/components/SearchItem';
import { extractIndustry, extractLocation } from '../home/utils';
import { emptyString, getDescription, getFirstChars, getViewTitle } from './utils';

const useStyles = makeStyles({
    tabRoot: {
        minWidth: 'auto',
        maxWidth: 60,
    },
    tabsCentered: {
        justifyContent: 'center',
    }
})

function Column2({ loading, goals, skills, topics, selectedSection, isDiscovery }) {
    const classes = useStyles();
    const { t } = useTranslation(['landing']);
    const systemLanguage = useLanguage();
    const [itemsToShow, setItemsToShow] = useState([]);
    const [selectedTab, setSelectedTab] = useState('');

    const startingChars = useMemo(
        () => {
            switch (selectedSection) {
                case "goals":
                    setItemsToShow(goals);
                    return getFirstChars(goals);
                case "skills":
                    setItemsToShow(skills);
                    return getFirstChars(skills);
                case "topics":
                    setItemsToShow(topics);
                    return getFirstChars(topics);
                default:
                    return [];
            }
        },
        [goals, skills, topics, selectedSection]
    );

    const getAddress = useCallback(
        (item) => {
            switch (selectedSection) {
                case "goals":
                    return isDiscovery?
                        `/${systemLanguage}/dashboard/discovery/jobs/${item.id}`
                    :
                        `/${systemLanguage}/dashboard/goals/${item.id}`;
                case "skills_learnable":
                    return isDiscovery?
                        `/${systemLanguage}/dashboard/discovery/skills/${item.id}`
                    :
                        `/${systemLanguage}/dashboard/skills/${item.id}`;
                case "topics_learnable":
                    return isDiscovery?
                        `/${systemLanguage}/dashboard/discovery/topics/${item.id}`
                    :
                        `/${systemLanguage}/dashboard/topics/${item.id}`;
                default:
                    return [];
            }
        },
        [ systemLanguage, selectedSection, isDiscovery ]
    );

    const calculatedItemProps = useMemo(
        () => ({
            formatTopText: selectedSection === "goals" ? extractIndustry(t) : emptyString,
            formatTitle: getViewTitle,
            formatBotText: selectedSection === "goals" ? extractLocation(t) : emptyString,
            formatDescription: getDescription,
            formatAddress: getAddress,
        }),
        [ selectedSection, t, getAddress ]
    );

    useEffect(
        () => {
            setSelectedTab('__all__');
        },
        [startingChars]
    );

    const handleTabs = useCallback(
        (e, newValue) => {
            setSelectedTab(newValue);
        },
        []
    );

    const handleClick = useCallback(
        e => e.stopPropagation(),
        []
    )

    return (loading ?
        <Box textAlign="center" pt={5}>
            <CircularProgress />
        </Box>
        :
        itemsToShow && itemsToShow.length > 0 ? 
        <Box pt={2}>
            <Tabs
                variant='scrollable'
                value={selectedTab}
                onChange={handleTabs}
                onClick={handleClick}
                classes={{
                    flexContainer: classes.tabsCentered
                }}
            >
                <Tab label={t("All")} key={-1} value="__all__" className={classes.tabRoot} />
                { startingChars.map(
                    (char, index) => (
                        <Tab label={char} key={index} value={char} className={classes.tabRoot} />
                    )
                )}
            </Tabs> 
            { itemsToShow &&
                <Grid container spacing={1} style={{ padding: 8 }}>
                { itemsToShow.filter(item => selectedTab === '__all__' || item.title.trim().toLowerCase().startsWith(selectedTab)).map(
                    (item, index) => (
                        <Grid item key={index} xs={12} md={6}>
                            <SearchItem
                                item={item}
                                lang={item.lang}
                                {...calculatedItemProps}
                            />
                        </Grid>
                    )
                )}
                </Grid>
            }
        </Box>
        :
        <Box textAlign="center" pt={5}>
            <em>{t("No items to show.")}</em>
        </Box>
    )
}

export default Column2