import React from "react"
import { Dialog, DialogContent, DialogTitle, Typography } from "@material-ui/core"
import LocalActivityIcon from "@material-ui/icons/LocalActivity"
import { useTranslation } from "react-i18next"

const DemoDialog = ({ open, onClose }) => {
    const { t } = useTranslation(["landing"]);

    return (
        <Dialog open={open} onClose={onClose} maxWidth="lg">
            <DialogTitle disableTypography>
                <Typography variant="h4" style={{ alignItems: "center", display: "flex", gap: 10 }}>
                    <LocalActivityIcon color="primary" fontSize="large" /> {t("eDoer Demo")}
                </Typography>

            </DialogTitle>
            <DialogContent style={{
                width: 850,
                height: 480,
            }}>
                <video width={800} height={450} controls>
                    <source src="https://labs.tib.eu/edoer/files/edoer.mp4" />
                </video>
            </DialogContent>
        </Dialog>
    )
}

export default DemoDialog
