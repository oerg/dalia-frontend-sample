from asyncore import write
import json
import os
import re
import sys
import csv

NEW_LANG_ARG = "new"  # argument for creating a new language
LANGS_DIR = "."  # the directory which contains language folders
TRANS_SECTIONS = {
    "landing.json": "../landing",
    "dashboard.json": "../dashboard",
}


def extract_t_calls(dir: str) -> dict:
    """Return a dictionary with all the strings which are called by t function in javascript codes as key and value."""
    path = os.walk(dir)
    t_func = re.compile(r"\bt[(]([\"'])(.+?)(\1)[)]")
    literals = dict()

    for root, _, files in path:
        for file in files:
            if file[-3:] != ".js":
                continue
            file_path = f"{root}/{file}"
            with open(file_path, "r") as file_handler:
                data = file_handler.read()
                found_ts = t_func.findall(data)
                # print(f"{len(found_ts)} t function calls found in {file_path}.")
                for ts in found_ts:
                    literal = ts[1]
                    literals[literal] = literal
    return literals


def unify_jsons(source_file: str, new_obj: dict) -> dict:
    """Return a dictionary by combining the JSON object in source file and new dictionary. Source file values have priority."""
    with open(source_file, "r") as fh:
        old_json = json.load(fh)

    return dict(new_obj, **old_json)


def save_json(obj: dict, file: str, sort_keys: bool = True) -> None:
    with open(file, "w") as fp:
        json.dump(obj, fp, indent=4, sort_keys=sort_keys, ensure_ascii=False)

def save_csv(obj: dict, file: str) -> None:
    rows = obj.keys()
    with open(f"{file}.csv", "w") as fp:
        writer = csv.writer(fp, )
        writer.writerow(["comment", "original text"])
        for row in rows:
            writer.writerow(["", row])


strings = dict()
# load strings of all sections
for section in TRANS_SECTIONS:
    strings[section] = extract_t_calls(TRANS_SECTIONS[section])

# check if new lang
# ./program.py new ar
if len(sys.argv) == 3 and sys.argv[1] == NEW_LANG_ARG:
    new_dir = f"{LANGS_DIR}/{sys.argv[2]}"
    os.mkdir(new_dir)
    for section in strings:
        save_json(strings[section], f"{new_dir}/{section}")
    print("New language created.")
    exit(0)

# check if csv output
# ./program.py csv
if len(sys.argv) == 2 and sys.argv[1] == "csv":
    for section in strings:
        save_csv(strings[section], f"{section}")
    print("New language created in csv.")
    exit(0)

# update all languages
lang_dirs = [f.path for f in os.scandir(LANGS_DIR) if f.is_dir()]
for lang_dir in lang_dirs:
    for section in strings:
        section_file = f"{lang_dir}/{section}"
        unified = unify_jsons(section_file, strings[section])
        save_json(unified, section_file)
        print(f"{section_file} has been updated.")
