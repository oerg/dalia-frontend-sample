import { Badge, Button, Grid, IconButton, makeStyles } from '@material-ui/core';
import { blue, green } from '@material-ui/core/colors';
import DraftsIcon from '@material-ui/icons/Drafts';
import MailIcon from '@material-ui/icons/Mail';
import PropTypes from 'prop-types';
import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import clsx from 'classnames';

const useStyles = makeStyles(
    theme => ({
        root: {
            ...theme.typography.caption,
            alignItems: "center",
            '&:hover': {
                backgroundColor: '#EFFFFF',
            },
        },
        actions: {
            textAlign: "right",
            marginRight: theme.spacing(2)
        },
        unread: {
            color: green[300],
        },
        read: {
            color: blue[50],
        },
        button: {
            width: 40,
            textAlign: "center",
            
        }
    })
);

function Notif({ text, actions, read, onRead, onAction, loading, className, ...props}) {
    const classes = useStyles();
    const { t } = useTranslation([ 'dashboard' ]);

    return (
        <Grid container className={clsx(classes.root, className)}>
            <Grid item className={classes.button}>
                { read ?
                    <span title={t("Marked as read")}>
                        <DraftsIcon className={classes.read} />
                    </span>
                :
                    <IconButton size="small" title={t("Mark as read")} disabled={read || loading} edge="start" onClick={onRead}>
                        <Badge variant="dot" color="secondary">
                            <MailIcon className={classes.unread} />
                        </Badge>
                    </IconButton>
                }
            </Grid>
            <Grid item xs container direction="column" spacing={1}>
                <Grid 
                    item 
                    xs
                >
                    {text}
                </Grid>
                { actions && actions.length > 0 &&
                    <Grid item className={classes.actions}>
                        <NotifActions actions={actions} onAction={onAction} loading={loading} />
                    </Grid> }
            </Grid>
        </Grid>
    )
}


export function NotifActions({ actions, onAction, loading }) {
    const classes = useStyles();

    const handleAction = useCallback(
        (act) => () => {
            if (onAction) onAction(act);
        },
        [ onAction ]
    );

    return (
        actions.map(
            (act, ind) => (
                <Button key={ind} variant="outlined" onClick={handleAction(act)} 
                    size="small" disabled={loading} className={classes.actionButton}>
                    {act['caption']}
                </Button>
            )
        )
    )
}

Notif.propTypes = {
    text: PropTypes.string.isRequired,
    actions: PropTypes.array,
    extraFields: PropTypes.array,
    onRead: PropTypes.func,
    onAction: PropTypes.func,
    read: PropTypes.bool,
}

Notif.defaultProps = {
    read: false,
}

export default Notif

