import { Box, Button, ClickAwayListener, Divider, Grow, makeStyles, Paper, Popper } from '@material-ui/core';
import clsx from 'classnames';
import PropTypes from 'prop-types';
import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import Notif from './components/Notif';

const useStyles = makeStyles(
    theme => ({
        root: {
            maxWidth: 450,
            maxHeight: 550,
            overflowY: 'auto',
            overflowX: 'hidden',
            padding: theme.spacing(1),
        },
        notif: {
            padding: theme.spacing(1),
        }
    })
);


function Notifications({ show, anchor, onClose, className, notifications, loading, onRead, onAction, onMarkAllAsRead, ...props}) {
    const classes = useStyles();
    const { t } = useTranslation([ 'dashboard' ]);

    const handleRead = useCallback(
        notif => () => onRead && onRead(notif),
        [ onRead ]
    );

    const handleAction = useCallback(
        notif => action => onAction && onAction(action, notif),
        [ onAction ]
    );

    const handleReadAll = useCallback(
        () => onMarkAllAsRead && onMarkAllAsRead(),
        []
    );

    return (
        <Popper
            open={show}
            onClose={onClose}
            anchorEl={anchor}
            role={undefined}
            placement="bottom-end"
            style={{ zIndex: 15 }}
            transition
            {...props}
        >
            {({ TransitionProps }) => (
                <Grow {...TransitionProps}>
                    <Paper elevation={1} className={clsx(classes.root, className)}>
                        <ClickAwayListener onClickAway={onClose}>
                            <div>
                            { notifications && notifications.length > 0 ?
                                <>
                                    { notifications.map(
                                        (notif, ind) => (
                                        <React.Fragment key={ind}>
                                            <Notif actions={notif['payload']['actions']} text={notif['payload']['text']} 
                                                read={notif['status'] === "RE"} className={classes.notif}
                                                onRead={handleRead(notif)} disabled={loading} onAction={handleAction(notif)} />
                                            <Divider />
                                        </React.Fragment>
                                        )
                                    )}
                                    <Button fullWidth variant='contained' onClick={handleReadAll} style={{
                                        position: "sticky",
                                        bottom: 0,
                                        backgroundColor: "#FFFFFF"
                                    }}>
                                        {t("Mark all as read")}
                                    </Button>
                                </>
                                :
                                <Box textAlign="center" p={2}>    
                                    <em>{t("No notifications")}</em>
                                </Box>
                            }
                            </div>
                        </ClickAwayListener>
                    </Paper>
                </Grow>
            )}
        </Popper>
    )
}

Notifications.propTypes = {
    notifications: PropTypes.array.isRequired,
}

export default Notifications

