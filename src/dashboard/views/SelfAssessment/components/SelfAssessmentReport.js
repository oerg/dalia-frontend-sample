import { Button, CircularProgress, Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import { withSnackbar } from 'notistack';
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../../i18n';
import { parseDateTime } from '../../../../utils';
import { getSelfAssessment } from '../utils'
import SelfAssessmentReportItem from './SelfAssessmentReportItem';

const useStyles = makeStyles(theme => ({
    root: {
        minWidth: 275,
    },
    typo: {
        padding: theme.spacing(1)
    },
    wrapper: {
        width: '100%',
        backgroundColor: "#FFFFFF",
        marginTop: theme.spacing(1),
        padding: theme.spacing(1),
    },
    pagination: {
        margin: theme.spacing(2),
        justifyContent: 'center'
    },
    ulPagination: {
        justifyContent: 'center'
    },
    loadingWrapper: {
        textAlign: "center",
    }

}));

function SelfAssessmentReport({ id, token, enqueueSnackbar }) {
    const classes = useStyles();

    const [loading, setLoading] = useState(false)
    const [selfAssessmentData, setSelfAssessmentData] = useState(null)
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage()

    useEffect(() => 
        {
            setLoading(true)
            getSelfAssessment(
                id,
                token,
                res => {
                    setSelfAssessmentData(res);
                },
                () => {
                    enqueueSnackbar(
                        t("Self-assessment not found!"),
                        {
                            variant: "error",
                            persist: true
                        }
                    );
                },
                () => setLoading(false)
            )

        },
        [id, token, enqueueSnackbar]
    )

    const handlePrint = useCallback(
        () => {
            const el = document.getElementById("assessment_result")
            const styles = document.head.getElementsByTagName("style")
            const winPrint = window.open('', '', 'left=0,top=0,width=1000,height=900,toolbar=0,scrollbars=0,status=0')
            winPrint.document.write(el.innerHTML)
            for (var i = 0; i < styles.length; i++) {
                winPrint.document.head.appendChild(styles.item(i).cloneNode(true))
            }
            winPrint.document.close()
            winPrint.focus()
            winPrint.print()
            winPrint.close()
        },
        []
    )


    return (loading ?
        <div style={{ padding: 16, textAlign: "center" }}>
            <CircularProgress />
        </div>
        :
        !selfAssessmentData ?
            null
            :
            <>
                <div className={classes.wrapper} spacing={1} id="assessment_result">
                    <Link to={`/${language}/dashboard/skills/${selfAssessmentData.skill}`}>
                        <Typography
                            variant='h5'
                            style={{
                                marginBottom: 8,
                                display: "inline-block"
                            }}
                            title={t("Course")}
                        >
                            {selfAssessmentData.skill_title}
                        </Typography>
                    </Link>
                    <Typography variant="caption">
                        <em> ({parseDateTime(selfAssessmentData.created_at)})</em>
                    </Typography>
                    <Paper style={{ padding: 16 }} dangerouslySetInnerHTML={{ __html: selfAssessmentData.options.description }} />
                    <div style={{ marginBottom: 8 }}>
                        <Paper square>
                            <Grid container>
                                <Grid
                                    item
                                    xs={5}
                                    style={{
                                        display: "flex",
                                        alignItems: "center",
                                        padding: 24
                                    }}
                                >
                                    <Typography
                                        variant="body"
                                        style={{
                                            fontWeight: "bold"
                                        }}
                                    >
                                        {t("Question")}
                                    </Typography>
                                </Grid>
                                <Grid
                                    item
                                    xs={2}
                                    style={{
                                        display: "flex",
                                        alignItems: "center",
                                        padding: 8
                                    }}
                                >
                                    <Typography
                                        variant="body"
                                        style={{
                                            fontWeight: "bold"
                                        }}
                                    >
                                        {t("Answer")}
                                    </Typography>
                                </Grid>
                                <Grid
                                    item
                                    xs={5}
                                    style={{
                                        display: "flex",
                                        alignItems: "center",
                                        textAlign: "center"
                                    }}
                                >
                                    <Typography
                                        variant="body"
                                        style={{
                                            fontWeight: "bold"
                                        }}
                                    >
                                        {t("Answer scale")}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Paper>
                        {
                            selfAssessmentData.answers.map((answer, index) => (
                                <SelfAssessmentReportItem key={index} answer={answer} options={selfAssessmentData.options} />
                            ))
                        }
                    </div>
                </div>
                <div
                    style={{
                        textAlign: "right"
                    }}
                >
                    <Button
                        onClick={handlePrint}
                        variant='contained'
                        color='primary'
                    >
                        {t("Print")}
                    </Button>
                </div>
            </>
    )
}

export default withSnackbar(SelfAssessmentReport)