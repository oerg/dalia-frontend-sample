import { MultiCall } from "../helpers"

const LQC = "FAQ_LQ_CODE"

export function loadQuestions(query, section, onLoad, onError, onEnd, addCode = '') {
    const mc = MultiCall.getInstance(LQC + addCode);
    const about_str = `about=GE&about=${section}`
    return mc.call(`/admin-services/faq/search/?search-term=${query}&page=1&${about_str}`, onLoad, onError, onEnd);
}

export function cancelLoadQuestions(addCode = '') {
    return MultiCall.getInstance(LQC + addCode).cancel();
}

const LDQC = "FAQ_LDQ_CODE"

export function loadDefaultQuestions(onLoad, onError, onEnd) {
    const mc = MultiCall.getInstance(LDQC);
    return mc.call(`/admin-services/faq/defaults`, onLoad, onError, onEnd);
}

export function cancelLoadDefaultQuestions() {
    return MultiCall.getInstance(LDQC).cancel();
}

const LRQC = "FAQ_LRQ_CODE"

export function loadRelatedQuestions(search, section, onLoad, onError, onEnd) {
    const mc = MultiCall.getInstance(LRQC);
    const about_str = `about=GE&about=${section}`
    return mc.call(`/admin-services/faq/search/?search-term=${search}&${about_str}`, onLoad, onError, onEnd);
}

export function cancelLoadRelatedQuestions() {
    return MultiCall.getInstance(LRQC).cancel();
}

const search_keywords = {
    'Goals': 'journey',
    'Courses': 'skill',
    'Topics': 'topic',
    'Packages': 'educational package',
    'Questions': 'questions',
    'Learn': 'learn tab',
    'Exams': 'assessment exam'
}

export function getSectionOfPage() {
    const selectedTab = document.querySelector('#navbar').children[1].children[0].querySelector('.Mui-selected')
    
    if (selectedTab) {
        const text = selectedTab.children[0].childNodes[1].data;
        return search_keywords[text] ? search_keywords[text] : text;
    }
    return '';
}