import { Button, CircularProgress, ClickAwayListener, Divider, Fade, IconButton, InputAdornment, makeStyles, Paper, Popper, Slide, TextField, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import SearchIcon from '@material-ui/icons/Search';
import { Alert } from '@material-ui/lab';
import React, { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useLocation } from 'react-router-dom';
import { UserContext } from '../../../landing/User';
import QuestionsList from '../../components/QuestionsList';
import MentoringRequestDialog from '../Overlays/MentoringRequestDialog';
import { cancelLoadQuestions, cancelLoadRelatedQuestions, getSectionOfPage, loadQuestions, loadRelatedQuestions } from './utils';

const useStyles = makeStyles(
    theme => ({
        root: {
            position: "fixed",
            top: 0,
            right: 0,
            width: 500,
            zIndex: theme.zIndex.drawer + 1,
            height: "100vh",
            padding: theme.spacing(1),
            display: "flex",
            flexDirection: "column",
            overflowX: "hidden",
        },
        divider: {
            marginBottom: theme.spacing(1),
            marginTop: theme.spacing(1),
        },
        askWrapper: {
            width: 350,
        },
        askRoot: {
            zIndex: theme.zIndex.drawer + 2,
        },
        titleWrapper: {
            display: "flex",
            width: "100%",
            marginBottom: theme.spacing(1),
        },
        title: {
            flexGrow: 1,
        },
        results: {
            flexGrow: 1,
        },
        resultItems: {
            overflowY: "auto",
        }
    })
);

function Faq({ open, onClose, ...props}) {
    const classes = useStyles();
    const user = useContext(UserContext)

    const [query, setQuery] = useState('');
    const [loading, setLoading] = useState(false);
    const [questions, setQuestions] = useState([]);
    const [relatedQuestions, setRelatedQuestions] = useState([]);

    const [askLoading, setAskLoading] = useState(false);
    const [askResult, setAskResult] = useState(null);
    const [askTarget, setAskTarget] = useState(null);
    const [askOpen, setAskOpen] = useState(false);
    const [showRequestMentor, setShowRequestMentor] = useState(false);

    const { t } = useTranslation(["dashboard"]);
    const location = useLocation();
    const section = useMemo(
        () =>  location.pathname.indexOf('discovery') === -1 ? 'LE' : 'CO',
        [location]
    );

    useEffect(
        () => {
            if (open) {
                cancelLoadRelatedQuestions();
                setQuery("");
                setLoading(true);
                loadRelatedQuestions(
                    getSectionOfPage(),
                    section,
                    res => {
                        setRelatedQuestions(res.data.results)
                    },
                    null,
                    () => setLoading(false)
                );
            }
        },
        [ open, section ]
    );

    const handleQueryChange = useCallback(
        e => {
            cancelLoadQuestions();
            cancelLoadRelatedQuestions();
            setQuery(e.target.value);
            if (e.target.value === '') {
                setQuestions([]);
                setLoading(false);
                return;
            }
            setLoading(true);
            loadQuestions(
                e.target.value,
                section,
                ({ data: res }) => {
                    setQuestions(res.results);
                },
                null,
                () => setLoading(false)
            );
        },
        [section]
    );

    const handleAsk = useCallback(
        (question, dom) => {
            cancelLoadQuestions("ask");
            setAskLoading(true);
            setAskOpen(true);
            setAskTarget(dom);
            loadQuestions(
                question,
                section,
                ({ data: res }) => {
                    setAskResult([ res.results[0] ]);
                },
                () => setAskOpen(false),
                () => setAskLoading(false),
                "ask"
            );
        },
        [section]
    );

    const handleAskClose = useCallback(
        () => setAskOpen(isOpen => isOpen ? false : true),
        []
    );

    const handleOpenRequest = useCallback(
        () => setShowRequestMentor(true),
        []
    );

    const handleCloseRequest = useCallback(
        () => setShowRequestMentor(false),
        []
    )

    return (
        <Slide direction='left' in={open} unmountOnExit>
            <div>
                <ClickAwayListener onClickAway={onClose}>
                    <Paper elevation={2} className={classes.root}>
                        <div className={classes.titleWrapper}>
                            <Typography variant='h4' className={classes.titleWrapper}>
                                {t("Help")}
                            </Typography>
                            <div>
                                <IconButton size='small' onClick={onClose} title={t("Close")}>
                                    <CloseIcon />
                                </IconButton>
                            </div>
                        </div>
                        <TextField
                            autoFocus
                            value={query}
                            onChange={handleQueryChange}
                            InputProps={{
                                endAdornment: <InputAdornment position='end'>
                                    {loading ? 
                                        <CircularProgress />
                                        :
                                        <SearchIcon />
                                    }
                                </InputAdornment>,
                                placeholder: t("Search in website help...")
                            }}
                            fullWidth
                            variant="outlined"
                        />
                        <Divider className={classes.divider} />
                        <div className={classes.results} >
                            { !loading && (
                                query ?
                                    <QuestionsList questions={questions} onAsk={handleAsk} className={classes.resultItems} />
                                :
                                    <>
                                        <Typography variant='h5' style={{ marginBottom: 8 }}>
                                            {t("Related Questions")}
                                        </Typography>
                                        <QuestionsList questions={relatedQuestions} onAsk={handleAsk} className={classes.resultItems} />
                                    </>
                            )}
                        </div>
                        <Alert
                            severity='info'
                            variant='filled'
                            action={<Button onClick={handleOpenRequest}>
                                {t("Request a mentor")}
                            </Button>}
                        >
                            {t("If you need help with the content of the website, request a mentor.")}       
                        </Alert>
                        <MentoringRequestDialog
                            user={user}
                            open={showRequestMentor}
                            onClose={handleCloseRequest}
                        />
                        <Popper open={askOpen} anchorEl={askTarget} transition placement='bottom'
                            modifiers={{
                                arrow: {
                                    enabled: true,
                                    // element: arrowRef,
                                },
                                preventOverflow: {
                                    enabled: true,
                                    boundariesElement: 'scrollParent',
                                },
                            }}
                            className={classes.askRoot}
                        >
                            {({ TransitionProps }) => (
                                <Fade {...TransitionProps}>
                                    <ClickAwayListener onClickAway={handleAskClose}>
                                        <Paper className={classes.askWrapper}>
                                            { askLoading ?
                                                <div style={{ textAlign: "center", padding: 16 }}>
                                                    <CircularProgress />
                                                </div>
                                            :
                                                <div>
                                                    <QuestionsList questions={askResult} defaultOpen onAsk={handleAsk} />
                                                    <Button fullWidth onClick={handleAskClose}>
                                                        {t("Close")}
                                                    </Button>
                                                </div>
                                            }
                                        </Paper>
                                    </ClickAwayListener>
                                </Fade>
                            )}
                        </Popper>
                    </Paper>
                </ClickAwayListener>
            </div>
        </Slide>
    )
}

export default Faq
