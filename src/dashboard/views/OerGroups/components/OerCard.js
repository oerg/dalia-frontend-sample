import { IconButton } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import { green } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core/styles';
import LaunchIcon from '@material-ui/icons/Launch';
import React, { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import OerLogo from '../../../components/Logos/Oer';
import DetailWindow from './DetailWindow';

const useStyles = makeStyles((theme) => ({
    root: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-around"
    },
    actionRight: {
        marginLeft: 'auto'
    },
    avatar: {
        backgroundColor: green[100],
    },
    cardHeaderTitle: {
        width: "95%",
        whiteSpace: "nowrap",
        overflow : "hidden",
        textOverflow: "ellipsis"
    },
    doneIcon: {
        cursor: "default",
    },
    title: {
        cursor: "pointer",
        paddingRight: theme.spacing(0.5)
    }
}));

export default function OerCard({ oer, basePath }) {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);

    const [mouseOver, setMouseOver] = useState(false);
    const [showDescription, setShowDescription] = useState(false);

    const handleMouseEnter = useCallback(
        () => setMouseOver(true),
        []
    );

    const handleMouseOut = useCallback(
        () => setMouseOver(false),
        []
    );

    const handleShowDetails = useCallback(
        () => setShowDescription(true),
        []
    );

    const handleCloseDescription = useCallback(
        () => setShowDescription(false),
        []
    );

    return (<>
        <Card className={classes.root}
            raised={mouseOver} 
            onMouseEnter={handleMouseEnter} 
            onMouseLeave={handleMouseOut}
            >
            <CardHeader
                avatar={<OerLogo />}
                title={oer.oer.title}
                titleTypographyProps={{
                    title: oer.title,
                    variant: "h6",
                    className: classes.title,
                    onClick: handleShowDetails
                }}
            />
            <CardActions disableSpacing>
                <div className={classes.actionRight}>
                    <IconButton title={t("Learn more")} onClick={handleShowDetails}>
                        <LaunchIcon />
                    </IconButton>
                </div>
            </CardActions>
        </Card>
        <DetailWindow
            title={oer.oer.title}
            icon={<OerLogo />}
            open={showDescription}
            onClose={handleCloseDescription} 
            url={oer.oer.url}
        >
            <p>
            {oer.description}
            </p>
        </DetailWindow>
    </>);
}
