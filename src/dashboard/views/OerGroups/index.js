import { Box, Button, CircularProgress, Grid, Paper, Typography } from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People';
import ShareIcon from '@material-ui/icons/Share';
import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useParams } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import Card from '../../components/Card/Card';
import CardBody from '../../components/Card/CardBody';
import CardHeader from '../../components/Card/CardHeader';
import GridContainer from '../../components/Grid/GridContainer';
import GridItem from '../../components/Grid/GridItem';
import OergLogo from '../../components/Logos/Oerg';
import ShareButton from '../../components/ShareButton';
import { getOerGroup } from '../Discovery/OerGroups/utils';
import useStyles from '../styles/SkillsStyle';
import OerCard from './components/OerCard';

export default function OerGroups({ basePath, user, userData }) {
    const classes = useStyles();
    const { id: strOerGroupId } = useParams();

    const oerGroupId = useMemo(
        () => {
            if (strOerGroupId) {
                return parseInt(strOerGroupId);
            }
            return null;
        },
        [strOerGroupId]
    )

    const [oerg, setOerg] = useState(null);
    const [oers, setOers] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [criticalError, setCriticalError] = useState(null);

    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    useEffect(() => { // load oerGroup and suggestions
        setLoading(true);
        getOerGroup(oerGroupId, (data) => { // load
            setOerg(data);
            setOers(data.oergroupoer_set.sort((a, b) => a.order - b.order));
            setLoading(false);
        }, error => { // on error
            if (error) {
                if (error.status === 404) {
                    setCriticalError("404: OerGroup was not found!");
                } else if (error.status >= 500) {
                    setCriticalError(error.status + ": Server error!")
                } else
                    setCriticalError(error.data);
            }
            setLoading(false);
        });
    }, [oerGroupId])

    return (
        isLoading ?
            <Box p={10} textAlign="center">
                <CircularProgress />
            </Box>
            :
            criticalError ?
                <Box p={10} textAlign="center">
                    {criticalError}
                </Box>
                :
                <GridContainer>
                    <GridItem xs={12}>
                        <Card>
                            <CardHeader color="rose" className={classes.titleWithButton}>
                                <OergLogo className={classes.logo} />

                                <div className={classes.cardTitleDiv}>
                                    <h3 className={classes.cardTitleWhite}>
                                        {oerg.title}
                                    </h3>
                                </div>
                                <div className={classes.cardButtonDiv}>
                                    {(user.loggedIn && userData.settings.discovery) &&
                                        <Button title={t("Participate in this Educational Package")} variant="outlined"
                                            component={Link} to={`/${language}/dashboard/discovery/oer-groups/${oerGroupId}`}
                                        >
                                            <PeopleIcon />
                                        </Button>
                                    }
                                    <ShareButton
                                        component={Button}
                                        variant="outlined"
                                        url={`${window.location.href}`}
                                        title={oerg.title}
                                        componentTitle={t("Share this Educational Package")}
                                    >
                                        <ShareIcon />
                                    </ShareButton>
                                </div>
                            </CardHeader>
                            <CardBody>
                                {oerg.description &&
                                    <Paper elevation={2} style={{ marginTop: 5, marginBottom: 25, padding: 20 }}>
                                        {oerg.description}
                                    </Paper>}
                                <Grid container spacing={3} className={classes.hasPadding} alignContent="stretch">
                                    <Grid item xs={12} className={classes.col} align="center">
                                        <Typography variant="h5" component="h2">
                                            {t("Existing Educational Contents in this Package")}
                                        </Typography>
                                    </Grid>
                                    {oers.length === 0 ?
                                        <i>{t("There are no educational content.")}</i>
                                        :
                                        oers.map((oer, ind) => (
                                            <Grid key={ind} item xs={12} md={6} lg={4}>
                                                <OerCard oer={oer} />
                                            </Grid>
                                        ))
                                    }
                                </Grid>
                            </CardBody>
                        </Card>
                    </GridItem>

                </GridContainer>
    )
}
