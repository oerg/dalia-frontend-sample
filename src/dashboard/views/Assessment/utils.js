import axios from "axios";
import { requestAddHandlers } from "../helpers";

export function getSelfAssessmentReport(id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(`/assessments/questions/get-self-assessments/?skill_id=${id}`),
        onLoad,
        onError,
        onEnd
    );
}

export function saveShares(assessment_id, emails, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.post(
            '/assessments/questions/share-self-assessment/',
            {
                assessment_id,
                emails,
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}