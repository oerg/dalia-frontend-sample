import { Box, CircularProgress, Grid, IconButton, Paper, Link as MuiLink } from '@material-ui/core'
import BuildIcon from '@material-ui/icons/Build'
import EditIcon from '@material-ui/icons/Edit'
import { withSnackbar } from 'notistack'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useHistory, useParams } from 'react-router-dom'
import { useLanguage } from '../../../../i18n'
import { backend_base_url } from '../../../../settings'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import DiscoveryNav from '../../../components/DiscoveryNav'
import ExpertRequestButton from '../../../components/ExpertRequestButton'
import GridContainer from '../../../components/Grid/GridContainer'
import GridItem from '../../../components/Grid/GridItem'
import Help from '../../../components/Help'
import Icons from '../../../components/Icons'
import LangIcon from '../../../components/LangIcon'
import LoginWarning from '../../../components/LoginWarning'
import TopicLogo from '../../../components/Logos/Topic'
import ToggleDiv from '../../../components/ToggleDiv'
import UserTable from '../../../components/TopUsers/UserTable'
import VotingTable from '../../../components/Vote/VotingTable'
import WordSuggestion from '../../../components/WordSuggestion'
import useStyles from '../../styles/SkillsStyle'
import QuestionsTable from '../components/QuestionsTable'
import { addWordOpinion, loadTopContributors } from '../utils'
import AddTopicDialog from './AddTopicDialog'
import OersTable from './OersTable'
import { addDeleteSuggestion, addNewOerg, doOergVote, doSkillVote, doSuggestionVote, filterSuggestions, formatSkillTitle, getTopic, loadOerGroup, processVote } from './utils'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import IAlert from '../../../components/ISys/IAlert'


function Topics({ requireLogin, navHistory, onNav, enqueueSnackbar }) {
    const classes = useStyles();
    const { id: strTopicId } = useParams();
    const { t } = useTranslation(['dashboard']);

    const topicId = useMemo(
        () => {
            if (strTopicId) {
                return parseInt(strTopicId);
            }
        },
        [strTopicId]
    )

    const [topic, setTopic] = useState(null);
    const [oergs, setOergs] = useState([]);
    const [delSuggestions, setDelSuggestions] = useState([]);
    const [skills, setSkills] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [criticalError, setCriticalError] = useState(null);
    const [votingOergs, setVotingOergs] = useState(false);
    const [votingSuggestions, setVotingSuggestions] = useState(false);
    const [votingSkill, setVotingSkill] = useState(false);
    const [oerSuggestions, setOerSuggestions] = useState([]);
    const [oersLoading, setOersLoading] = useState(false);
    const [suggestionsOpen, setSuggestionsOpen] = useState(false);
    const [edoerSuggestionsOpen, setEdoerSuggestionsOpen] = useState(false)
    const [addOerObject, setAddOerObject] = useState({});
    const [topContributors, setTopContributors] = useState([]);
    const language = useLanguage();

    const [reloadNeeded, setReloadNeeded] = useState(true);
    const [showAddOer, setShowAddOer] = useState(false);

    const history = useHistory();

    useEffect(() => { // load skill and suggestions
        setLoading(true);
        getTopic(topicId, (data, suggestions) => { // load
            const suggs = filterSuggestions(suggestions.results);
            setOergs(data.oer_groups);
            setTopic(data);
            setSkills(data.skilltopic_set.sort((a, b) => a.order - b.order));
            setDelSuggestions(suggs.remove);
            setOerSuggestions(data.recommended_oer_groups);
            setLoading(false);
        }, error => { // on error
            if (error) {
                if (error.status === 404) {
                    setCriticalError("404: Topic was not found!");
                } else if (error.status >= 500) {
                    setCriticalError(error.status + ": Server error!")
                } else
                    setCriticalError(error.data);
            }
            setLoading(false);
        });
        setReloadNeeded(false);
    }, [topicId, reloadNeeded]);

    useEffect(() => { // load top contributors
        loadTopContributors(
            "TO",
            topicId,
            (res) => {
                setTopContributors(res)
            },
            null,
            null)
    }, [topicId])

    const handleOergVote = useCallback((id) => (up, down, selfVote) => {
        if (requireLogin) return setShowLoginWarning(true);

        setVotingOergs(true);
        doOergVote(topicId, id, selfVote,
            () => {
                setOergs(t => processVote(t, id, up, down, selfVote))
            },
            null,
            () => setVotingOergs(false)
        );
    }, [topicId, requireLogin]);

    const handleSuggestionVote = useCallback((setter) => (id) => (up, down, selfVote) => {
        if (requireLogin) return setShowLoginWarning(true);

        setVotingSuggestions(true);
        doSuggestionVote(id, selfVote, () => {
            setVotingSuggestions(false);
            setter(s => processVote(s, id, up, down, selfVote))
        })
    }, [requireLogin]);

    const handleSkillVote = useCallback((id) => (up, down, selfVote) => {
        if (requireLogin) return setShowLoginWarning(true);

        setVotingSkill(true);
        doSkillVote(id, topicId, selfVote, () => {
            setVotingSkill(false);
            setSkills(j => processVote(j, id, up, down, selfVote, 'skill'));
        })
    }, [topicId, requireLogin]);

    const handleDelete = useCallback((oergId) => { // deleting suggestions
        if (requireLogin) return setShowLoginWarning(true);

        setLoading(true);
        addDeleteSuggestion(topicId, oergId, () => {
            setLoading(false);
            setReloadNeeded(true);
        });
    }, [topicId, requireLogin])

    const handleNew = useCallback((data) => {
        if (requireLogin) return setShowLoginWarning(true);
        const topics = data.topics.map(t => t.id)
        setLoading(true);
        addNewOerg({ ...data, topics }, () => {
            setLoading(false);
            setReloadNeeded(true);
        });
    }, [topicId, requireLogin])

    const formatDelTitle = useCallback(s => {
        const oerg = oergs.find(t => t.id === s.payload.oer_group_id);
        return <span>
            {t("Delete")} <Link to={`/${language}/dashboard/discovery/oer-groups/${oerg.id}`} target="_blank"><i><b>{oerg.title}</b></i></Link>
        </span>
    }, [oergs, t])

    const formatSkillLink = useCallback(skill => {
        return `/${language}/dashboard/discovery/skills/${skill.skill}`;
    }, []);

    const handleOerClick = useCallback(
        oerSugg => {
            window.open(
                oerSugg.recommendation.url,
                '_blank'
            )
        },
        []
    )

    // login
    const [showLoginWarning, setShowLoginWarning] = useState(false);
    const handleLogin = useCallback((action) => {
        setShowLoginWarning(false);
        if (action) {
            history.push(action);
        }
    }, [history]);

    const handleAddWord = useCallback(
        (id, word) => {
            setAddOerObject({ id, word });
            setOersLoading(true);
            setShowAddOer(true);
        },
        [t]
    );

    const handleNeutral = useCallback(
        (id) => {
            setOersLoading(true);
            addWordOpinion(
                id,
                "NE",
                null,
                null,
                () => setOersLoading(false)
            )
        },
        []
    );

    const handleIrrelaventWord = useCallback(
        (id) => {
            setOersLoading(true);
            addWordOpinion(
                id,
                "IR",
                () => {
                    enqueueSnackbar(
                        t("Thank you for your feedback."),
                        {
                            variant: "info"
                        }
                    );
                    setOerSuggestions(
                        words => words.filter(w => w.id !== id)
                    );
                },
                null,
                () => setOersLoading(false)
            )
        },
        [t, enqueueSnackbar]
    );

    const handleAddedWord = useCallback(
        (id) => {
            setOersLoading(true);
            addWordOpinion(
                id,
                "AA",
                () => {
                    enqueueSnackbar(
                        t("Thank you for your feedback."),
                        {
                            variant: "info"
                        }
                    );
                    setOerSuggestions(
                        words => words.filter(w => w.id !== id)
                    )
                },
                null,
                () => setOersLoading(false)
            )
        },
        [t, enqueueSnackbar]
    );

    const handleAddOerDialog = useCallback(
        (save, topics) => {
            if (save) {
                addWordOpinion(
                    addOerObject.id,
                    "AD",
                    (res) => {
                        if (window.confirm(t('This educational package has been added to this topic. Would you like to edit the metadata?'))) {
                            loadOerGroup(
                                res.oer_group_id,
                                innerRes => {
                                    if (innerRes.oergroupoer_set && innerRes.oergroupoer_set.length > 0)
                                        history.push(`/${language}/dashboard/discovery/oers/edit/${innerRes.oergroupoer_set[0].oer.id}`)
                                },
                                null,
                                () => setOersLoading(false)
                            )

                        } else {
                            setReloadNeeded(true);
                            setOersLoading(false)
                        }
                    },
                    null,
                    null,
                    topics.map(t => t.id)
                );
            }
            setShowAddOer(false);
        },
        [addOerObject]
    );

    return (
        <>
            {requireLogin &&
                <LoginWarning show={showLoginWarning} onSelect={handleLogin} />
            }
            {isLoading ?
                <Box p={10} textAlign="center">
                    <CircularProgress />
                </Box>
                :
                criticalError ?
                    <Box p={10} textAlign="center">
                        {criticalError}
                    </Box>
                    :
                    <>
                        <DiscoveryNav topic={topic} onNav={onNav} navHistory={navHistory} />
                        <AddTopicDialog show={showAddOer} mainTopic={topic} onClose={handleAddOerDialog} oerTitle={addOerObject.word} />
                        <GridContainer>
                            <GridItem xs={12}>
                                <Card>
                                    <CardHeader color="rose" style={{ display: 'flex', alignItems: 'center' }}>
                                        <TopicLogo picture={topic.picture} size="large" style={{ marginRight: 10 }} />
                                        <h3 className={classes.cardTitleWhite}>
                                            {topic['title']} <LangIcon language={topic.lang} small />
                                            <Box component="span" ml={1}>
                                                {(topic.is_contributor || topic.is_admin || topic.is_expert) &&
                                                    <IconButton style={{ color: "white" }} title={t("Edit this topic")} size="small" onClick={() => history.push(`/${language}/dashboard/discovery/topics/edit/${topic['id']}`)}>
                                                        <EditIcon />
                                                    </IconButton>
                                                }
                                            </Box>
                                            <Icons learnable={topic.learnable} assessmentable={topic.assessmentable} withBorder={true} className={classes.lightBorders} isTopic />
                                        </h3>
                                        <IconButton
                                            style={{ color: "white", marginRight: 20 }}
                                            component={MuiLink}
                                            target="_blank"
                                            href={`${backend_base_url}topics/download-json-export/?topic_id=${topic.id}`}
                                            title={t("Download the structure of this item as JSON")}
                                            size="small"
                                        >
                                            <CloudDownloadIcon />
                                        </IconButton>
                                        {topic.is_expert ?
                                            <div
                                                style={{
                                                    border: "1px solid #FFF",
                                                    borderRadius: 5,
                                                    width: 35,
                                                    height: 35,
                                                    display: "flex",
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                                }}
                                                title={t("You are an expert")}
                                            >
                                                <BuildIcon
                                                    htmlColor='#FFF'
                                                />
                                            </div>
                                            :
                                            <ExpertRequestButton object_id={topicId} object_type="TO" startIcon={<BuildIcon />} />
                                        }
                                    </CardHeader>
                                    <CardBody>
                                        {topic.description &&
                                            <Paper elevation={2} className={classes.descriptionBox}>
                                                <div>
                                                    {topic.description}
                                                </div>
                                            </Paper>}
                                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }}>
                                            <Grid container spacing={3} className={classes.hasPadding} alignContent="flex-start">
                                                <Grid item xs={12} md={6} className={classes.col}>
                                                    <OersTable onVote={handleOergVote} oers={oergs} onDelete={handleDelete} onNew={handleNew}
                                                        disabled={votingOergs} headClass={classes.addSugg} topic={topic} />
                                                    <VotingTable onVote={handleSkillVote} data={skills} formatTitle={formatSkillTitle} disabled={votingSkill} idField="skill"
                                                        objectType="SK" title={t("Related Courses")} emptyText={t("No Courses")} height="100%" collapsable headClass={classes.delSugg}
                                                        formatLink={formatSkillLink} style={{ marginBottom: 8, marginTop: 8 }} />
                                                    <UserTable data={topContributors} title={t("Top Contributors")}
                                                        height="100%" collapsable headClass={classes.delSugg} />
                                                </Grid>
                                                <Grid item xs={12} md={6} className={classes.col}>
                                                    <ToggleDiv
                                                        title={<b>{t("Suggestions")}</b>}
                                                        titleClassName={classes.suggestionsBox}
                                                        open={suggestionsOpen}
                                                        onChange={o => setSuggestionsOpen(o)}
                                                        help={
                                                            <Help>
                                                                {t("Here you can find:")}
                                                                <ul>
                                                                    <li>{t("eDoer Intelligence which you can validate, and add if they fit this page. Also, you can mark them as irrelevant or indicate if they already exist in this page.")}</li>
                                                                    <li>{t("Up-vote/Down-vote existing sugestions (i.e. Delete). eDoer automatically decides to accept or reject the suggestions based on your votes.")}</li>
                                                                </ul>
                                                            </Help>
                                                        }
                                                        style={{
                                                            marginBottom: 8
                                                        }}>
                                                        <Paper
                                                            style={{
                                                                padding: 8
                                                            }}>
                                                            <ToggleDiv
                                                                title={<b>{t("eDoer Intelligence")}</b>}
                                                                titleClassName={classes.edoerSugg}
                                                                open={edoerSuggestionsOpen}
                                                                onChange={o => setEdoerSuggestionsOpen(o)}
                                                                help={
                                                                    <Help>
                                                                        {t("You can validate the eDoer's intelligent suggestions and add them if they fit this page. Also, you can mark them as irrelevant or indicate if they already exist in this page.")}
                                                                    </Help>
                                                                }
                                                                style={{
                                                                    marginBottom: 8
                                                                }}
                                                            >
                                                                {oerSuggestions.length > 0 ?
                                                                    <WordSuggestion
                                                                        words={oerSuggestions}
                                                                        pause={oersLoading || !suggestionsOpen}
                                                                        disabled={oersLoading}
                                                                        formatWord={word => word.recommendation.title}
                                                                        question={t("Is this a good content for this topic?")}
                                                                        onWordClick={handleOerClick}
                                                                        onAdd={handleAddWord}
                                                                        onIrrelavent={handleIrrelaventWord}
                                                                        onAlreadyAdded={handleAddedWord}
                                                                        onNeutral={handleNeutral}
                                                                        formatFrom={word => word.recommendation.resource}
                                                                        showRelevancy
                                                                    />
                                                                    :
                                                                    <>
                                                                        {new Date().getTime() - new Date(topic.created_at) < (30 * 60 * 1000) ?
                                                                            <Paper style={{ padding: 24, backgroundColor: "#e8f4fd" }}>
                                                                                <IAlert
                                                                                    show
                                                                                    transition={false}
                                                                                    style={{
                                                                                        border: "1px solid #2196f3",
                                                                                    }}
                                                                                >
                                                                                    {t("We are searching for suggestions to help you develop this page. Please check again this part in a few minutes.")}
                                                                                </IAlert>
                                                                            </Paper>
                                                                            :
                                                                            <Paper style={{ padding: 24, backgroundColor: "#e8f4fd" }}>
                                                                                <IAlert
                                                                                    show
                                                                                    transition={false}
                                                                                    style={{
                                                                                        border: "1px solid #2196f3",
                                                                                    }}
                                                                                >
                                                                                    {t("Currently, there is no eDoer-generated suggestion for this page.")}
                                                                                </IAlert>
                                                                            </Paper>
                                                                        }
                                                                    </>
                                                                }
                                                            </ToggleDiv>
                                                            <VotingTable onVote={handleSuggestionVote(setDelSuggestions)} data={delSuggestions} disabled={votingSuggestions} objectType="SU"
                                                                title={t("DELETE")} className={classes.hasYMargin} formatTitle={formatDelTitle} headClass={classes.movSugg} />
                                                        </Paper>
                                                    </ToggleDiv>
                                                    <QuestionsTable
                                                        titleClassName={classes.suggestionsBox}
                                                        objectType="TO"
                                                        objectId={topicId}
                                                        linkExternalData={{
                                                            topicId,
                                                            topicTitle: topic.title
                                                        }}
                                                        showAdd={topic.is_expert || topic.is_admin}
                                                    />
                                                </Grid>
                                            </Grid>
                                        </Paper>
                                    </CardBody>
                                </Card>
                            </GridItem>
                        </GridContainer>
                    </>
            }
        </>
    )
}

Topics.defaultProps = {
    requireLogin: false,
}

export default withSnackbar(Topics)
