import { Box, Chip, Grid, IconButton, LinearProgress, makeStyles, Paper } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { Alert } from '@material-ui/lab'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useHistory } from 'react-router-dom'
import { useLanguage } from '../../../../i18n'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import Button from '../../../components/CustomButton/Button'
import IDiv from '../../../components/ISys/IDiv'
import LangTextField from '../../../components/LangTextField'
import { StaticNote } from '../../../components/Notes'
import { getSuggestionColor, useParam } from '../../helpers'
import { roseColor } from '../../styles/variables'
import Description from '../components/Description'
import { cancelIsEditableTopic, cancelLoadSimilarTopics, doAddTopic, getIsEditableTopic, loadSimilarTopics as getSimilarTopics } from './utils'

const useStyles = makeStyles(theme => ({
    formField: {
        padding: theme.spacing(2),
        // textAlign: 'center',
        position: 'relative',
    },
    listRoot: {
        border: '1px solid #EEE',
        borderRadius: '5px',
        textAlign: 'center',
        margin: `${theme.spacing(1)}px auto`,
        maxHeight: '300px',
        overflowY: 'auto',
        '& > li:nth-child(even)': {
            backgroundColor: '#CCC',
        }
    },
    smallCenter: {
        width: '60%',
        minWidth: '200px',
        margin: 'auto',
    },
    addTopicButton: {
        position: 'absolute',
        color: roseColor[0],
        bottom: 0,
        zIndex: 999,
    },
    toRight: {
        right: 10,
    },
    addTopicForm: {
        padding: theme.spacing(2),
        color: '#FFF',
        position: 'absolute',
        zIndex: 998,
        bottom: -30,
        right: 55,
        left: 30,
    },
    suggestions: {
        border: '1px solid ' + getSuggestionColor(0.65),
        color: getSuggestionColor(0.65),
        margin: theme.spacing(2),
        position: 'relative',
        maxHeight: 250,
        minHeight: 80,
        overflowY: 'auto',
        '& > *': {
            margin: theme.spacing(0.5),
        }
    },
    suggestionsLoading: {
        position: 'absolute',
        width: 35,
        height: 35,
        top: 5,
        right: 15,
    },
    suggestionsHelp: {
        position: 'absolute',
        width: 35,
        height: 35,
        right: 12,
        top: 8,
        color: "#888",
        opacity: 0.7,
        cursor: 'help',
        transition: 'opacity 1s, top 0.5s',
        '&:hover': {
            opacity: 0.9
        }
    },
    suggestion: {
        backgroundColor: getSuggestionColor(0.65),
        borderColor: getSuggestionColor(0.65),
        "&:focus": {
            backgroundColor: `${getSuggestionColor(0.65)} !important`,
        }
    }
}))

function Add({ navHistory, ...props }) {
    const classes = useStyles();
    const [title, setTitle] = useState('');
    const [loading, setLoading] = useState(false);
    const [isEditable, setIsEditable] = useState(null);
    const [editableId, setEditableId] = useState(null);
    const [editableLoading, setEditableLoading] = useState(false);
    const [description, setDescription] = useState('');
    const [similarTopics, setSimilarTopics] = useState([]);
    const [loadingSimilarTopics, setLoadingSimilarTopics] = useState(false);

    const history = useHistory();
    const initTitle = useParam('init');
    const { t } = useTranslation(['dashboard']);
    const systemLanguage = useLanguage();
    const [language, setLanguage] = useState(systemLanguage);

    useEffect(
        () => { // load init title
            if (initTitle) {
                setTitle(initTitle);
            }
        },
        [initTitle]
    );

    const loadSimilarTopics = useCallback(
        (title, language) => {
            setSimilarTopics([]);
            cancelLoadSimilarTopics();
            if (title.length > 0) {
                setLoadingSimilarTopics(true);
                getSimilarTopics(
                    title,
                    language,
                    res => setSimilarTopics(res.data.results),
                    null,
                    () => setLoadingSimilarTopics(false)
                );
            }
        },
        []
    );


    useEffect(
        () => {
            loadSimilarTopics(title, language);
        },
        [title, language]
    );

    const checkEditable = useCallback((title, language) => {
        cancelIsEditableTopic();
        setEditableLoading(false);
        setIsEditable(null);
        if (title && title.length > 0) {
            setEditableLoading(true);
            getIsEditableTopic(
                title,
                language,
                res => {
                    var data = res.data;
                    setIsEditable(data.editable);
                    setEditableId(data.topic_id);
                    setEditableLoading(false);
                }
            )
        }
    }, []);

    const handleTitle = useCallback(
        e => {
            checkEditable(e.target.value, language);
            setTitle(e.target.value);
        },
        [language]
    );

    const handleSave = useCallback(() => {
        setLoading(true);
        doAddTopic(
            title, 
            language, 
            description, 
            res => {
                history.push(`/${systemLanguage}/dashboard/discovery/topics/${res.id}`)
            },
            null,
            () => setLoading(false))
    }, [title, description, history, language, systemLanguage]);

    const handleLanguage = useCallback(
        (lang) => {
            setLanguage(lang);
            if (initTitle === title) {
                checkEditable(title, lang)
            }
        },
        [initTitle, title, checkEditable]
    );

    return (
        <Grid container>
            <Grid item xs={12}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("Add a new topic")}
                            <Box component="span" style={{ float: "right" }} p={1}>
                                <IconButton size="small" title={t("Go back")} onClick={() => history.goBack()} style={{ color: "white" }}>
                                    <ArrowBackIcon />
                                </IconButton>
                            </Box>
                        </h3>
                    </CardHeader>
                    <CardBody>
                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }} className={classes.smallCenter}>
                            <StaticNote
                                title={<b>{t("Important Notes")}</b>}
                                id="discovery_add_topic"
                            >
                                <ul>
                                    <li>
                                        {t("A topic consist of a list of educational packages.")}
                                    </li>
                                    <li>
                                        {t("Each and every educational package must cover the topic by its own.")}
                                    </li>
                                </ul>
                            </StaticNote>
                        </Paper>
                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }} className={classes.smallCenter}>
                            <Grid container>
                                <Grid item xs={12} className={classes.formField}>
                                    <LangTextField
                                        value={title}
                                        onChange={handleTitle}
                                        label={t("Title")}
                                        fullWidth
                                        language={language}
                                        onLanguageChange={handleLanguage}
                                        disabled={loading}
                                    />
                                    {editableLoading && <LinearProgress />}
                                    {isEditable === false &&
                                        <Alert severity="error">
                                            {t("This topic already exists.")} (<Link to={`/${systemLanguage}/dashboard/discovery/topics/${editableId}`}>{t("View")}</Link>)
                                        </Alert>
                                    }
                                </Grid>
                                {(loadingSimilarTopics || similarTopics.length > 0) &&
                                    <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left', padding: "0 16px" }}>
                                        <IDiv
                                            loading={loadingSimilarTopics}
                                            loadingTexts={[
                                                t('Searching in eDoer...'),
                                            ]}
                                            title={t("We have similar items in the system.")}
                                            help={t("If the item you are going to add is among them, define it, or contribute to it if it has been already defined.")}
                                            items={similarTopics}
                                            formatItem={(to, index) => (
                                                <Chip
                                                    key={index}
                                                    style={{ margin: 4 }}
                                                    label={to.title}
                                                    variant="outlined"
                                                    color="primary"
                                                    title={to.title}
                                                    component={Link}
                                                    target="_blank"
                                                    to={`/${systemLanguage}/dashboard/discovery/topics/${to.id}`}
                                                    clickable
                                                />
                                            )}
                                        />
                                    </Grid>
                                }
                                {isEditable && (<>
                                    <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                        <Description
                                            style={{ padding: "10px" }}
                                            label={t("Topic Description")}
                                            help={t("You can describe the topic here.")}
                                            value={description}
                                            onChange={e => setDescription(e.target.value)}
                                            placeholder={t('Describe the topic')}
                                            lang={language}
                                            keyword={title}
                                        />
                                    </Grid>

                                    <Grid item xs={12} className={classes.formField}>
                                        <Button fullWidth color="rose" onClick={handleSave} disabled={loading}>
                                            {t("Save")}
                                        </Button>
                                    </Grid>
                                </>)}
                            </Grid>
                        </Paper>
                    </CardBody>
                </Card>
            </Grid>
        </Grid>
    )
}

Add.propTypes = {

}

export default Add

