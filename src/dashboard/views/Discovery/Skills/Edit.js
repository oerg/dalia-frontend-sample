import { Box, Grid, IconButton, Input, LinearProgress, makeStyles, Paper } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { Alert, Autocomplete } from '@material-ui/lab'
import { withSnackbar } from 'notistack'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useHistory, useParams } from 'react-router-dom'
import { useLanguage } from '../../../../i18n'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import Button from '../../../components/CustomButton/Button'
import DiscoveryNav from '../../../components/DiscoveryNav'
import LangTextField from '../../../components/LangTextField'
import SkillLogo from '../../../components/Logos/Skill'
import { getSuggestionColor } from '../../helpers'
import { roseColor } from '../../styles/variables'
import Description from '../components/Description'
import { cancelSkillRecommendations, doEditSkill, getSkillOnly, getSkillRecommendations, uploadSkillPicture } from './utils'

const useStyles = makeStyles(theme => ({
    formField: {
        padding: theme.spacing(2),
        textAlign: 'center',
        position: 'relative',
    },
    listRoot: {
        border: '1px solid #EEE',
        borderRadius: '5px',
        textAlign: 'center',
        margin: `${theme.spacing(1)}px auto`,
        maxHeight: '300px',
        overflowY: 'auto',
        '& > li:nth-child(even)': {
            backgroundColor: '#CCC',
        }
    },
    smallCenter: {
        width: '60%',
        minWidth: '200px',
        margin: 'auto',
    },
    addTopicButton: {
        position: 'absolute',
        color: roseColor[0],
        bottom: 0,
        zIndex: 999,
    },
    toRight: {
        right: 10,
    },
    addTopicForm: {
        padding: theme.spacing(2),
        color: '#FFF',
        position: 'absolute',
        zIndex: 998,
        bottom: -30,
        right: 55,
        left: 30,
    },
    suggestions: {
        border: '1px solid ' + getSuggestionColor(0.65),
        color: getSuggestionColor(0.65),
        margin: theme.spacing(2),
        position: 'relative',
        maxHeight: 250,
        minHeight: 80,
        overflowY: 'auto',
        '& > *': {
            margin: theme.spacing(0.5),
        }
    },
    suggestionsLoading: {
        position: 'absolute',
        width: 35,
        height: 35,
        top: 5,
        right: 15,
    },
    suggestionsHelp: {
        position: 'absolute',
        width: 35,
        height: 35,
        right: 12,
        top: 8,
        color: "#888",
        opacity: 0.7,
        cursor: 'help',
        transition: 'opacity 1s, top 0.5s',
        '&:hover': {
            opacity: 0.9
        }
    },
    suggestion: {
        backgroundColor: getSuggestionColor(0.65),
        borderColor: getSuggestionColor(0.65),
    }
}))

function Edit({ navHistory, enqueueSnackbar, ...props }) {
    const classes = useStyles();
    const [title, setTitle] = useState('');
    const [initTitle, setInitTitle] = useState('');
    const [loading, setLoading] = useState(false);
    const [skillRecommendations, setSkillRecommendations] = useState([]);
    const [skillRecommendationsLoading, setSkillRecommendationsLoading] = useState(false);
    const [titleInput, setTitleInput] = useState('');
    const [isEditable, setIsEditable] = useState(true);
    const [description, setDescription] = useState('');
    const [changed, setChanged] = useState(false);
    const [picture, setPicture] = useState(undefined)
    const { t } = useTranslation(['dashboard']);
    const systemLanguage = useLanguage();
    const [language, setLanguage] = useState(systemLanguage);

    const history = useHistory();

    const { id } = useParams();

    useEffect(
        () => {
            setLoading(true);
            getSkillOnly(
                id,
                res => {
                    var skill = res['data'];
                    setIsEditable(skill['is_admin'] || skill['is_contributor'] || skill.is_expert);
                    setTitle(skill['title']);
                    setInitTitle(skill['title']);
                    setDescription(skill['description']);
                    setLanguage(skill.lang);
                },
                null,
                () => setLoading(false)
            )
        },
        [id]
    )

    const handleTitle = useCallback((e, value) => {
        setTitle(value);
        setChanged(true);
    }, []);

    const handleTitleInput = useCallback((e, value) => {
        setTitleInput(value);

        if (value.length === 0) {
            setSkillRecommendationsLoading(false);
            setSkillRecommendations([]);
            cancelSkillRecommendations();
        } else {
            setSkillRecommendationsLoading(true);
            getSkillRecommendations(value,
                res => {
                    setSkillRecommendations(res.data);
                    setSkillRecommendationsLoading(false);
                }
            );
        }
    }, []);

    const handleTitleBlur = useCallback(
        () => {
            setTitle(titleInput);
            setChanged(true);
        },
        [titleInput]
    );

    const handleSave = useCallback(() => {
        setLoading(true);
        doEditSkill(id, title, language, description, res => {
            setLoading(false);
            history.push(`/${systemLanguage}/dashboard/discovery/skills/` + id)
        })
    }, [id, title, language, systemLanguage, description]);

    const handleLanguage = useCallback(
        (lang) => {
            setLanguage(lang);
            setChanged(true);
        },
        []
    );

    const handleDescription = useCallback(
        e => {
            setDescription(e.target.value)
            setChanged(true)
        },
        []
    );

    const handleFile = useCallback(
        e => {
            if (!e.target.files[0] || e.target.files[0].size > 2097152) { // 2MB
                enqueueSnackbar(
                    t("Please select a file which is not larger than 2 MB"),
                    {
                        varaint: "warning"
                    }
                )
                return;
            }
            var data = new FormData();
            data.append('file', e.target.files[0]);
            data.append('skill_id', id);
            uploadSkillPicture(
                data,
                res => {
                    setPicture(res.picture)
                    enqueueSnackbar(
                        t("Course picture updated successfully."),
                        {
                            variant: "success"
                        }
                    )
                },
                () => {
                    enqueueSnackbar(
                        t("Upload failed. Please try again."),
                        {
                            variant: "error"
                        }
                    )
                }
            )
        },
        [t, id, enqueueSnackbar]
    );

    return (
        <Grid container>
            <Grid item xs={12}>
                <DiscoveryNav navHistory={navHistory} />
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("Editing Course")}: <em>{initTitle}</em>
                            <Box component="span" style={{ float: "right" }} p={1}>
                                <IconButton size="small" title={t("Go back")} onClick={() => history.goBack()} style={{ color: "white" }}>
                                    <ArrowBackIcon />
                                </IconButton>
                            </Box>
                        </h3>
                    </CardHeader>
                    <CardBody>
                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }} className={classes.smallCenter}>
                            <Grid container>
                                <Grid item xs={12} className={classes.formField}>
                                    <Autocomplete options={skillRecommendations} value={title} onChange={handleTitle} disabled={loading || !isEditable}
                                        freeSolo handleHomeEndKeys disableClearable loading={skillRecommendationsLoading}
                                        renderInput={(params) =>
                                            <LangTextField
                                                label={t("Course Title")}
                                                margin="normal"
                                                fullWidth
                                                error={!isEditable}
                                                language={language}
                                                onLanguageChange={handleLanguage}
                                                {...params}
                                            />}
                                        onInputChange={handleTitleInput} inputValue={titleInput} onBlur={handleTitleBlur}
                                    />
                                    {loading && <LinearProgress />}
                                    {!isEditable &&
                                        <Alert severity="error">
                                            {t("You cannot edit this course.")} (<Link to={`/${systemLanguage}/dashboard/discovery/skills/${id}`}>{t("View")}</Link>)
                                        </Alert>
                                    }
                                </Grid>
                                <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left', display: "flex" }}>
                                    <SkillLogo src={picture} style={{ marginRight: 10 }} />
                                    <Input
                                        id="upload-button"
                                        type="file"
                                        onChange={handleFile}
                                        fullWidth
                                        placeholder={t("Picture")}
                                        inputProps={{
                                            accept: "image/png, image/jpeg"
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                    <Description
                                        style={{ padding: "10px" }}
                                        label={t("Course Description")}
                                        help={t("You can describe the course here. For example, you may explain the objective, importance, application, etc. of this course.")}
                                        value={description}
                                        onChange={handleDescription}
                                        loading={loading}
                                        TextFieldProps={{
                                            disabled: !isEditable
                                        }}
                                        placeholder={t('Describe the course')}
                                        lang={language}
                                        keyword={title}
                                    />
                                </Grid>
                                <Grid item xs={12} className={classes.formField}>
                                    <Button color="rose" onClick={handleSave} disabled={loading || !isEditable || !changed || title.length === 0}
                                        fullWidth>{t("Save")}</Button>
                                </Grid>
                            </Grid>
                        </Paper>
                    </CardBody>
                </Card>
            </Grid>
        </Grid>
    )
}

Edit.propTypes = {

}

export default withSnackbar(Edit)