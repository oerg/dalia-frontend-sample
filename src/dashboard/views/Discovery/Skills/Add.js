import { Box, Button as MuiButton, Chip, Grid, IconButton, LinearProgress, makeStyles, Paper, Typography } from '@material-ui/core'
import { LaunchOutlined } from '@material-ui/icons'
import AddIcon from '@material-ui/icons/Add'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { Alert, Autocomplete } from '@material-ui/lab'
import arrayMove from 'array-move'
import clsx from 'classnames'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useHistory } from 'react-router-dom'
import { useLanguage } from '../../../../i18n'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import Button from '../../../components/CustomButton/Button'
import Help from '../../../components/Help'
import IDiv from '../../../components/ISys/IDiv'
import LangTextField from '../../../components/LangTextField'
import ListDnd from '../../../components/ListDnd/ListDnd'
import { StaticNote } from '../../../components/Notes'
import TitleConfirmDialog from '../../../components/TitleConfirmDialog'
import { getSuggestionColor, getUrlParameter, useParam } from '../../helpers'
import { roseColor } from '../../styles/variables'
import Description from '../components/Description'
import { getIsEditableTopic } from '../Topics/utils'
import AddTopicWindow from './components/AddTopicWindow'
import { cancelIsEditableSkill, cancelLoadSimilarSkills, cancelRelatedWords, cancelSkillRecommendations, doAddSkill, getIsEditableSkill, getRelatedWords, getSkillOnly, getSkillRecommendations, loadSimilarSkills as getSimilarSkills } from './utils'

const useStyles = makeStyles(theme => ({
    formField: {
        padding: theme.spacing(2),
        textAlign: 'center',
        position: 'relative',
    },
    listRoot: {
        border: '1px solid #EEE',
        borderRadius: '5px',
        textAlign: 'center',
        margin: `${theme.spacing(1)}px auto`,
        maxHeight: '300px',
        overflowY: 'auto',
        '& > div > div:nth-child(odd)': {
            backgroundColor: '#CCC',
        },
    },
    dragClass: {
        backgroundColor: "#EEE"
    },
    smallCenter: {
        width: '60%',
        minWidth: '200px',
        margin: 'auto',
    },
    addTopicButton: {
        position: 'absolute',
        color: roseColor[0],
        bottom: 0,
        zIndex: 999,
    },
    toRight: {
        right: 10,
    },
    addTopicForm: {
        padding: theme.spacing(2),
        color: '#FFF',
        position: 'absolute',
        zIndex: 998,
        bottom: -30,
        right: 55,
        left: 30,
    },
    suggestionsLoading: {
        position: 'absolute',
        width: 35,
        height: 35,
        top: 5,
        right: 15,
    },
    suggestionsHelp: {
        position: 'absolute',
        width: 35,
        height: 35,
        right: 12,
        top: 8,
        color: "#888",
        opacity: 0.7,
        cursor: 'help',
        transition: 'opacity 1s, top 0.5s',
        '&:hover': {
            opacity: 0.9
        }
    },
    suggestion: {
        backgroundColor: getSuggestionColor(0.65),
        borderColor: getSuggestionColor(0.65),
        "&:focus": {
            backgroundColor: `${getSuggestionColor(0.65)} !important`,
        }
    },
    toggleDivTitle: {
        textAlign: "left"
    },
    suggestionChip: {
        margin: 4,
    }
}))

function Add({ navHistory, ...props }) {
    const classes = useStyles();
    const [selectedTopics, setSelectedTopics] = useState([]);
    const [loading, setLoading] = useState(false);
    const [showForm, setShowForm] = useState(false);
    const [suggestions, setSuggestions] = useState([]);
    const [suggestionsLoading, setSuggestionsLoading] = useState(false);
    const [skillRecommendations, setSkillRecommendations] = useState([]);
    const [skillRecommendationsLoading, setSkillRecommendationsLoading] = useState(false);
    const [titleInput, setTitleInput] = useState('');
    const [isEditable, setIsEditable] = useState(null);
    const [editableId, setEditableId] = useState(null);
    const [editableLoading, setEditableLoading] = useState(false);
    const [description, setDescription] = useState('');

    const [showTopicConfirm, setShowTopicConfirm] = useState(false);
    const [topicConfirmText, setTopicConfirmText] = useState('');
    const [topicConfirmIndex, setTopicConfirmIndex] = useState(-1);
    const [topicConfirmIsRename, setTopicConfirmIsRename] = useState(false)
    const [similarSkills, setSimilarSkills] = useState([]);
    const [loadingSimilarSkills, setLoadingSimilarSkills] = useState(false);
    const [topicIsEditableLoading, setTopicIsEditableLoading] = useState(false)

    const initTitle = useParam('init')
    const history = useHistory();
    const { t } = useTranslation(['dashboard']);
    const systemLanguage = useLanguage();
    const [language, setLanguage] = useState(systemLanguage);

    const urlId = getUrlParameter('id');

    const formatTopic = useCallback(
        (t) => {
          if (t.isEditable)
              return t.title
          else
              return <>
                      {t.title} &nbsp;
                      <Chip
                          icon={<LaunchOutlined />}
                          style={{
                              backgroundColor: "#e8f4fd"
                          }}
                          clickable
                          component={Link}
                          target="_blank"
                          onClick={(e) => e.stopPropagation()}
                          to={`/${language}/dashboard/discovery/topics/${t.topicId}`}
                          label={"already exists"}
                      />
                  </>
        },
        [],
      )

    useEffect(
        () => { // load init title
            if (initTitle) {
                setTitleInput(initTitle);
                checkEditable(initTitle);
            }
        },
        [initTitle]
    );

    const loadSimilarSkills = useCallback(
        (title, language) => {
            setSimilarSkills([]);
            cancelLoadSimilarSkills();
            if (title.length > 0 && language) {
                setLoadingSimilarSkills(true);
                getSimilarSkills(
                    title,
                    language,
                    res => setSimilarSkills(res.data.results.filter(s => s.title !== title)),
                    null,
                    () => setLoadingSimilarSkills(false)
                );
            }
        },
        []
    );


    useEffect(
        () => {
            loadSimilarSkills(titleInput, language);
        },
        [titleInput, language]
    );

    useEffect(() => { // load suggestions
        setSuggestions([]);
        cancelRelatedWords();
        setSuggestionsLoading(false);
        setSelectedTopics([]);

        if (titleInput.length > 0) {
            setSuggestionsLoading(true);
            getRelatedWords(titleInput, language,
                (res) => {
                    setSuggestionsLoading(false);
                    setSuggestions(res.data);
                },
                () => setSuggestionsLoading(false)
            );
        }
    }, [titleInput, language]);

    const checkEditable = useCallback((title, language) => {
        cancelIsEditableSkill();
        setEditableLoading(false);
        setIsEditable(null);
        if (title.length > 0 && language) {
            setEditableLoading(true);
            getIsEditableSkill(
                title,
                language,
                res => {
                    var data = res.data;
                    setIsEditable(data.editable);
                    setEditableId(data['skill_id']);
                    setEditableLoading(false);
                }
            )
        }
    }, []);

    useEffect(
        () => { // load all the topics
            if (urlId) {
                getSkillOnly(
                    urlId,
                    res => {
                        checkEditable(res['data']['title'], res.data.lang);
                        setTitleInput(res['data']['title']);
                        setDescription(res['data']['description']);
                        setSelectedTopics(
                            res['data']['topics'].map(t => t['title'])
                        );
                        setLanguage(res.data.lang);
                    }
                )
            }
        },
        [urlId]
    );

    const handleTitle = useCallback((e, value) => {
        setTitleInput(value);
    }, []);

    const handleLanguage = useCallback(
        (lang) => {
            setLanguage(lang);
        },
        []
    );
    useEffect(
        () => {
            setSelectedTopics([]);
            checkEditable(titleInput, language);
        },
        [checkEditable, titleInput, language]
    );

    const handleTitleInput = useCallback((e, value) => {
        setTitleInput(value);
        cancelSkillRecommendations();

        if (value.length === 0) {
            setSkillRecommendationsLoading(false);
            setSkillRecommendations([]);
        } else {
            setSkillRecommendationsLoading(true);
            getSkillRecommendations(value,
                res => {
                    setSkillRecommendations(res.data);
                    setSkillRecommendationsLoading(false);
                }
            );
        }
    }, [])

    const handleAddTopic = useCallback(
        (topic) => {
            setTopicIsEditableLoading(true)
            getIsEditableTopic(
                topic,
                language,
                res => {
                    setSelectedTopics(l => {
                        if (l.some(t => t.title === topic))
                            return l;
                        return [...l, {title: topic, isEditable: res.data.editable, topicId: res.data.topic_id}];
                    })
                    setTopicIsEditableLoading(false)
                }
            )
            setShowForm(false);
        },
        [language]
    );

    const handleCancelAddTopic = useCallback(
        () => setShowForm(false),
        []
    )

    const handleRemove = useCallback(
        (index) => {
            setSelectedTopics(topics => {
                topics.splice(index, 1);
                return [...topics];
            })
        },
        []
    )

    const handleTopicMove = useCallback(
        (fromIndex, toIndex) => {
            setSelectedTopics(
                topics => arrayMove(topics, fromIndex, toIndex)
            )
        },
        []
    )

    const handleSave = useCallback(() => {
        setLoading(true);
        doAddSkill(
            urlId, 
            titleInput, 
            language, 
            description, 
            selectedTopics.map(t => t.title), 
            data => {
                history.push(`/${systemLanguage}/dashboard/discovery/skills/${data.id}`)
            },
            null,
            () => setLoading(false)
        )
    }, [urlId, titleInput, language, description, selectedTopics])

    const handleTopicWindow = useCallback(() => {
        setShowForm(s => {
            return !s;
        });
    }, []);

    const handleChip = useCallback((index, str) => () => {
        setTopicConfirmText(str);
        setTopicConfirmIndex(index);
        setShowTopicConfirm(true);
    }, []);

    const handleTopicConfirmCancel = useCallback(
        () => setShowTopicConfirm(false),
        []
    );

    const handleTopicConfirmSelect = useCallback(
        topic => {
            setTopicIsEditableLoading(true)
            getIsEditableTopic(
                topic,
                language,
                res => {
                    if (topicConfirmIsRename) {
                        setSelectedTopics(
                            topics => topics.map(
                                (t, index) =>
                                    index === topicConfirmIndex ?
                                        {
                                            title: topic,
                                            isEditable: res.data.editable,
                                            topicId: res.data.topic_id
                                        }
                                        :
                                        t
                            )
                        )
                        setTopicConfirmIsRename(false)
                    } else {
                        setSelectedTopics(topics => (
                            [...topics, {title: topic, isEditable: res.data.editable, topicId: res.data.topic_id}]
                        ));
                        setSuggestions(sug => {
                            if (sug[topicConfirmIndex] === topicConfirmText) {
                                sug.splice(topicConfirmIndex, 1);
                                return [...sug];
                            }
                            return sug;
                        });
                    }
                    setTopicIsEditableLoading(false)
                }
            )
            setShowTopicConfirm(false);
        },
        [topicConfirmIndex, topicConfirmText, topicConfirmIsRename, language]
    );

    const handleTopicClick = useCallback( // rename topic
        (index, item) => {
            setTopicConfirmIndex(index)
            setTopicConfirmText(item.title)
            setShowTopicConfirm(true)
            setTopicConfirmIsRename(true)
        },
        []
    )

    return (
        <Grid container>
            <Grid item xs={12}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("Add a new skill")}
                            <Box component="span" style={{ float: "right" }} p={1}>
                                <IconButton size="small" title={t("Go back")} onClick={() => history.goBack()} style={{ color: "white" }}>
                                    <ArrowBackIcon />
                                </IconButton>
                            </Box>
                        </h3>
                    </CardHeader>
                    <CardBody>
                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }} className={classes.smallCenter}>
                            <StaticNote
                                title={<b>{t("Important Notes")}</b>}
                                id="discovery_add_skill"
                            >
                                <ul>
                                    <li>
                                        {t("Courses includes a list of topics, each topic will be covered by educational packages.")}
                                    </li>
                                    <li>
                                        {t("A topic is the smallest meaningful learning component of a course.")}
                                    </li>
                                </ul>
                            </StaticNote>
                        </Paper>
                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }} className={classes.smallCenter}>
                            <Grid container>
                                <Grid item xs={12} className={classes.formField}>
                                    <Autocomplete options={skillRecommendations} value={titleInput} onChange={handleTitle} disabled={loading || initTitle || urlId}
                                        freeSolo handleHomeEndKeys disableClearable loading={skillRecommendationsLoading}
                                        renderInput={(params) =>
                                            <LangTextField
                                                label={t("Course Title")}
                                                margin="normal"
                                                fullWidth
                                                insideAutocomplete
                                                error={isEditable === false}
                                                {...params}
                                                language={language}
                                                onLanguageChange={handleLanguage}
                                                autoFocus
                                            />
                                        }
                                        onInputChange={handleTitleInput} inputValue={titleInput}
                                    />
                                    {(editableLoading || loadingSimilarSkills) && <LinearProgress />}
                                    {isEditable === false &&
                                        <Alert severity="error">
                                            {t("This course already exists.")} (<Link to={`/${systemLanguage}/dashboard/discovery/skills/${editableId}`}>{t("View")}</Link>)
                                        </Alert>
                                    }
                                    {(loadingSimilarSkills || similarSkills.length > 0) &&
                                        <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left', padding: "0 16px" }}>
                                            <IDiv
                                                loading={loadingSimilarSkills}
                                                loadingTexts={[
                                                    t('Searching in eDoer...'),
                                                ]}
                                                title={t("We have similar items in the system.")}
                                                help={t("If the item you are going to add is among them, define it, or contribute to it if it has been already defined.")}
                                                items={similarSkills}
                                                formatItem={(sk, index) => (
                                                    <Chip
                                                        key={index}
                                                        style={{ margin: 4 }}
                                                        label={sk.title}
                                                        variant="outlined"
                                                        color="primary"
                                                        title={sk.title}
                                                        component={Link}
                                                        target="_blank"
                                                        to={`/${systemLanguage}/dashboard/discovery/skills/${sk.id}`}
                                                        clickable
                                                    />
                                                )}
                                            />
                                        </Grid>
                                    }

                                </Grid>
                                {isEditable && (<>
                                    <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                        <Description
                                            style={{ padding: "10px" }}
                                            label={t("Course Description")}
                                            help={t("You can describe the course here. For example, you may explain the objective, importance, application, etc. of this course.")}
                                            value={description}
                                            onChange={e => setDescription(e.target.value)}
                                            placeholder={t('Describe the course')}
                                            lang={language}
                                            keyword={titleInput}
                                        />
                                    </Grid>
                                    <Grid item xs={12} className={classes.formField} style={{ textAlign: "left" }}>
                                        <Paper>
                                            <Typography style={{ textAlign: 'left', padding: 10 }}>
                                                {t("Topics to be covered")}
                                                <Help>
                                                    {t("Here you can define the topics that will be covered through this course.")} <br />
                                                    {t("In order to define the topics, you are able to use the Related Topics below, or directly type by pressing Add Topic button.")}
                                                </Help>
                                            </Typography>
                                            {(suggestionsLoading || suggestions.length > 0) && (
                                                <Grid item xs={12} className={clsx(classes.formField)} style={{ textAlign: 'left' }}>
                                                    <IDiv
                                                        loading={suggestionsLoading}
                                                        loadingTexts={[
                                                            t('Collecting educational material for this course...'),
                                                            t('Analyzing the collected materials...'),
                                                            t('Extracting topics from educational materials...'),
                                                        ]}
                                                        title={t("System has found related topics.")}
                                                        help={<>{t("These are Computer Generated suggestions for the topics in this course.")} <br /> {t("These suggestions are extracted based on Youtube playlists and wikipedia pages related to the course you have chosen.")}</>}
                                                        items={suggestions}
                                                        formatItem={(s, index) => (
                                                            <Chip
                                                                label={s}
                                                                tabIndex="-1"
                                                                variant="outlined"
                                                                color="primary"
                                                                clickable
                                                                key={index}
                                                                onClick={handleChip(index, s)}
                                                                title={t("Add to topics")}
                                                                className={classes.suggestionChip}
                                                            />

                                                        )}
                                                    />
                                                </Grid>
                                            )}
                                            <ListDnd
                                                className={classes.listRoot}
                                                items={selectedTopics}
                                                onRemove={handleRemove}
                                                onMove={handleTopicMove}
                                                dragClass={classes.dragClass}
                                                emptyText={t("No topics have been added to this course yet.")}
                                                onClick={handleTopicClick}
                                                itemTitle={t("Click to rename")}
                                                formatLabel={formatTopic}
                                                loadingLastItem={topicIsEditableLoading}
                                            />
                                            <MuiButton
                                                fullWidth
                                                style={{
                                                    marginTop: -10,
                                                }}
                                                onClick={handleTopicWindow}
                                                startIcon={<AddIcon />}
                                            >
                                                {t("Add Topic")}
                                            </MuiButton>
                                            <AddTopicWindow
                                                open={showForm}
                                                loading={loading}
                                                onAdd={handleAddTopic}
                                                onCancel={handleCancelAddTopic}
                                                disabledSave={loading}
                                            />
                                        </Paper>
                                    </Grid>
                                    <Grid item xs={12} className={classes.formField}>
                                        <Button color="rose" onClick={handleSave} disabled={loading || selectedTopics.length === 0 || titleInput.length === 0}
                                            fullWidth>{t("Save")}</Button>
                                    </Grid>
                                </>)}
                            </Grid>
                        </Paper>
                    </CardBody>
                </Card>
                <TitleConfirmDialog
                    title={topicConfirmText} open={showTopicConfirm} onCancel={handleTopicConfirmCancel}
                    onSelect={handleTopicConfirmSelect} label={t("Confirm topic's title:")}
                    okButtonLabel={topicConfirmIsRename ? t("Rename") : t("Add")}
                />
            </Grid>
        </Grid>
    )
}

Add.propTypes = {

}

export default Add

