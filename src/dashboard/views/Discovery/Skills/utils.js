import { Typography } from '@material-ui/core';
import Axios from 'axios';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { MultiCall, requestAddHandlers } from '../../helpers';

export function getSkill(id, onLoad, onError) { // get topic from the server
    const skillReq = Axios.get('/skills/' + id);
    const suggestionsReq = Axios.get('/suggestions?object_type=SK&object_id=' + id + '&ordering=-created_at');
    Axios.all([skillReq, suggestionsReq])
        .then(Axios.spread((...res) => {
            onLoad(res[0].data, res[1].data);
        }))
        .catch(err => {
            onError && onError(err.response);
        });
}

export function getSkillOnly(id, onLoad, onError, onEnd) {
    Axios.get('/skills/' + id)
        .then(
            res => onLoad && onLoad(res)
        )
        .catch(
            err => {
                onError && onError(err.response);
                err.response.result && alert(err.response.result)
            }
        )
        .finally(
            () => onEnd && onEnd()
        )
}

export function createSortString(sortBy, sortAD) {
    let sortStr = sortAD ? '+' : '-';
    sortStr += sortBy === 'date' ? 'created_at' : 'upvote_count'
    return sortStr;
}

export function doTopicVote(skillId, topicId, vote, onSuccess, onError) { // send topics' vote to server.
    Axios.post('/skills/add-skill-topic-vote/', {
        skill: skillId,
        topic: topicId,
        vote: vote,
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
        .catch(res => {
            onError ? onError(res.response.data) : alert(res.response.data.result);
        })
}

export function doSuggestionVote(id, vote, onSuccess, onError) { // send topics' vote to server.
    Axios.post('/suggestions/add-suggestion-vote/', {
        suggestion: id,
        vote: vote,
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
        .catch(res => {
            onError ? onError(res.response.data) : alert(res.response.data.result);
        })
}

export function doJobVote(skillId, jobId, vote, onSuccess, onError) { // send jobs' vote to server.
    Axios.post('/jobs/add-job-skill-vote/', {
        skill: skillId,
        job: jobId,
        vote: vote,
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
        .catch(res => {
            onError ? onError(res.response.data) : alert(res.response.data.result);
        })
}

export function processVote(list, id, up, down, selfVote, idField = 'id') { // update local list of votes
    let data = list.find(t => t[idField] === id)
    data.vote = selfVote;
    data.upvote_count = up;
    data.downvote_count = down;
    return [...list];
}

export function addDeleteSuggestion(skillId, topicId, onSuccess) { // send request to server
    Axios.post('/suggestions/', {
        object_type: 'SK',
        object_id: skillId,
        payload: {
            suggestion_type: "DE",
            topic_id: topicId,
        }
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
}

export function addNewSuggestion(skillId, data, onSuccess) { // send request to server
    Axios.post('/suggestions/', {
        object_type: 'SK',
        object_id: skillId,
        payload: Object.assign(data, {
            suggestion_type: 'AD'
        })
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
}

export function addEditSkillSuggestion(skillId, skillTitle, onSuccess) { // send request to server
    Axios.post('/suggestions/', {
        object_type: 'SK',
        object_id: skillId,
        payload: {
            suggestion_type: "ES",
            skill_title: skillTitle,
        }
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
}

export function addEditSuggestion(skillId, topicId, data, onSuccess) { // send request to server 
    Axios.post('/suggestions/', {
        object_type: 'SK',
        object_id: skillId,
        payload: Object.assign(data, {
            suggestion_type: "RO",
            topic_id: topicId
        })
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
}

export function filterSuggestions(suggestions) {
    const add = [] // add
    const edit = [] // edit
    const reorder = [] // reorder
    const remove = [] // delete
    suggestions.forEach(s => {
        if (s.decision === 'PE') { // only pendings
            switch (s.payload.suggestion_type) {
                case 'AD':
                    add.push(s)
                    break;
                case 'RO':
                    reorder.push(s);
                    break;
                case 'DE':
                    remove.push(s);
                    break;
                default:
            }
        }
        return {
            add, edit, reorder, remove
        }
    });

    return { add, edit, reorder, remove }
}

export function doAddDiscussion(skillId, msg, parent, onSuccess, onError) {
    Axios.post('/discussions/', {
        text: msg,
        object_type: 'SK',
        object_id: skillId,
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
        .catch(err => {
            onError ? onError(err.response.data) : alert(err.response.data.result);
        })
}

export function formatJobTitle(j) {
    const { t } = useTranslation(['dashboard']);

    return (
        <span>
            {j.job_title}<br />
            <Typography variant="caption" title={t("Industry")}>{j.job_industry}</Typography>
        </span>
    )
}

export function getAllTopics(onSucces, onError) {
    Axios.get('/skills/topics/')
        .then(res => {
            onSucces && onSucces(res);
        })
        .catch(err => {
            err &&
                onError ? onError(err.response ? err.response.data : err) : alert(err.response.data.result);
        })
}

export function doAddSkill(id, title, lang, description, topics, onSucces, onError, onEnd) {
    return requestAddHandlers(
        Axios.post(
            '/skills/add-a-skill/', 
            {
                id, title, lang, topics, description
            }
        ),
        onSucces,
        onError,
        onEnd
    )
}

export function doEditSkill(id, title, lang, description, onLoad, onError, onEnd) {
    requestAddHandlers(
        Axios.put(
            '/skills/edit-skill-properties/',
            {
                skill_id: id,
                title,
                lang,
                description
            }
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function getRelatedWords(str, lang, onSucces, onError) {
    var mc = MultiCall.getInstance("AS_getRelatedWords");
    return mc.call(`/skills/get-related-words/?title=${str}&lang=${lang}`, onSucces, onError);
}

export function cancelRelatedWords() {
    return MultiCall.getInstance('AS_getRelatedWords').cancel();
}

export function getSkillRecommendations(search, onSucces, onError) {
    var mc = MultiCall.getInstance("AS_getRecom");
    return mc.call('/skills/get-existing-skills/?search=' + search, onSucces, onError);
}

export function cancelSkillRecommendations() {
    return MultiCall.getInstance("AS_getRecom").cancel();
}

export function getIsEditableSkill(title, lang, onSuccess, onError) {
    var mc = MultiCall.getInstance("AS_isEditable")
    return mc.call(`/skills/is-skill-editable/?title=${title}&lang=${lang}`, onSuccess, onError)
}

export function cancelIsEditableSkill() {
    return MultiCall.getInstance("AS_isEditable").cancel();
}

const SIMILAR_SKILLS_MC = "AS_similarSkillsLoading";
export function loadSimilarSkills(title, lang, onSuccess, onError, onEnd) {
    var mc = MultiCall.getInstance(SIMILAR_SKILLS_MC)
    return mc.call(`/skills/search/?search-term=${title}&lang=${lang}`, onSuccess, onError, onEnd)
}

export function cancelLoadSimilarSkills() {
    return MultiCall.getInstance(SIMILAR_SKILLS_MC).cancel();
}

export function uploadSkillPicture(data, onLoad, onError, onEnd) {
    return requestAddHandlers(
        Axios.post('/skills/upload-item-picture/', data),
        onLoad,
        onError,
        onEnd
    )
}