import { Dialog, DialogActions, DialogContent } from '@material-ui/core'
import React, { useCallback, useState } from 'react'
import { useTranslation } from 'react-i18next'
import TopicInput from '../../../../components/SystemInputs/TopicInput'
import Button from '../../../../components/CustomButton/Button'

function AddTopicWindow({ loading, disabledSave, onCancel, onAdd, ...props }) {
    const { t } = useTranslation(['dashboard'])
    const [selectedTopic, setSelectedTopic] = useState("")

    const handleSelect = useCallback(
        (topic) => setSelectedTopic(topic),
        []
    )

    const handleAdd = useCallback(
        () => {
            onAdd && onAdd(selectedTopic)
        },
        [selectedTopic, onAdd]
    )

    return (
        <Dialog
            keepMounted={false}
            onClose={onCancel}
            {...props}
        >
            <DialogContent dividers style={{ minWidth: 400 }}>
                <TopicInput 
                    loading={loading}
                    fullWidth
                    onSelect={handleSelect}
                    label={t("Search/Add Topic")}
                />
            </DialogContent>
            <DialogActions>
                <Button color="danger" onClick={onCancel}>
                    {t("Cancel")}
                </Button>
                <Button color="rose" onClick={handleAdd} disabled={disabledSave || !selectedTopic} fullWidth>
                    {t("Add topic to the course")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default AddTopicWindow