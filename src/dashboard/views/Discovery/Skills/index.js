import { Box, CircularProgress, Grid, IconButton, Link as MuiLink, Paper } from '@material-ui/core'
import BuildIcon from '@material-ui/icons/Build'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload'
import EditIcon from '@material-ui/icons/Edit'
import { withSnackbar } from 'notistack'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useHistory, useParams } from 'react-router-dom'
import { useLanguage } from '../../../../i18n'
import { backend_base_url } from '../../../../settings'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import DiscoveryNav from '../../../components/DiscoveryNav'
import ExpertRequestButton from '../../../components/ExpertRequestButton'
import GridContainer from '../../../components/Grid/GridContainer'
import GridItem from '../../../components/Grid/GridItem'
import Help from '../../../components/Help'
import Icons from '../../../components/Icons'
import IAlert from '../../../components/ISys/IAlert'
import LangIcon from '../../../components/LangIcon'
import LoginWarning from '../../../components/LoginWarning'
import SkillLogo from '../../../components/Logos/Skill'
import ToggleDiv from '../../../components/ToggleDiv'
import TopicsDialog from '../../../components/TopicsDialog'
import UserTable from '../../../components/TopUsers/UserTable'
import VotingTable from '../../../components/Vote/VotingTable'
import WordSuggestion from '../../../components/WordSuggestion'
import useStyles from '../../styles/SkillsStyle'
import QuestionsTable from '../components/QuestionsTable'
import { addWordOpinion, loadTopContributors } from '../utils'
import TopicsTable from './TopicsTable'
import { addDeleteSuggestion, addEditSuggestion, addNewSuggestion, doJobVote, doSuggestionVote, doTopicVote, filterSuggestions, formatJobTitle, getSkill, processVote } from './utils'


function Skills({ requireLogin, navHistory, onNav, enqueueSnackbar, user }) {
    const classes = useStyles();
    const { id: strSkillId } = useParams();

    const skillId = useMemo(
        () => {
            if (strSkillId) {
                return parseInt(strSkillId);
            }
        },
        [strSkillId]
    )

    const [skill, setSkill] = useState(null);
    const [topics, setTopics] = useState([]);
    const [addSuggestions, setAddSuggestions] = useState([]);
    const [delSuggestions, setDelSuggestions] = useState([]);
    const [movSuggestions, setMovSuggestions] = useState([]);
    const [jobs, setJobs] = useState([]);
    const [topContributors, setTopContributors] = useState([])
    const [isLoading, setLoading] = useState(true);
    const [criticalError, setCriticalError] = useState(null);
    const [votingTopics, setVotingTopics] = useState(false);
    const [votingSuggestions, setVotingSuggestions] = useState(false);
    const [votingJob, setVotingJob] = useState(false);
    const [suggestionsOpen, setSuggestionsOpen] = useState(false);
    const [edoerSuggestionsOpen, setEdoerSuggestionsOpen] = useState(false)
    const [words, setWords] = useState([]);
    const [wordsLoading, setWordsLoading] = useState(false);
    const [addWordObject, setAddWordObject] = useState(null);
    const [showTopicsDialog, setShowTopicsDialog] = useState(false);
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const [reloadNeeded, setReloadNeeded] = useState(true);

    const history = useHistory();

    useEffect(() => { // load skill and suggestions
        if (!reloadNeeded) return;
        setLoading(true);
        getSkill(skillId, (data, suggestions) => { // load
            if (data.skilltopic_set.length === 0) { // new skill
                if (window.confirm(t("This course is available in the system but its definition is not completed. Do you wish to complete its definition?")))
                    history.replace(`/${language}/dashboard/discovery/skills/add/?id=${data['id']}`);
                else
                    history.goBack();
            } else {
                const suggs = filterSuggestions(suggestions.results);
                setTopics(data.skilltopic_set.sort((a, b) => a.order - b.order));
                setSkill(data);
                setJobs(data.jobskill_set.sort((a, b) => a.order - b.order));
                // setExtractedTopics(data.extracted_topics);
                setAddSuggestions(suggs.add);
                setDelSuggestions(suggs.remove);
                setMovSuggestions(suggs.reorder);
                setWords(data.extracted_words)
                setLoading(false);
            }
        }, error => { // on error
            if (error) {
                if (error.status === 404) {
                    setCriticalError("404: Course was not found!");
                } else if (error.status >= 500) {
                    setCriticalError(error.status + ": Server error!")
                } else
                    setCriticalError(error.data);
            }
            setLoading(false);
        });
        setReloadNeeded(false);
    }, [skillId, reloadNeeded, t])

    useEffect(() => { // load top contributors
        loadTopContributors(
            "SK",
            skillId,
            (res) => {
                setTopContributors(res)
            },
            null,
            null)
    }, [skillId])

    const handleTopicVote = useCallback((id) => (up, down, selfVote) => {
        if (requireLogin) return setShowLoginWarning(true);

        setVotingTopics(true);
        doTopicVote(skillId, id, selfVote, () => {
            setVotingTopics(false);
            setTopics(t => processVote(t, id, up, down, selfVote, 'topic'))
        });
    }, [skillId, requireLogin]);

    const handleSuggestionVote = useCallback((setter) => (id) => (up, down, selfVote) => {
        if (requireLogin) return setShowLoginWarning(true);

        setVotingSuggestions(true);
        doSuggestionVote(id, selfVote, () => {
            setVotingSuggestions(false);
            setter(s => processVote(s, id, up, down, selfVote))
        })
    }, [requireLogin]);

    const handleJobVote = useCallback((id) => (up, down, selfVote) => {
        if (requireLogin) return setShowLoginWarning(true);

        setVotingJob(true);
        doJobVote(skillId, id, selfVote, () => {
            setVotingJob(false);
            setJobs(j => processVote(j, id, up, down, selfVote, 'job'));
        })
    }, [skillId, requireLogin]);

    const handleDelete = useCallback((topicId) => { // deleting suggestions
        if (requireLogin) return setShowLoginWarning(true);

        setLoading(true);
        addDeleteSuggestion(skillId, topicId, () => {
            setLoading(false);
            setReloadNeeded(true);
        });
    }, [skillId, requireLogin])

    const handleNew = useCallback((data) => {
        if (requireLogin) return setShowLoginWarning(true);

        setAddWordObject(null); // clear add word
        setLoading(true);
        addNewSuggestion(skillId, data, () => {
            setLoading(false);
            setReloadNeeded(true);
            user.reload();
        });
    }, [skillId, requireLogin, user])

    const handleEdit = useCallback((topicId, data) => {
        if (requireLogin) return setShowLoginWarning(true);

        setLoading(true);
        addEditSuggestion(skillId, topicId, data, () => {
            setLoading(false);
            setReloadNeeded(true);
        });
    }, [skillId, requireLogin])

    const formatDelTitle = useCallback( // TODO fix
        s => {
            const topic = topics.find(t => t.topic === s.payload.topic_id)
            if (topic) {
                return <span>
                    {t("Delete:")} <Link to={`/${language}/dashboard/discovery/topics/${topic.topic}`} target="_blank"><i><b>{topic.topic_title}</b></i></Link>
                </span>
            } else {
                return <em>{t("This suggestion is already deleted.")}</em>
            }
        },
        [t, topics])

    const formatMovTitle = useCallback(s => {
        const topic = topics.find(t => t.topic === s.payload.topic_id);
        const prevTopicIndex = topics.findIndex(t => t.order > s.payload.topic_order);
        const prevTopic = topics[prevTopicIndex === -1 ? topics.length - 1 : prevTopicIndex - 1];

        return <span>
            Move <Link to={`/${language}/dashboard/discovery/topics/${topic.topic}`} target="_blank"><i><b>{topic.topic_title}</b></i></Link>
            {
                prevTopicIndex === 0 ?
                    <> to the top</>
                    :
                    <> after <i><b><Link to={`/${language}/dashboard/discovery/topics/${prevTopic.topic}`} target="_blank">{prevTopic.topic_title}</Link></b></i></>
            }
        </span>
    }, [topics])

    const formatAddTitle = useCallback(s => {
        const prevTopicIndex = topics.findIndex(t => t.order > s.payload.topic_order);
        const prevTopic = topics[prevTopicIndex === -1 ? topics.length - 1 : prevTopicIndex - 1];
        return <span>
            Add &nbsp;
            <i><b>
                {s.payload.skill_id ?
                    <Link to={`/${language}/dashboard/discovery/topics/${s.payload.topic_id}`} target="_blank">{s.payload.topic_title}</Link>
                    :
                    s.payload.topic_title
                }
            </b></i>
            {
                prevTopicIndex <= 0 ?
                    <> to the top</>
                    :
                    <> after <i><b><Link to={`/${language}/dashboard/discovery/topics/${prevTopic.topic}`} target="_blank">{prevTopic.topic_title}</Link></b></i></>
            }
        </span>

    }, [topics]);

    const formatJobLink = useCallback(job => {
        return `/${language}/dashboard/discovery/jobs/` + job.job
    }, []);

    const handleAddWord = useCallback(
        (id, word) => {
            setWordsLoading(true);
            addWordOpinion(
                id,
                "AD",
                () => {
                    setAddWordObject(word);
                    setWords(
                        words => words.filter(w => w.id !== id)
                    );
                },
                null,
                () => setWordsLoading(false)
            )
        },
        []
    )

    const handleIrrelaventWord = useCallback(
        (id) => {
            setWordsLoading(true);
            addWordOpinion(
                id,
                "IR",
                () => {
                    enqueueSnackbar(
                        t("Thank you for your feedback."),
                        {
                            variant: "info"
                        }
                    );
                    setWords(
                        words => words.filter(w => w.id !== id)
                    );
                },
                null,
                () => setWordsLoading(false)
            )
        },
        [t, enqueueSnackbar]
    )

    const handleAddedWord = useCallback(
        (id) => {
            setWordsLoading(true);
            addWordOpinion(
                id,
                "AA",
                () => {
                    enqueueSnackbar(
                        t("Thank you for your feedback."),
                        {
                            variant: 'info'
                        }
                    )
                    setWords(
                        words => words.filter(w => w.id !== id)
                    );
                },
                null,
                () => setWordsLoading(false)
            )
        },
        [t, enqueueSnackbar]
    )

    const handleNeutral = useCallback(
        (id) => {
            setWordsLoading(true);
            addWordOpinion(
                id,
                "NE",
                null,
                null,
                () => setWordsLoading(false)
            )
        },
        []
    );

    const handleAddQuestion = useCallback(
        () => {
            setShowTopicsDialog(true);
        },
        []
    );

    const handleTopicSelectionForQuestion = useCallback(
        (e, reason, topics) => {
            if (reason === "add") {
                history.push(
                    `/${language}/dashboard/discovery/questions/add`,
                    {
                        skillId,
                        topics: topics.map(
                            t => ({ id: t.topic, title: t.topic_title })
                        )
                    }
                )
            } else {
                setShowTopicsDialog(false);
            }
        },
        [history, skillId]
    );

    // login
    const [showLoginWarning, setShowLoginWarning] = useState(false);
    const handleLogin = useCallback((action) => {
        setShowLoginWarning(false);
        if (action) {
            history.push(action);
        }
    }, [history]);


    return (
        <>
            {requireLogin &&
                <LoginWarning show={showLoginWarning} onSelect={handleLogin} />
            }
            {isLoading ?
                <Box p={10} textAlign="center">
                    <CircularProgress />
                </Box>
                :
                criticalError ?
                    <Box p={10} textAlign="center">
                        {criticalError}
                    </Box>
                    :
                    <>
                        <DiscoveryNav skill={skill} onNav={onNav} navHistory={navHistory} />
                        <GridContainer>
                            <GridItem xs={12}>
                                <Card>
                                    <CardHeader color="rose" style={{ display: 'flex', alignItems: 'center' }}>
                                        <SkillLogo picture={skill.picture} size="large" style={{ marginRight: 10 }} />
                                        <h3 className={classes.cardTitleWhite}>
                                            {skill['title']} <LangIcon language={skill.lang} small />
                                            <Box component="span" ml={2}>
                                                {(skill.is_contributor || skill.is_admin || skill.is_expert) &&
                                                    <IconButton style={{ color: "white" }} title={t("Edit this course")} size="small" onClick={() => history.push(`/${language}/dashboard/discovery/skills/edit/${skill['id']}`)}>
                                                        <EditIcon />
                                                    </IconButton>
                                                }
                                            </Box>
                                            <Icons addable={skill.addable} learnable={skill.learnable} assessmentable={skill.assessmentable} withBorder={true} className={classes.lightBorders} isTopic={false} />
                                        </h3>
                                        <IconButton
                                            style={{ color: "white", marginRight: 20 }}
                                            component={MuiLink}
                                            target="_blank"
                                            href={`${backend_base_url}skills/download-json-export/?skill_id=${skill.id}`}
                                            title={t("Download the structure of this item as JSON")}
                                            size="small"
                                        >
                                            <CloudDownloadIcon />
                                        </IconButton>
                                        {skill.is_expert ?
                                            <div
                                                style={{
                                                    border: "1px solid #FFF",
                                                    borderRadius: 5,
                                                    width: 35,
                                                    height: 35,
                                                    display: "flex",
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                                }}
                                                title={t("You are an expert")}
                                            >
                                                <BuildIcon
                                                    htmlColor='#FFF'
                                                />
                                            </div>
                                            :
                                            <ExpertRequestButton object_id={skillId} object_type="SK" startIcon={<BuildIcon />} />
                                        }
                                    </CardHeader>
                                    <CardBody>
                                        {skill.description &&
                                            <Paper elevation={2} className={classes.descriptionBox}>
                                                <div>
                                                    {skill.description}
                                                </div>
                                            </Paper>}
                                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }}>
                                            <Grid container spacing={3} className={classes.hasPadding} alignContent="stretch">
                                                <Grid item xs={12} md={6} className={classes.col}>
                                                    <TopicsTable onVote={handleTopicVote} topics={topics} onDelete={handleDelete} onEdit={handleEdit}
                                                        disabled={votingTopics} onNew={handleNew} headClass={classes.addSugg}
                                                        addObject={addWordObject} isExpert={skill.is_expert || skill.is_admin} />
                                                    <VotingTable onVote={handleJobVote} data={jobs} formatTitle={formatJobTitle} disabled={votingJob} idField="job"
                                                        objectType="JO" title={t("Related Journeys")} height="100%" collapsable headClass={classes.delSugg}
                                                        formatLink={formatJobLink} style={{ marginBottom: 8, marginTop: 8 }} />
                                                    <UserTable data={topContributors} title={t("Top Contributors")}
                                                        height="100%" collapsable headClass={classes.delSugg} />
                                                </Grid>
                                                <Grid item xs={12} md={6} className={classes.col}>
                                                    <ToggleDiv
                                                        title={<b>{t("Suggestions")}</b>}
                                                        titleClassName={classes.suggestionsBox}
                                                        open={suggestionsOpen}
                                                        onChange={o => setSuggestionsOpen(o)}
                                                        help={
                                                            <Help>
                                                                {t("Here you can find:")}
                                                                <ul>
                                                                    <li>{t("eDoer Intelligence which you can validate, and add if they fit this page. Also, you can mark them as irrelevant or indicate if they already exist in this page.")}</li>
                                                                    <li>{t("Up-vote/Down-vote existing sugestions (i.e. Add, Delete, or Reorder). eDoer automatically decides to accept or reject the suggestions based on your votes.")}</li>
                                                                </ul>
                                                            </Help>
                                                        }
                                                        style={{
                                                            marginBottom: 8
                                                        }}>
                                                        <Paper
                                                            style={{
                                                                padding: 8
                                                            }}>
                                                            <ToggleDiv
                                                                title={<b>{t("eDoer Intelligence")}</b>}
                                                                titleClassName={classes.edoerSugg}
                                                                open={edoerSuggestionsOpen}
                                                                onChange={o => setEdoerSuggestionsOpen(o)}
                                                                help={
                                                                    <Help>
                                                                        {t("You can validate the eDoer's intelligent suggestions and add them if they fit this page. Also, you can mark them as irrelevant or indicate if they already exist in this page.")}
                                                                    </Help>
                                                                }
                                                                style={{
                                                                    marginBottom: 8
                                                                }}
                                                            >
                                                                {words.length > 0 ?
                                                                    <WordSuggestion
                                                                        words={words}
                                                                        pause={wordsLoading || !suggestionsOpen}
                                                                        disabled={wordsLoading}
                                                                        onAdd={handleAddWord}
                                                                        onIrrelavent={handleIrrelaventWord}
                                                                        onAlreadyAdded={handleAddedWord}
                                                                        onNeutral={handleNeutral}
                                                                    />
                                                                    :
                                                                    <>
                                                                        {new Date().getTime() - new Date(skill.created_at) < (30 * 60 * 1000) ?
                                                                            <Paper style={{ padding: 24, backgroundColor: "#e8f4fd" }}>
                                                                                <IAlert
                                                                                    show
                                                                                    transition={false}
                                                                                    style={{
                                                                                        border: "1px solid #2196f3",
                                                                                    }}
                                                                                >
                                                                                    {t("We are searching for suggestions to help you develop this page. Please check again this part in a few minutes.")}
                                                                                </IAlert>
                                                                            </Paper>
                                                                            :
                                                                            <Paper style={{ padding: 24, backgroundColor: "#e8f4fd" }}>
                                                                                <IAlert
                                                                                    show
                                                                                    transition={false}
                                                                                    style={{
                                                                                        border: "1px solid #2196f3",
                                                                                    }}
                                                                                >
                                                                                    {t("Currently, there is no eDoer-generated suggestion for this page.")}
                                                                                </IAlert>
                                                                            </Paper>
                                                                        }
                                                                    </>
                                                                }
                                                            </ToggleDiv>
                                                            <VotingTable onVote={handleSuggestionVote(setAddSuggestions)} data={addSuggestions} disabled={votingSuggestions} objectType="SU"
                                                                title={t("ADD")} className={classes.hasYMargin} formatTitle={formatAddTitle} headClass={classes.movSugg} />
                                                            <VotingTable onVote={handleSuggestionVote(setDelSuggestions)} data={delSuggestions} disabled={votingSuggestions} objectType="SU"
                                                                title={t("DELETE")} className={classes.hasYMargin} formatTitle={formatDelTitle} headClass={classes.movSugg} />
                                                            <VotingTable onVote={handleSuggestionVote(setMovSuggestions)} data={movSuggestions} disabled={votingSuggestions} objectType="SU"
                                                                title={t("REORDER")} className={classes.hasYMargin} formatTitle={formatMovTitle} headClass={classes.movSugg} />
                                                        </Paper>
                                                    </ToggleDiv>
                                                    <QuestionsTable
                                                        titleClassName={classes.suggestionsBox}
                                                        objectType="SK"
                                                        objectId={skillId}
                                                        linkExternalData={{
                                                            skillId,
                                                            skillTitle: skill.title
                                                        }}
                                                        showAdd={skill.is_expert || skill.is_admin}
                                                        onAddQuestion={handleAddQuestion}
                                                    />
                                                    <TopicsDialog
                                                        topics={topics}
                                                        open={showTopicsDialog}
                                                        onClose={handleTopicSelectionForQuestion}
                                                        titleField='topic_title'
                                                        idField='topic'
                                                        title={t("Select the topics this question is going to cover: ")}
                                                    />
                                                </Grid>
                                            </Grid>
                                        </Paper>

                                    </CardBody>
                                </Card>
                            </GridItem>
                        </GridContainer>
                    </>
            }
        </>
    )
}

Skills.defaultProps = {
    requireLogin: false,
}

export default withSnackbar(Skills)
