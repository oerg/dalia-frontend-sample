import Axios from 'axios';
import { requestAddHandlers } from '../../helpers';


export function getOer(id, onLoad, onError, onEnd) { // get topic from the server
    return requestAddHandlers(
        Axios.get('/oers/oers/' + id),
        onLoad,
        onError,
        onEnd
    );
}

export function updateOer(oer_id, data, onLoad, onError, onEnd) {
    return requestAddHandlers(
        Axios.put(
            '/oers/update-oer/',
            {
                oer_id,
                ...data,
            }
        ),
        onLoad,
        onError,
        onEnd
    );
}

export function formatLength(rawLength) {
    let hours = 0
    let mins = 0
    let seconds = 0
    if (rawLength) {
        hours = Math.floor(rawLength / 3600);
        mins = Math.floor((rawLength - (hours * 3600)) / 60)
        seconds = rawLength - (hours * 3600) - (mins * 60)
    }
    return { hours, minutes: mins, seconds }
}