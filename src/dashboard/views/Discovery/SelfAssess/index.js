import { Box, Button, ButtonGroup, CircularProgress, Paper, Snackbar, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import HelpIcon from '@material-ui/icons/Help';
import Cookies from 'js-cookie';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useParams } from 'react-router-dom';
import { useLanguage } from '../../../../i18n';
import Card from '../../../components/Card/Card';
import CardBody from '../../../components/Card/CardBody';
import CardHeader from '../../../components/Card/CardHeader';
import DiscoveryNav from '../../../components/DiscoveryNav';
import GridContainer from '../../../components/Grid/GridContainer';
import GridItem from '../../../components/Grid/GridItem';
import IAlert from '../../../components/ISys/IAlert';
import LangIcon from '../../../components/LangIcon';
import SkillLogo from '../../../components/Logos/Skill';
import ToggleDiv from '../../../components/ToggleDiv';
import useStyles from '../../styles/SkillsStyle';
import { getSkillOnly } from '../Skills/utils';
import QuestionList from './components/QuestionList';
import ScaleList from './components/ScaleList';
import { getSelfAssessment, saveScales } from './components/utils';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

const cookieCode = "help-d-1"

function SelfAssess({ navHistory, onNav, enqueueSnackbar, user, ...props }) {
    const classes = useStyles()
    const [scales, setScales] = useState([])
    const [description, setDescription] = useState("")
    const [questions, setQuestions] = useState([])

    const { id: strSkillId } = useParams()
    const { t } = useTranslation(['dashboard'])
    const language = useLanguage()
    const [loading, setLoading] = useState(true)
    const [saving, setSaving] = useState(false)
    const [skill, setSkill] = useState(null)
    const [showHelp, setShowHelp] = useState(false)

    useEffect(
        () => {
            const cookie = Cookies.get(cookieCode)
            if (!cookie) {
                const handle = window.setTimeout(
                    () => setShowHelp(true),
                    750
                )
                return () => window.clearTimeout(handle)
            }
        },
        []
    )

    const skillId = useMemo(
        () => {
            if (strSkillId) {
                return parseInt(strSkillId);
            }
        },
        [strSkillId]
    )

    useEffect(
        () => {
            setLoading(true)
            getSkillOnly(
                skillId,
                ({ data }) => {
                    setSkill(data)
                    getSelfAssessment(
                        skillId,
                        selfAssessment => {
                            setScales(selfAssessment.options && selfAssessment.options.length > 0 ? [...selfAssessment.options[0].options] : [])
                            setDescription(selfAssessment.options && selfAssessment.options.length > 0 ? selfAssessment.options[0].description : "")
                            setQuestions([...selfAssessment.questions])
                        },
                        null,
                        () => setLoading(false)
                    )
                }
            )

        },
        [skillId]
    )

    const handleChangeScales = useCallback(
        (scales, text) => {
            setSaving(true)
            saveScales(
                skillId,
                scales,
                text,
                () => {
                    setScales(scales)
                    setDescription(text)
                },
                null,
                () => setSaving(false)
            )
        },
        [skillId],
    )

    const handleDismissHelp = useCallback(
        () => {
            setShowHelp(false)
            Cookies.set(cookieCode, "1", {
                sameSite: 'Lax'
            })
        },
        []
    )


    return (loading ?
        <Box p={10} textAlign="center">
            <CircularProgress />
        </Box>
        :
        <>
            <DiscoveryNav skill={skill} onNav={onNav} navHistory={navHistory} />
            <GridContainer>
                <GridItem xs={12}>
                    <Card>
                        <CardHeader color="rose" style={{ display: 'flex', alignItems: 'center' }}>
                            <SkillLogo picture={skill.picture} size="large" style={{ marginRight: 10 }} />
                            <div className={classes.cardTitleWhite}>
                                <h3>
                                    {skill.title} <LangIcon language={skill.lang} small />
                                    <Typography variant="caption" title={t("Company")} className={classes.company}>
                                        - {t("Self Assessment Questions")}
                                    </Typography>
                                </h3>
                            </div>
                        </CardHeader>
                        <CardBody>
                            <Button
                                style={{
                                    marginRight: 10,
                                }}
                                component={Link}
                                to={`/${language}/dashboard/discovery/skills/${skillId}`}
                                variant="outlined"
                                startIcon={<ArrowBackIosIcon />}
                            >
                                {t("Back to Course")}
                            </Button>
                            <ToggleDiv
                                alwaysOpen
                                component={Paper}
                                title={<><b>{t("Description and options")}</b>: {t("The description and options for the self-assessment (max: 9 options).")}</>}
                                style={{
                                    margin: '0 auto 1em auto',
                                    padding: '0.5em',
                                    width: 675,
                                }}
                            >
                                <ScaleList
                                    scales={scales}
                                    description={description}
                                    onChange={handleChangeScales}
                                    loading={saving}
                                />
                            </ToggleDiv>

                            <ToggleDiv
                                component={Paper}
                                title={<><b>{t("Questions")}</b>: {t("The questions for the self-assessment of this course.")}</>}
                                style={{
                                    padding: '0.5em',
                                    width: 675,
                                    margin: '0 auto 1em auto',
                                }}
                            >
                                <QuestionList
                                    questions={questions}
                                    onChange={q => setQuestions(q)}
                                    loading={saving}
                                    skillId={skillId}
                                />
                            </ToggleDiv>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
            <Snackbar
                open={showHelp}
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "center"
                }}
                style={{
                    top: 40,
                    zIndex: 99
                }}
            >
                <IAlert
                    show
                    transition={false}
                    action={
                        <ButtonGroup color='inherit' variant='text'>
                            <Button onClick={handleDismissHelp} title={t("Dismiss")}>
                                <CloseIcon />
                            </Button>
                        </ButtonGroup>
                    }
                    style={{
                        border: "1px solid #2196f3",
                    }}
                    icon={<HelpIcon fontSize='inherit' />}
                >
                    {t("Defining a self-assessment consists of two stages:")}
                    <ol>
                        <li>
                            <b>{t("Defining options")}</b>:
                            <ol>
                                <li>{t("Press add and enter an option")}</li>
                                <li>{t("Repeat step 1 until you add all the options")}</li>
                                <li>{t("Press save")}</li>
                            </ol>
                        </li>
                        <li>
                            <b>{t("Defining questions")}</b>: {t("type the question, press the save button on top left corner of each question.")}
                            <ol>
                                <li>{t("Press add and enter the question")}</li>
                                <li>{t("Press save")}</li>
                                <li>{t("Repeat steps 1 & 2 until you enter all your questions")}</li>
                            </ol>                        
                        </li>
                    </ol>
                </IAlert>
            </Snackbar>
        </>
    )
}

export default SelfAssess