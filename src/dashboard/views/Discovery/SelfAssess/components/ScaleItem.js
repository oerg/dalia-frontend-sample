import { Button, Collapse, Input, makeStyles, Paper, Typography } from '@material-ui/core';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import clsx from 'classnames';

const useStyles = makeStyles({
    root: {
        width: 125,
        height: 125,
        position: "relative",
        display: "flex",
        flexDirection: "column",
    },
    order: {
        display: "block",
        height: 30,
        textAlign: "center",
        paddingTop: 10,
        borderBottom: "1px solid #CCC"
    },
    label: {
        flex: "1",
        height: "100%",
        marginBottom: 15,
        marginTop: 20,
        marginLeft: 5,
        marginRight: 5,
    },
    labelText: {
        height: 60,
        overflowY: "auto",
        textAlign: "center",
    },
    editButton: {
        position: "absolute",
        bottom: 1,
        right: 1,
    },
})

function ScaleItem({ label, order, onChange, onDelete, editing, className }) {
    const classes = useStyles()
    const { t } = useTranslation(['dashboard'])

    const handleChange = useCallback(
        e => {
            onChange && onChange(e.target.value, order)
        },
        [onChange, order]
    )

    const handleDelete = useCallback(
        () => {
            onDelete && onDelete(order)
        },
        [order]
    )

    return (
        <Paper elevation={1} square className={clsx(classes.root, className)}>
            <Typography variant='caption' className={classes.order}>
                {order}
            </Typography>
            <Typography variant='body2' className={classes.label} component="div">
                <Collapse in={editing}>
                    <Input
                        placeholder={t("New Label")}
                        value={label}
                        onChange={handleChange}
                    />
                    <Button
                        startIcon={<DeleteForeverIcon />}
                        title={t("Delete this option")}
                        fullWidth
                        onClick={handleDelete}
                    >
                        {t("Delete")}
                    </Button>
                </Collapse>
                <Collapse 
                    in={!editing} 
                    classes={{
                        wrapperInner: classes.labelText
                    }}
                >
                    <em>{label}</em>
                </Collapse>
            </Typography>
        </Paper>
    )
}

export default ScaleItem