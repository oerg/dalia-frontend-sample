import { Button, IconButton, makeStyles, Paper } from '@material-ui/core';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { ContentState, convertToRaw, EditorState } from 'draft-js';
import draftjsToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { useTranslation } from 'react-i18next';
import '../../../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { toolbar } from './utils';
import SaveIcon from '@material-ui/icons/Save';
import DragHandleIcon from '@material-ui/icons/DragHandle';
import { Draggable } from 'react-smooth-dnd';
import clsx from 'classnames';

const useStyles = makeStyles(theme => ({
    formField: {
        padding: theme.spacing(2),
        textAlign: 'left',
        position: 'relative',
    },
    editor: {
        paddingLeft: theme.spacing(1),
        paddingRight: theme.spacing(1),
    },
    titleWrapper: {
        display: "flex",
        padding: '1em',
        alignItems: "center",
        backgroundColor: "#CCC",
    },
    title: {
        flex: 1,
    },
    deleteIcon: {

    },
    dragHandle: {
        cursor: "grab",
        marginRight: "0.5rem"
    }
}))


const QuestionForm = ({ question, number, onSave, onDelete, canDelete = true, reorderHandleClassName }) => {
    const classes = useStyles();
    const [text, setText] = useState(EditorState.createEmpty());
    const [changed, setChanged] = useState(false);
    const { t } = useTranslation(['dashboard']);

    const RootComponent = useMemo(
        () => reorderHandleClassName ? Draggable : "div",
        [reorderHandleClassName]
    )

    useEffect(
        () => {
            if (question) {
                const blocks = htmlToDraft(question.text);
                setText(
                    EditorState.createWithContent(
                        ContentState.createFromBlockArray(
                            blocks.contentBlocks,
                            blocks.entityMap
                        )
                    )
                )
            }
        },
        [question]
    )

    const handleText = useCallback(
        (newText) => {
            setText(newText)
            setChanged(true)
        },
        []
    );

    const handleSave = useCallback(
        () => {
            var html = draftjsToHtml(convertToRaw(text.getCurrentContent()))
            onSave(number, question, html, () => setChanged(false))
        },
        [text, question, onSave]
    )

    const handleDelete = useCallback(
        () => onDelete(number),
        [number]
    )


    return (
        <RootComponent className={classes.formField}>
            <Paper>
                <div className={classes.titleWrapper}>
                    {reorderHandleClassName && <span title={t("Reorder this question")}>
                        <DragHandleIcon className={clsx(reorderHandleClassName, classes.dragHandle)} />
                    </span>}
                    <span className={classes.title}>
                        {t("Question")} {number}
                    </span>
                    {canDelete &&
                        <IconButton size="small" onClick={handleDelete} title={t("Delete this question")}>
                            <DeleteForeverIcon />
                        </IconButton>}
                    <Button onClick={handleSave} disabled={!changed} title={t("Save this question")} startIcon={<SaveIcon />}>
                        {t("Save")}
                    </Button>
                </div>
                <Editor
                    editorState={text}
                    onEditorStateChange={handleText}
                    editorClassName={classes.editor}
                    toolbar={toolbar}
                />
            </Paper>
        </RootComponent>
    )
}

export default QuestionForm
