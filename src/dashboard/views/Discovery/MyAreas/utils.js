import axios from "axios";
import { requestAddHandlers } from "../../helpers";
import React from 'react';
import { format, parseISO } from "date-fns";

export function getNonlearnableItems(onSuccess, onError) { // get job and suggestions from the server
    requestAddHandlers(
        axios.get("/mentoring/get-nonlearnable-items/"),
        onSuccess,
        onError
    );
}

export const renderGeneralCell = (params) => {
    const text = params.row[params.field]
    return (
        <span title={text} style={{textOverflow: "ellipsis", overflow: "hidden"}}>
            {text}
        </span>
    )
}

export const renderDateCell = (params) => {
    const date = parseISO(params.row[params.field])

    return format(date, "yyyy-MM-dd H:mm:ss")
}