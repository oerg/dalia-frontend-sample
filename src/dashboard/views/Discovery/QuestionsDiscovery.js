import { Box, Button, CircularProgress, Grid, IconButton, InputAdornment, List, ListItem, ListItemIcon, ListItemText, ListSubheader, makeStyles, Paper, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import ListAltIcon from '@material-ui/icons/ListAlt';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import { cancelBothSkillsTopics, loadBothSkillsTopics } from '../../components/SystemInputs/utils';
import { loadMyQuestions, loadQuestions } from './utils';
const useStyles = makeStyles(theme => ({
    wrapper: {
        width: '100%',
        backgroundColor: "#FFFFFF",
        marginTop: theme.spacing(1),
        paddingTop: theme.spacing(1),
    },
    hasPadding: {
        padding: theme.spacing(1),
    },
    loadingPanel: {
        textAlign: 'center',
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    searchWrapper: {
        padding: theme.spacing(1),
        maxHeight: 500,
        overflowY: 'auto',
        marginLeft: theme.spacing(1),
    },
    searchList: {
        backgroundColor: theme.palette.background.paper
    }
}));

function SubList({ title, items, selectedItem, onSelect }) {
    const classes = makeStyles({
        sameBg: {
            backgroundColor: 'inherit',
        }
    })();
    const handleClick = useCallback(
        item => () => {
            onSelect(item);
        },
        [onSelect]
    );

    return <li className={classes.sameBg}>
        <ul className={classes.sameBg} style={{ padding: 0 }}>
            <ListSubheader><b><i>{title}</i></b></ListSubheader>
            {items.map(
                (item, index) => (<ListItem key={index} button
                    selected={selectedItem && selectedItem.object_id === item.object_id && selectedItem.object_type === item.object_type}
                    onClick={handleClick(item)}
                >
                    <ListItemText primary={item.title} />
                    <ListItemIcon>
                        <KeyboardArrowRightIcon />
                    </ListItemIcon>
                </ListItem>)
            )}
        </ul>
    </li>;
}

function QuestionsDiscovery() {
    const classes = useStyles();
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();
    const [loading, setLoading] = useState(false);
    const [loadingQuestions, setLoadingQuestions] = useState(false);
    const [topics, setTopics] = useState([]);
    const [skills, setSkills] = useState([]);
    const [searchString, setSearchString] = useState('');
    const [selectedItem, setSelectedItem] = useState(null);
    const [questions, setQuestions] = useState(null);

    useEffect(() => { // do the search
        setSelectedItem(null);
        if (searchString.length === 0) {
            cancelBothSkillsTopics();
            setSkills([]);
            setTopics([]);
            setLoading(false);
        } else {
            setLoading(true);
            loadBothSkillsTopics(
                searchString,
                ({ data }) => {
                    setSkills(data.skills);
                    setTopics(data.topics);
                },
                null,
                () => setLoading(false)
            );
        }
    }, [searchString])

    const handleSearch = useCallback(e => {
        setSearchString(e.target.value);
    }, []);

    const handleSelectItem = useCallback(
        item => {
            setSelectedItem(old => item === old ? null : item);
        },
        []
    );

    const handleLoadQuestions = useCallback(
        item => {
            setLoadingQuestions(true);
            loadQuestions(
                item,
                ({ results }) => {
                    setQuestions(results);
                },
                () => setQuestions([]),
                () => setLoadingQuestions(false)
            );
        },
        []
    );

    const handleMyQuestions = useCallback(
        () => {
            setLoadingQuestions(true);
            loadMyQuestions(
                ({ results }) => setQuestions(results),
                () => setQuestions([]),
                () => setLoadingQuestions(false)
            );
        },
        []
    )

    useEffect(
        () => {
            if (selectedItem !== null)
                handleLoadQuestions(selectedItem)
            else
                setQuestions(null)
        },
        [selectedItem]
    );

    return (
        <Grid container justifyContent="center" className={classes.wrapper}>
            <Grid item xs={12} lg={6}>
                <Paper elevation={1} className={classes.searchWrapper}>
                    <TextField label={t("Search topics and courses...")} variant="outlined" fullWidth type="search" value={searchString} onChange={handleSearch}
                        InputProps={{
                            endAdornment:
                                <InputAdornment position="end">
                                    <IconButton title={t("Add a new question")} className={classes.newButton}
                                        component={Link} to={`/${language}/dashboard/discovery/questions/add`}>
                                        <AddIcon />
                                    </IconButton>
                                </InputAdornment>
                        }}
                        autoFocus />
                    <Box my={1}>
                        <Button
                            startIcon={<ListAltIcon />}
                            endIcon={<KeyboardArrowRightIcon />}
                            fullWidth
                            color="primary"
                            onClick={handleMyQuestions}
                        >
                            {t("My Questions")}
                        </Button>
                    </Box>
                    {searchString.length > 0 &&
                        (loading ?
                            <div className={classes.loadingPanel}>
                                <CircularProgress />
                            </div>
                            :
                            ((skills.length === 0 && topics.length === 0) ?
                                <div className={classes.loadingPanel}>
                                    <i>{t("No match found...")}</i>
                                </div>
                                :
                                <List subheader={<li />} className={classes.searchList}>
                                    {skills.length > 0 && <SubList title="Exams" items={skills} onSelect={handleSelectItem} selectedItem={selectedItem} />}
                                    {topics.length > 0 && <SubList title="Topic Tests" items={topics} onSelect={handleSelectItem} selectedItem={selectedItem} />}
                                </List>
                            ))}
                </Paper>
            </Grid>
            {(questions || loadingQuestions) && <Grid item xs={12} lg={6}>
                <Paper elevation={2} className={classes.searchWrapper}>
                    {loadingQuestions ?
                        <div className={classes.loadingPanel}>
                            <CircularProgress />
                        </div>
                        :
                        (questions && questions.length === 0) ?
                            <div className={classes.loadingPanel}>
                                <i>{t("No questions found...")}</i>
                            </div>
                            :
                            <List className={classes.searchList} subheader={<li />}>
                                <li className={classes.sameBg}>
                                    <ul className={classes.sameBg} style={{ padding: 0 }}>
                                        <ListSubheader><b><i>Questions</i></b></ListSubheader>
                                        {questions.map(
                                            (q, index) => <ListItem key={index} button component={Link} to={`/${language}/dashboard/discovery/questions/${q.id}`}>
                                                <ListItemText primary={q.text.replace(/(<([^>]+)>)/gi, "")} />
                                            </ListItem>
                                        )}
                                    </ul>
                                </li>
                            </List>
                    }
                </Paper>
            </Grid>
            }
        </Grid>

    )
}

export default QuestionsDiscovery
