import { CircularProgress, Grid, IconButton, ListItem, ListItemSecondaryAction, ListItemText, makeStyles, Paper, TextField } from '@material-ui/core';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { cancelLoadPart, loadPart } from '../../utils';
import List from '@material-ui/core/List';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';


const useStyles = makeStyles(
    theme => ({
        wrapper: {
            width: '100%',
            backgroundColor: "#FFFFFF",
        },
        hasPadding: {
            padding: theme.spacing(1),
            width: "100%",
            marginBottom: theme.spacing(2)
        },
        loadingPanel: {
            textAlign: 'center',
            paddingTop: theme.spacing(1),
            paddingBottom: theme.spacing(1),
        }        
    })
);

function OerGroupSearch({ afterAdd }) {
    const classes = useStyles();
    const [oers, setOers] = useState(null);
    const [oersLoading, setOersLoading] = useState(false);
    const [searchString, setSearchString] = useState('');
    const { t } = useTranslation([ 'dashboard' ]);

    useEffect(() => { // do the search
        if (searchString.length === 0) {
            cancelLoadPart();
            setOers(null);
            setOersLoading(false);
        } else {
            setOersLoading(true);
            loadPart(
                "oers/oers",
                searchString, res => {
                setOers(res.data.results);
                setOersLoading(false);
            })
        }
    }, [searchString])

    const handleSearch = useCallback(e => {
        setSearchString(e.target.value);
    }, [])


    return (
        <Grid container justifyContent="center" className={classes.wrapper}>
            <Grid item xs={12}>
                <Paper elevation={1} className={classes.hasPadding}>
                    <TextField label={t("Search educational content...")} variant="outlined" fullWidth type="search" value={searchString} onChange={handleSearch}
                    autoFocus />
                    {oersLoading ? 
                        <div className={classes.loadingPanel}>
                            <CircularProgress />
                        </div>
                    :
                    oers && (
                        oers.length === 0 ?
                            <div className={classes.loadingPanel}>
                                <i>{t("No match found...")}</i>
                            </div>
                        :
                        <List>
                            {oers.map((o, ind) => (                        
                                <ListItem key={ind} onClick={afterAdd(o)} button>
                                    <ListItemText
                                        primary={o.title}
                                    />
                                    <ListItemSecondaryAction>
                                        <a href={o.url} target="_blank" rel="noopener noreferrer">
                                            <IconButton edge="end" aria-label="open" title={t("Open the content")}>
                                                <OpenInNewIcon />
                                            </IconButton>
                                        </a>
                                    </ListItemSecondaryAction>
                                </ListItem>
                                ))
                            }
                        </List>
                    )}
                </Paper>
            </Grid>
        </Grid>

    )
}
export default OerGroupSearch
