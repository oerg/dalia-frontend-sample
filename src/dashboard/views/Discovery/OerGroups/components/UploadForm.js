import { Checkbox, FormControlLabel, Grid, Input, InputLabel, makeStyles, TextField } from '@material-ui/core';
import AttachmentIcon from '@material-ui/icons/Attachment';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import React, { useCallback, useMemo, useState } from 'react';
import Button from '../../../../components/CustomButton/Button';
import ProgressWithLabel from '../../../../components/ProgressWithLabel';
import { doUpload } from './utils';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(
    theme => ({
        formField: {
            padding: theme.spacing(2),
            textAlign: 'center',
            position: 'relative',
        },
    })
);

function UploadForm({ isQuestion, afterUpload }) {
    const classes = useStyles();
    const [file, setFile] = useState(null);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [progress, setProgress] = useState(0);
    const [isUploading, setIsUploading] = useState(false);
    const [licenseChecked, setLicenseChecked] = useState(false);
    const { t } = useTranslation([ 'dashboard' ]);

    const isSaveEnabled = useMemo(
        () => file && title && title.length > 0 && licenseChecked,
        [ file, title, licenseChecked ]
    );

    const handleFile = useCallback(
        e => {
            setFile(e.target.files[0])
        },
        []
    );

    const handleSave = useCallback(
        () => {
            var data = new FormData();
            data.append('file', file);
            // data.append('title', file.name);
            data.append('title', title);
            data.append('mime_type', file.type);
            data.append('is_question', isQuestion);

            setIsUploading(true);

            doUpload(
                data,
                setProgress,
                res => {
                    afterUpload && afterUpload({
                        url: res.url, 
                        title,
                        description
                    });
                },
                null,
                () => setIsUploading(false)
            );

        },
        [ file, title, description, isQuestion, afterUpload ]
    );

    return (
        <Grid container>
            <Grid item xs={12} className={classes.formField}>
                <TextField value={title} onChange={e => setTitle(e.target.value)} label={t("Title")} 
                    fullWidth variant="outlined" required autoFocus />
            </Grid>
            <Grid item xs={12} className={classes.formField}>
                <TextField value={description} onChange={e => setDescription(e.target.value)} multiline rows={3} label={t("Description")} fullWidth variant="outlined" />
            </Grid>
            <Grid item xs={12} className={classes.formField}>
                <Input 
                    id="upload-button"
                    type="file"
                    style={{ display: "none" }}
                    onChange={handleFile}
                />
                <InputLabel htmlFor="upload-button">
                    <Button color="rose" component="span" startIcon={<AttachmentIcon />}>
                        { file ?
                            file['name']
                        :
                            t("Select file *")
                        }
                    </Button>
                </InputLabel>

            </Grid>
            <Grid item xs={12} className={classes.formField}>
                <FormControlLabel disabled={isUploading} 
                    label={<>
                        {t("I have the rights to upload this file under ")}
                        <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank" rel="noopener noreferrer">CCBYSA 4.0</a>
                    </>}
                    control={<Checkbox checked={licenseChecked} onChange={(e) => setLicenseChecked(e.target.checked)} color="primary" />} />
            </Grid>
            <Grid item xs={12} className={classes.formField}>
                <Button color="rose" startIcon={<CloudUploadIcon />} disabled={!isSaveEnabled } onClick={handleSave}>
                    {t("Upload")}
                </Button>
                { isUploading &&
                    <ProgressWithLabel value={progress} color="secondary" /> 
                }
            </Grid>
        </Grid>
    )
}

UploadForm.defaultProps = {
    isQuestion: false,
}

export default UploadForm
