import { Box, FormControl, FormControlLabel, FormHelperText, Grid, IconButton, Input, InputLabel, LinearProgress, makeStyles, MenuItem, Paper, Select, Switch, TextField } from '@material-ui/core'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { Alert, Autocomplete } from '@material-ui/lab'
import { withSnackbar } from 'notistack'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useHistory, useParams } from 'react-router-dom'
import { allIndustries } from '../../../../data'
import { useLanguage } from '../../../../i18n'
import Card from '../../../components/Card/Card'
import CardBody from '../../../components/Card/CardBody'
import CardHeader from '../../../components/Card/CardHeader'
import CitySelect from '../../../components/CitySelect'
import CountrySelect from '../../../components/CountrySelect'
import Button from '../../../components/CustomButton/Button'
import LangTextField from '../../../components/LangTextField'
import GoalLogo from '../../../components/Logos/Goal'
import { getSuggestionColor } from '../../helpers'
import { roseColor } from '../../styles/variables'
import Description from '../components/Description'
import { cancelJobRecommendations, doEditJob, getJobOnly, getJobRecommendations, uploadGoalPicture } from '../Jobs/utils'

const useStyles = makeStyles(theme => ({
    formField: {
        padding: theme.spacing(2),
        textAlign: 'center',
        position: 'relative',
    },
    listRoot: {
        border: '1px solid #EEE',
        borderRadius: '5px',
        textAlign: 'center',
        margin: `${theme.spacing(1)}px auto`,
        maxHeight: '300px',
        overflowY: 'auto',
        '& > li:nth-child(even)': {
            backgroundColor: '#CCC',
        }
    },
    smallCenter: {
        width: '60%',
        minWidth: '200px',
        margin: 'auto',
    },
    addSkillButton: {
        position: 'absolute',
        color: roseColor[0],
        bottom: 0,
        zIndex: 999,
    },
    toRight: {
        right: 10,
    },
    addSkillForm: {
        padding: theme.spacing(2),
        color: '#FFF',
        position: 'absolute',
        zIndex: 998,
        bottom: -30,
        right: 55,
        left: 30,
    },
    suggestions: {
        border: '1px solid ' + getSuggestionColor(1),
        color: getSuggestionColor(0.8),
        margin: theme.spacing(2),
        position: 'relative',
        maxHeight: 200,
        minHeight: 80,
        overflowY: 'auto',
        '& > *': {
            margin: theme.spacing(0.5),
        }
    },
    suggestionsLoading: {
        position: 'absolute',
        width: 35,
        height: 35,
        top: 5,
        right: 15,
    },
    suggestionsHelp: {
        position: 'absolute',
        width: 35,
        height: 35,
        right: 12,
        top: 8,
        color: "#888",
        opacity: 0.7,
        cursor: 'help',
        transition: 'opacity 1s, top 0.5s',
        '&:hover': {
            opacity: 0.9
        }
    },
    suggestion: {
        backgroundColor: getSuggestionColor(1),
        borderColor: getSuggestionColor(1),
    },
}))

function Edit({ enqueueSnackbar }) {
    const classes = useStyles();
    const [title, setTitle] = useState('');
    const [company, setCompany] = useState('');
    const [country, setCountry] = useState('');
    const [city, setCity] = useState('');
    const [loading, setLoading] = useState(false);
    const [jobRecommendations, setJobRecommendations] = useState([]);
    const [jobRecommendationsLoading, setJobRecommendationsLoading] = useState(false);
    const [isEditable, setIsEditable] = useState(true);
    const [industry, setIndustry] = useState('GE'); // general
    const [description, setDescription] = useState('');
    const [changed, setChanged] = useState(false);
    const [initJobTitle, setInitJobTitle] = useState('');
    const [isPrivate, setIsPrivate] = useState(false);
    const [picture, setPicture] = useState(undefined);
    const { t } = useTranslation(['dashboard']);
    const systemLanguage = useLanguage();
    const [language, setLanguage] = useState(systemLanguage);

    const { id } = useParams();
    const history = useHistory();

    useEffect(
        () => {
            setLoading(true);
            getJobOnly(
                id,
                (data) => {
                    var job = data;
                    var hasPerm = job['is_admin'] || job['is_contributor'] || job.is_expert;
                    setIsEditable(hasPerm);
                    setTitle(job['title']);
                    setIndustry(job['industry']);
                    setCompany(job['company']);
                    setCountry(job['country']);
                    setCity(job['city']);
                    setInitJobTitle(job['title']);
                    setDescription(job['description']);
                    setIsPrivate(job['is_private']);
                    setLanguage(job.lang);
                    setPicture(job.picture);
                },
                null,
                () => setLoading(false)
            )
        },
        [id]
    )

    const handleChangePrivate = useCallback(
        e => {
            setIsPrivate(e.target.checked);
            setChanged(true);
        },
        []
    );

    const handleIndustry = useCallback((e) => {
        setIndustry(e.target.value);
        setChanged(true);
    }, []);

    const handleTitleInput = useCallback((e, value) => {
        setTitle(value);
        setChanged(true);
        if (value.length === 0) {
            setJobRecommendationsLoading(false);
            setJobRecommendations([]);
            cancelJobRecommendations();
        } else {
            setJobRecommendationsLoading(true);
            getJobRecommendations(value,
                res => {
                    setJobRecommendations(res.data);
                    setJobRecommendationsLoading(false);
                }
            );
        }

    }, []);

    const handleSave = useCallback(
        () => {
            setLoading(true);
            doEditJob(
                id, title, language, description, industry, company, country, city, isPrivate,
                res => {
                    history.push(`/${systemLanguage}/dashboard/discovery/jobs/${id}`); // page will unload. So no stop loading.
                }
            )
        },
        [id, title, language, industry, description, company, city, country, isPrivate]
    )

    const handleDescription = useCallback(
        e => {
            setDescription(e.target.value);
            setChanged(true);
        },
        []
    );

    const handleLanguage = useCallback(
        (lang) => {
            setLanguage(lang);
            setChanged(true);
        },
        []
    );

    const handleFile = useCallback(
        e => {
            if (!e.target.files[0] || e.target.files[0].size > 2097152) { // 2MB
                enqueueSnackbar(
                    t("Please select a file which is not larger than 2 MB"),
                    {
                        varaint: "warning"
                    }
                )
                return;
            }
            var data = new FormData();
            data.append('file', e.target.files[0]);
            data.append('job_id', id);
            uploadGoalPicture(
                data,
                res => {
                    setPicture(res.picture)
                    enqueueSnackbar(
                        t("Journey picture updated successfully."),
                        {
                            variant: "success"
                        }
                    )
                },
                () => {
                    enqueueSnackbar(
                        t("Upload failed. Please try again."),
                        {
                            variant: "error"
                        }
                    )
                }
            )
        },
        [t, id, enqueueSnackbar]
    );

    return (
        <Grid container>
            <Grid item xs={12}>
                <Card>
                    <CardHeader color="rose">
                        <h3 className={classes.cardTitleWhite}>
                            {t("Editing journey")}: <em>{initJobTitle}</em>
                            <Box component="span" style={{ float: "right" }} p={1}>
                                <IconButton size="small" title={t("Go back")} onClick={() => history.goBack()} style={{ color: "white" }}>
                                    <ArrowBackIcon />
                                </IconButton>
                            </Box>
                        </h3>
                    </CardHeader>
                    <CardBody>
                        <Paper elevation={2} style={{ marginTop: 25, marginBottom: 25 }} className={classes.smallCenter}>
                            <Grid container>
                                <Grid item xs={12} md className={classes.formField}>
                                    <Autocomplete options={jobRecommendations} value={title} disabled={loading || !isEditable} openOnFocus
                                        freeSolo selectOnFocus handleHomeEndKeys disableClearable loading={jobRecommendationsLoading}
                                        renderInput={(params) =>
                                            <LangTextField
                                                label={t("Journey Title")}
                                                margin="normal"
                                                fullWidth
                                                required
                                                {...params}
                                                error={isEditable === false}
                                                language={language}
                                                onLanguageChange={handleLanguage}
                                            />
                                        }
                                        onInputChange={handleTitleInput} inputValue={title}
                                    />
                                    {loading && <LinearProgress />}
                                    {!isEditable &&
                                        <Alert severity="error">
                                            {t("You cannot edit this journey.")} (<Link to={`/${systemLanguage}/dashboard/discovery/jobs/${id}`}>{t("View")}</Link>)
                                        </Alert>
                                    }
                                </Grid>
                                <Grid item xs={12} md={4} className={classes.formField}>
                                    <FormControlLabel
                                        control={
                                            <Switch
                                                checked={isPrivate}
                                                onChange={handleChangePrivate}
                                                color="primary"
                                            />
                                        }
                                        label={t("Private Journey")}
                                        style={{ marginTop: "24px" }}
                                    />
                                </Grid>
                                <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left', display: "flex" }}>
                                    <GoalLogo src={picture} style={{ marginRight: 10 }} />
                                    <Input
                                        id="upload-button"
                                        type="file"
                                        onChange={handleFile}
                                        fullWidth
                                        placeholder={t("Picture")}
                                        inputProps={{
                                            accept: "image/png, image/jpeg"
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                    <Paper>
                                        <Grid container>
                                            <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                                <FormControl fullWidth variant="standard">
                                                    <InputLabel id="industry-label">{t("Industry")}</InputLabel>
                                                    <Select value={industry} onChange={handleIndustry} label={t("Industry")} disabled={loading || !isEditable}>
                                                        {allIndustries.map((i, index) => (
                                                            <MenuItem value={i.code} key={index}>{t(i.label)}</MenuItem>
                                                        ))}
                                                    </Select>
                                                </FormControl>
                                            </Grid>
                                            <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                                <TextField label={t("Company")} value={company} onChange={e => setCompany(e.target.value) || setChanged(true)} fullWidth
                                                    helperText={t("Leave empty to make it general.")} disabled={loading || !isEditable} placeholder={t("General")}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }} />
                                            </Grid>
                                            <Grid item xs={12} md={6} className={classes.formField} style={{ textAlign: 'left' }}>
                                                <CountrySelect onChange={(e, v) => { setCity(''); setCountry(v); setChanged(true) }} value={country}
                                                    disabled={loading || !isEditable} showGeneral />
                                                <FormHelperText>{t("Leave empty to make it general.")}</FormHelperText>
                                            </Grid>
                                            <Grid item xs={12} md={6} className={classes.formField} style={{ textAlign: 'left' }}>
                                                <CitySelect value={city} countryCode={country} name="country"
                                                    onChange={(e, v) => { setCity(v ? v : ''); setChanged(true); }} disabled={loading || !isEditable} showGeneral />
                                                <FormHelperText>{t("Leave empty to make it general.")}</FormHelperText>
                                            </Grid>
                                        </Grid>
                                    </Paper>
                                </Grid>
                                <Grid item xs={12} className={classes.formField} style={{ textAlign: 'left' }}>
                                    {/* <Paper style={{ padding: "10px" }}>
                                        <Typography style={{ textAlign: 'left', padding: 10 }}>
                                            {t("Journey Description")}
                                            <Help>
                                                {t("You can describe the journey here. For example, you may explain the objective, importance, etc. of this journey.")}
                                            </Help>
                                        </Typography>
                                        <TextField multiline fullWidth variant='outlined'
                                            value={description} onChange={handleDescription} rows={4} disabled={loading || !isEditable} />
                                    </Paper> */}
                                    <Description
                                        style={{ padding: "10px" }}
                                        label={t("Journey Description")}
                                        help={t("You can describe the journey here. For example, you may explain the objective, importance, etc. of this journey.")}
                                        value={description}
                                        onChange={handleDescription}
                                        loading={loading}
                                        TextFieldProps={{
                                            disabled: !isEditable
                                        }}
                                        placeholder={t('Describe the journey')}
                                        lang={language}
                                        keyword={title}
                                    />
                                </Grid>
                                <Grid item xs={12} className={classes.formField}>
                                    <Button color="rose" onClick={handleSave} disabled={!isEditable || loading || title.length === 0 || industry.length === 0 || !changed}
                                        fullWidth>{t("Save")}</Button>
                                </Grid>
                            </Grid>
                        </Paper>
                    </CardBody>
                </Card>
            </Grid>
        </Grid>
    )
}

Edit.propTypes = {

}

export default withSnackbar(Edit)
