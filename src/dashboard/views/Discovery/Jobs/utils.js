import Axios from 'axios';
import { MultiCall, requestAddHandlers } from '../../helpers';

export function getJob(id, onSuccess, onError) { // get job and suggestions from the server
    const jobReq = Axios.get('/jobs/' + id);
    const suggestionsReq = Axios.get('/suggestions?object_type=JO&object_id=' + id + '&ordering=-created_at');
    Axios.all([jobReq, suggestionsReq])
        .then(Axios.spread((...res) => {
            onSuccess && onSuccess(res[0].data, res[1].data);
        }))
        .catch(err => {
            onError && onError(err.response);
        });
}

export function getJobOnly(id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        Axios.get('/jobs/' + id),
        onLoad,
        onError,
        onEnd
    );
}

export function createSortString(sortBy, sortAD) {
    let sortStr = sortAD ? '+' : '-';
    sortStr += sortBy === 'date' ? 'created_at' : 'upvote_count'
    return sortStr;
}

export function doSkillVote(jobId, skillId, vote, onSuccess, onError, onEnd) { // send topics' vote to server.
    return requestAddHandlers(
        Axios.post('/jobs/add-job-skill-vote/', {
            skill: skillId,
            job: jobId,
            vote: vote,
        }),
        onSuccess,
        onError,
        onEnd
    );
}

export function doSuggestionVote(id, vote, onSuccess, onError, onEnd) { // send topics' vote to server.
    return requestAddHandlers(
        Axios.post('/suggestions/add-suggestion-vote/', {
            suggestion: id,
            vote: vote,
        }),
        onSuccess,
        onError,
        onEnd
    );
}

export function processVote(list, id, up, down, selfVote, idField = 'id') { // update local list of votes
    let data = list.find(t => t[idField] === id)
    data.vote = selfVote;
    data.upvote_count = up;
    data.downvote_count = down;
    return [...list];
}

export function addDeleteSuggestion(jobId, skillId, onSuccess) { // send request to server
    Axios.post('/suggestions/', {
        object_type: 'JO',
        object_id: jobId,
        payload: {
            suggestion_type: "DE",
            skill_id: skillId,
        }
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
}

export function addNewSuggestion(jobId, data, onSuccess) { // send request to server
    Axios.post('/suggestions/', {
        object_type: 'JO',
        object_id: jobId,
        payload: Object.assign(data, {
            suggestion_type: 'AD'
        })
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
}

export function addEditSkillSuggestion(jobId, jobTitle, onSuccess) { // send request to server
    Axios.post('/suggestions/', {
        object_type: 'JO',
        object_id: jobId,
        payload: {
            suggestion_type: "EJ",
            job_title: jobTitle,
        }
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
}

export function addEditSuggestion(jobId, skillId, data, onSuccess) { // send request to server
    Axios.post('/suggestions/', {
        object_type: 'JO',
        object_id: jobId,
        payload: Object.assign(data, {
            suggestion_type: "RO",
            skill_id: skillId
        })
    })
        .then(res => {
            onSuccess && onSuccess(res.data);
        })
}

export function filterSuggestions(suggestions) {
    const add = [] // add
    const edit = [] // edit
    const reorder = [] // reorder
    const remove = [] // delete
    suggestions.forEach(s => {
        if (s.decision === 'PE') { // only pendings
            switch (s.payload.suggestion_type) {
                case 'AD':
                    add.push(s)
                    break;
                case 'RO':
                    reorder.push(s);
                    break;
                case 'DE':
                    remove.push(s);
                    break;
                default:
            }
        }
        return {
            add, edit, reorder, remove
        }
    });

    return { add, edit, reorder, remove }
}

export function getAllSkills(onSucces, onError, onEnd) {
    return requestAddHandlers(
        Axios.get('/skills/get-existing-skills/?search='),
        onSucces,
        onError,
        onEnd
    );  
}

export function doAddJob(title, lang, description, skills, industry, company, country, city, is_private, esco_title, onSucces, onError, onEnd) {
    return requestAddHandlers(
        Axios.post('/jobs/add-a-job/', {
            title, lang, skills, industry, company, country, city, description, is_private, esco_title
        }),
        onSucces,
        onError,
        onEnd
    );
}

function removeNulls(obj) {
    for (var propName in obj) {
        if (obj[propName] === null || obj[propName] === undefined) {
            delete obj[propName];
        }
    }
    return obj
}

export function doEditJob(id, title, lang, description, industry, company, country, city, is_private, onSucces, onError, onEnd) {
    const body = removeNulls({ job_id: id, title, lang, industry, company, country, city, description, is_private });

    Axios.put('/jobs/edit-job-properties/', body)
        .then(res => {
            onSucces && onSucces(res);
        })
        .catch(err => {
            err &&
                onError ? onError(err.response ? err.response.data : err) : alert(err.response.data.result);
        })
        .finally(() => onEnd && onEnd());
}

// ##########################
export function getRelatedSkills(job, lang, onSuccess, onError, onEnd) {
    var mc = MultiCall.getInstance("AD_getRelatedSkills");
    return mc.call(`/jobs/get-job-skills/?job=${job}&lang=${lang}`, onSuccess, onError, onEnd);
}

export function cancelRelatedSkills() {
    return MultiCall.getInstance("AD_getRelatedSkills").cancel();
}


export function getRelatedJobs(search, lang, onSuccess, onError, onEnd) {
    var mc = MultiCall.getInstance("AD_getRelatedJobs");
    return mc.call(`/jobs/get-related-jobs/?search=${search}&lang=${lang}`, onSuccess, onError, onEnd);
}

export function cancelRelatedJobs() {
    return MultiCall.getInstance("AD_getRelatedJobs").cancel();
}
// ########################

export function getJobRecommendations(search, onSuccess, onError) {
    var mc = MultiCall.getInstance("AJ_getRecom")
    return mc.call('/jobs/get-existing-jobs/?search=' + search, onSuccess, onError);
}

export function cancelJobRecommendations() {
    return MultiCall.getInstance("AJ_getRecom").cancel();
}

export function getIsEditableJob(title, lang, industry, company, country, city, isPrivate, onSuccess, onError) {
    var mc = MultiCall.getInstance("AJ_isEditable")
    return mc.call(`/jobs/is-job-editable/?title=${title}&lang=${lang}&company=${company}&industry=${industry}&country=${country}&city=${city}&is_private=${isPrivate}`, onSuccess, onError)
}

export function cancelIsEditableJob() {
    return MultiCall.getInstance("AJ_isEditable").cancel();
}

const SIMILAR_JOBS_MC = "AJ_similarJobsLoading";
export function loadSimilarJobs(title, lang, onSuccess, onError, onEnd) {
    var mc = MultiCall.getInstance(SIMILAR_JOBS_MC)
    return mc.call(`/jobs/search/?search-term=${title}&lang=${lang}`, onSuccess, onError, onEnd)
}

export function cancelLoadSimilarJobs() {
    return MultiCall.getInstance(SIMILAR_JOBS_MC).cancel();
}

export function formatWord(wordRecomm) {
    return wordRecomm.recommendation.skill;
}

export function uploadGoalPicture(data, onLoad, onError, onEnd) {
    return requestAddHandlers(
        Axios.post('/jobs/upload-item-picture/', data),
        onLoad,
        onError,
        onEnd
    )
}