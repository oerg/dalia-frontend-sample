import { Box, CircularProgress } from '@material-ui/core'
import React, { useCallback, useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import LoginWarning from '../../../components/LoginWarning'
import Add from './Add'
import { getQuestion } from './utils'

function Questions({ requireLogin, navHistory, onNav, ...props}) {
    const { id: questionId } = useParams();

    const [question, setQuestion] = useState(null);
    const [isLoading, setLoading] = useState(true);
    const [criticalError, setCriticalError] = useState(null);

    const [reloadNeeded, setReloadNeeded] = useState(true);

    const history = useHistory();

    useEffect(
    () => { // load oerGroup and suggestions
        if (reloadNeeded) {
            setLoading(true);
            getQuestion(
                questionId, 
                (data) => { // load
                    setQuestion(data);
                },
                error => { // on error
                    if (error) {
                        if (error.status === 404) {
                            setCriticalError("404: Question was not found!");
                        } else if (error.status >= 500) {
                            setCriticalError(error.status + ": Server error!")
                        } else
                            setCriticalError(error.data);
                    }
                },
                () => setLoading(false)
            );
            setReloadNeeded(false);
        }
    }, [questionId, reloadNeeded])

    // login
    const [showLoginWarning, setShowLoginWarning] = useState(false);
    const handleLogin = useCallback((action) => {
        setShowLoginWarning(false);
        if (action) {
            history.push(action);
        }
    }, [history]);

    return (<>
        { requireLogin &&
            <LoginWarning show={showLoginWarning} onSelect={handleLogin} />
        }
        {isLoading ?
            <Box p={10} textAlign="center">
                <CircularProgress />
            </Box>
        :
        criticalError ?
        <Box p={10} textAlign="center">
            {criticalError}
        </Box>
        :
        <Add
            id={question.id}
            question={question}
            topics={question.topics}
            choices={question.choices}
        />
        }
    </>)
}

export default Questions
