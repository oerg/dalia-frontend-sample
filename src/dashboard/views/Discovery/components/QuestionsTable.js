import clsx from 'classnames';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import QuestionSearchBox from '../../../components/QuestionSearchBox';
import ToggleDiv from '../../../components/ToggleDiv';

function QuestionsTable({ title: propTitle, titleClassName, ...props }) {
    const { t } = useTranslation(['dashboard']);
    const title = useMemo(
        () => propTitle ? propTitle : <b>{t("Questions")}</b>,
        [t, propTitle]
    );

    return <ToggleDiv
        title={<b>{title}</b>}
        className={clsx(titleClassName)}
    >
        <QuestionSearchBox height={250} {...props} />
    </ToggleDiv>;
}

export default QuestionsTable;
