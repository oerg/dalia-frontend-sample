import { Button, Collapse, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControlLabel, makeStyles, Paper, Radio, RadioGroup } from '@material-ui/core'
import React, { useCallback, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { useLanguage } from '../../../../i18n'
import GoalLogo from '../../../components/Logos/Goal'
import SkillLogo from '../../../components/Logos/Skill'
import TopicLogo from '../../../components/Logos/Topic'

const useStyles = makeStyles({
    dialogPaper: {
        minWidth: 700
    }
})

function Comments({ show, ...props }) {
    return <Collapse in={show}>
        <Paper style={{ padding: 8, backgroundColor: "#EEE", display: "flex", alignItems: "center" }} {...props} />
    </Collapse>
}

function AddWizardDialog({ open, onClose }) {
    const classes = useStyles()
    const { t } = useTranslation(['dashboard'])
    const language = useLanguage()
    const [selectedAnswer, setSelectedAnswer] = useState('')

    const handleSelectAnswer = useCallback(
        (event) => {
            setSelectedAnswer(event.target.value);
        },
        []
    )

    return (
        <Dialog
            open={open}
            onClose={onClose}
            keepMounted={false}
            style={{ minWidth: 800 }}
            classes={{
                paper: classes.dialogPaper
            }}
        >
            <DialogTitle>
                {t("Add Item")}
            </DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {t("According to your plan, will the item you are going to add")}&nbsp;...
                </DialogContentText>
                <RadioGroup value={selectedAnswer} onChange={handleSelectAnswer}>
                    <FormControlLabel
                        value="a1"
                        control={<Radio />}
                        label={t("directly be covered by educational materials")}
                    />
                    <Comments show={selectedAnswer === "a1"}>
                        <TopicLogo title={t("Topic")} style={{ marginRight: 8 }} />
                        <div>
                            {t("You should add a topic.")}
                            <Button
                                component={Link}
                                to={`/${language}/dashboard/discovery/topics/add`}
                            >
                                {t("Add a Topic")}
                            </Button><br />
                            <img src={`${process.env.PUBLIC_URL}/images/edoer-graph-topic.jpg`} alt={t("Topic Structure")} width="100%" />
                        </div>
                    </Comments>
                    <FormControlLabel
                        value="a2"
                        control={<Radio />}
                        style={{
                            marginTop: 8
                        }}
                        label={t("needs closely related sections (each section will be covered by educational materials)")}
                    />
                    <Comments show={selectedAnswer === "a2"}>
                        <SkillLogo title={t("Course")} style={{ marginRight: 8 }} />
                        <div>
                            {t("You should add a course and define the sections as its topics.")}
                            <Button
                                component={Link}
                                to={`/${language}/dashboard/discovery/skills/add`}
                            >
                                {t("Add a Course")}
                            </Button><br />
                            <img src={`${process.env.PUBLIC_URL}/images/edoer-graph-skill-structure.jpg`} alt={t("Course Structure")} width="100%" />
                        </div>
                    </Comments>
                    <FormControlLabel
                        value="a3"
                        control={<Radio />}
                        style={{
                            marginTop: 8
                        }}
                        label={t("needs different knowledge areas")}
                    />
                    <Comments show={selectedAnswer === "a3"}>
                        <GoalLogo title={t("Journey")} style={{ marginRight: 8 }} />
                        <div>
                            {t("You should add a journey and define the knowledge areas as its courses.")}
                            <Button
                                component={Link}
                                to={`/${language}/dashboard/discovery/jobs/add`}
                            >
                                {t("Add a Journey")}
                            </Button><br />
                            <img src={`${process.env.PUBLIC_URL}/images/edoer-graph-goal-structure.jpg`} alt={t("Journey Structure")} width="100%" />
                        </div>
                    </Comments>
                </RadioGroup>
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="secondary">
                    {t("Cancel")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default AddWizardDialog