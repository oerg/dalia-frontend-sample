import axios from "axios";
import Axios from "axios";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation } from "react-router-dom";
import { allIndustries } from '../../data'

export function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

export function useParam(paramName) {
    const [value, setValue] = useState('');
    const [lastParamName, setLastParamName] = useState('')

    var tmpVal = value;
    if (paramName !== lastParamName) {
        setLastParamName(paramName);
        tmpVal = getUrlParameter(paramName);
        setValue(tmpVal);
    }

    return tmpVal;
}

export function useParamWatcher(params, urlParamObj = false) {
    const urlSearch = useLocation().search;
    const urlParams = useMemo(() => new URLSearchParams(urlSearch), [urlSearch]);
    const paramNames = useMemo(() => params.split('&'), [params]);

    const extractData = useCallback((params) => {
        const val = {}
        for (const p in params) {
            val[params[p]] = urlParams.get(params[p]);
        }

        if (params.length > 1)
            return val;
        else if (params.length === 1)
            return val[params[0]];
        return '';
    }, [urlParams]);

    const [results, setResults] = useState(() => extractData(paramNames));

    useEffect(() => {
        setResults(extractData(paramNames));
    }, [paramNames, extractData]);

    return urlParamObj ? [results, urlParams] : results;
}

export function getUrlSearchParams() {
    return new URLSearchParams(window.location.search);
}

export function isMyFirstTime(page, userData) {
    return !userData || !userData.visited_pages || userData.visited_pages.indexOf(page) === -1
}

export function saveVisit(page, setData = () => { }) {
    setData(p => (
        { ...p, visited_pages: p.visited_pages + page + "," }
    ))
    Axios.post('members/add-visit/', { page: page })
        .catch(() => { });
}


export class MultiCall {
    static objects = {}

    constructor(timeout = 1000) {
        this.lastTry = null;
        this.lastTokenSource = null; // for cancellation
        this.timeout = timeout;
    }

    static getInstance(code, timeout = 1000) {
        if (!(code in this.objects))
            this.objects[code] = new MultiCall(timeout);
        return this.objects[code];
    }

    call(url, onSuccess, onError, onEnd) {
        this.cancel();

        var me = this; // needed for settimeout
        this.lastTry = window.setTimeout(() => {
            me.lastTokenSource = axios.CancelToken.source();
            axios.get(url, { cancelToken: me.lastTokenSource.token })
                .then(res => {
                    onSuccess && onSuccess(res);
                })
                .catch(err => {
                    if (err && err.response) {
                        onError ? onError(err.response ? err.response.data : err) : alert(err.response.data.result);
                    }
                })
                .finally(() => {
                    onEnd && onEnd();
                })
        }, this.timeout);
        return this;
    }

    cancel() {
        if (this.lastTry) // stop last try
            window.clearTimeout(this.lastTry);
        if (this.lastTokenSource)
            this.lastTokenSource.cancel();

        this.lastTry = null;
        this.lastTokenSource = null;
        return this;
    }
}


var suggestionsColors = [
    { pct: 0.0, color: { r: 0xff, g: 0, b: 0 } },
    { pct: 0.5, color: { r: 0xff, g: 0xd0, b: 0 } },
    { pct: 1.0, color: { r: 0x01, g: 0xff, b: 0 } },
];

export function getSuggestionColor(pct) {
    for (var i = 1; i < suggestionsColors.length - 1; i++) {
        if (pct < suggestionsColors[i].pct) {
            break;
        }
    }
    var lower = suggestionsColors[i - 1];
    var upper = suggestionsColors[i];
    var range = upper.pct - lower.pct;
    var rangePct = (pct - lower.pct) / range;
    var pctLower = 1 - rangePct;
    var pctUpper = rangePct;
    var color = {
        r: Math.floor(lower.color.r * pctLower + upper.color.r * pctUpper),
        g: Math.floor(lower.color.g * pctLower + upper.color.g * pctUpper),
        b: Math.floor(lower.color.b * pctLower + upper.color.b * pctUpper)
    };
    return 'rgb(' + [color.r, color.g, color.b].join(',') + ')';
};


// Add all handlers to axios request
export function requestAddHandlers(req, onLoad, onError, onEnd, raw = false) {
    req
        .then(res => {
            onLoad && (raw ? onLoad(res) : onLoad(res.data));
        })
        .catch(err => {
            onError && onError(err.response);
            err.response && err.response.result && alert(err.response.result)
        })
        .finally(() => onEnd && onEnd());
}


export function canI(obj) {
    return obj.is_admin || obj.is_contributor;
}

export function getIndustryLabel(industry_code) {
    var item = allIndustries.find(i => i.code === industry_code)
    if (item)
        return item.label;
    return "";
}

export function usePropState(prop, def) {
    const [state, setState] = useState(def);

    useEffect(
        () => prop !== undefined && setState(prop),
        [prop]
    );

    return [state, setState];
}

export function validateEmail(email) {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};