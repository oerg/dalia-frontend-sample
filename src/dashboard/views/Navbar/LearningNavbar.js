import { AppBar, Avatar, Badge, Button, ButtonGroup, Hidden, Slide, Tab, Tabs } from '@material-ui/core';
import AssessmentIcon from '@material-ui/icons/Assessment';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import HowToRegIcon from "@material-ui/icons/HowToReg";
import LockOpenIcon from "@material-ui/icons/LockOpen";
import MenuBookIcon from '@material-ui/icons/MenuBook';
import NotificationsIcon from '@material-ui/icons/Notifications';
import PeopleIcon from '@material-ui/icons/People';
import clsx from 'classnames';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import Notifications from '../Notifications';
import { doMarkRead, markAllAsRead } from '../Notifications/utils';
import MentoringRequestDialog from '../Overlays/MentoringRequestDialog';
import useSearch from '../Search/useSearch';
import '../styles/blink.css';
import useStyles from '../styles/NavbarStyles';
import SearchButton from './components/SearchButton';
import SideBar from './components/SideBar';
import UserMenu from './components/UserMenu';
import { doAction, getName, loadNotifs, notifCheckDelay } from './utils';


const logoImage = `${process.env.PUBLIC_URL}/images/logo.png`;
function LearningNavbar({ basePath, user, userData, enqueueSnackbar, closeSnackbar }) {
    const classes = useStyles();
    const history = useHistory();
    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();

    const [notifRef, setNotifRef] = useState(null);
    const [openNotifs, setOpenNotifs] = useState(false);
    const [notifs, setNotifs] = useState([]);
    const [loadingNotifs, setLoadingNotifs] = useState(false);
    const [openUserMenu, setOpenUserMenu] = useState(false);

    const unreadNotifsCount = useMemo(
        () => notifs.filter(n => n['status'] === "UR").length,
        [notifs]
    );
    const location = useLocation();
    const [openSearch] = useSearch()

    const userButtonRef = useRef(null);

    const linksData = useMemo(() => {
        return user.loggedIn ? [
            {
                location: `${basePath}/edu`,
                icon: <MenuBookIcon />,
                label: t('Learn'),
            },
            {
                location: `${basePath}/assess`,
                icon: <AssessmentIcon />,
                label: t('Assessments'),
            },
        ] :
            [
                {
                    location: {
                        pathname: `/${language}/`,
                        state: {
                            login: true,
                            backUrl: history.location.pathname
                        }
                    },
                    icon: <LockOpenIcon />,
                    label: t('Login'),
                },
                {
                    location: `/${language}/register`,
                    icon: <HowToRegIcon />,
                    label: t('Register'),
                },
            ];
    }, [user.loggedIn, basePath, t, history]);

    const sideBarLinks = useMemo(
        () => [
            ...linksData,
            {
                label: "-",
            },
            ...userData && Object.keys(userData).length > 0 && userData.settings.discovery ?
                [{
                    label: t("Participate in Content Curation"),
                    location: `${basePath}/discovery`,
                    icon: <PeopleIcon />,
                },]
                :
                [],
        ],
        [linksData, basePath, userData, t]
    )

    const activeMenu = useMemo(
        () => {
            const active = linksData.find(
                menuItem => location.pathname.startsWith(menuItem.location)
                    || (menuItem.alias && menuItem.alias.some(alias => location.pathname.startsWith(alias)))
            );
            if (active) return active.label;
            return false;
        },
        [linksData, basePath, location]
    );

    const [requestMentoring, setRequestMentoring] = useState(false);

    // load notifications
    const loadNewNotifs = useCallback(
        () => {
            loadNotifs(
                notifs,
                r => setNotifs( // add new notifs
                    nts => {
                        if (r['results'] && r['results'].length > 0)
                            return [...r['results'], ...nts]
                        return nts;
                    }
                )
            )
        },
        [notifs]
    )

    // call load new notifications priodically
    useEffect(
        () => {
            loadNewNotifs();
        },
        [loadNewNotifs]
    );
    useEffect(
        () => {
            if (user.loggedIn) {
                const req = setInterval(
                    loadNewNotifs,
                    notifCheckDelay
                );
                return () => clearInterval(req); // clear interval on end
            }
        },
        [loadNewNotifs, user.loggedIn]
    )

    const handleCloseNotifs = useCallback(
        () => {
            setOpenNotifs(false);
        },
        []
    );

    const handleToggleNotifs = useCallback(
        e => {
            setNotifRef(e.currentTarget);
            setOpenNotifs(o => !o);
        },
        []
    );

    const handleSearch = useCallback(
        () => {
            openSearch(
                null,
                {
                    showTopicOption: true,
                }
            )
        },
        [openSearch]
    )


    const handleReadNotif = useCallback(
        notif => {
            setLoadingNotifs(true);
            doMarkRead(
                notif,
                r => {
                    notif['status'] = "RE"; // mark as read locally
                    setNotifs(n => [...n]); // refresh notifications
                },
                null,
                () => setLoadingNotifs(false)
            )
        },
        []
    );

    const handleActionNotif = useCallback(
        (action, notif) => {
            doAction(
                action,
                history,
                () => {
                    handleReadNotif(notif)
                    if (action.reload) {
                        user.reload()
                    }
                }
            );
        },
        [handleReadNotif, user]
    );

    const handleMarkAll = useCallback(
        () => {
            setLoadingNotifs(true);
            markAllAsRead(
                () => {
                    setNotifs(
                        notifs => notifs.map(
                            n => ({ ...n, status: "RE" })
                        )
                    );
                },
                null,
                () => setLoadingNotifs(false)
            );
        },
        []
    );

    const handleCloseMentoring = useCallback(
        () => setRequestMentoring(false),
        []
    );

    const handleRequestMentoring = useCallback(
        () => setRequestMentoring(true),
        []
    );

    return <>
        <div style={{
            position: "absolute",
            top: 86,
            left: -5,
            zIndex: 4,
        }}>
            <Slide
                in={userData && userData.settings ? userData.settings.discovery : false}
                direction='right'
            >
                <Button
                    title={t("Go to Content Curation Mode")}
                    component={Link}
                    to={`${basePath}/discovery/`}
                    variant='contained'
                    className={classes.backButton}
                    startIcon={<PeopleIcon />}
                >
                    {t("Content Cur.")}
                </Button>
            </Slide>
        </div>
        <AppBar className={classes.appBar} position="static">
            <div className={classes.container}>
                <Link to={`/${language}/`} title={t("Home")}>
                    <img src={`${process.env.PUBLIC_URL}/images/dalia/logo2.png`} alt={t("Dalia Logo")} className={classes.logo} />
                </Link>
                <Hidden smDown implementation="css" className={classes.topMenuWrapper}>
                    <Tabs scrollButtons="auto" className={classes.topMenu} value={activeMenu}
                        classes={{ indicator: classes.menuIndicator }} variant="scrollable" id="navbar">
                        {linksData.map(
                            (link, index) => (
                                <Tab
                                    component={Link}
                                    to={link.location}
                                    icon={link.icon}
                                    key={index}
                                    value={link.label}
                                    className={clsx(classes.menuItem)}
                                    classes={{
                                        wrapper: classes.menuItemWrapper
                                    }}
                                    label={link.label}
                                    {...link.props} />
                            )
                        )}
                    </Tabs>
                </Hidden>
                <ButtonGroup variant="contained" color="inherit" className={classes.rightButton}>
                    <SearchButton
                        onClick={handleSearch}
                    />
                    <Button
                        className={classes.userButton}
                        classes={{
                            label: classes.userButtonLabel
                        }}
                        title={t("Request a mentor")}
                        onClick={handleRequestMentoring}
                    >
                        <ContactMailIcon />
                    </Button>
                    <Button
                        ref={notifRef}
                        onClick={handleToggleNotifs}
                        title={t("Notifications")}
                    >
                        <Badge
                            badgeContent={unreadNotifsCount}
                            classes={{
                                badge: 'notifBadge'
                            }}>
                            <NotificationsIcon />
                        </Badge>
                    </Button>
                    {user.loggedIn &&
                        <Button
                            ref={userButtonRef}
                            onClick={() => setOpenUserMenu(o => !o)}
                            className={classes.userButton}
                            classes={{
                                label: classes.userButtonLabel
                            }}
                            title={getName(userData)}
                        >
                            <Avatar style={{ width: 40, height: 40 }} src={userData.profile_picture} />
                        </Button>
                    }
                </ButtonGroup>
                <Hidden mdUp implementation="css" className={classes.menuButton}>
                    <SideBar links={sideBarLinks} logoPath={logoImage} />
                </Hidden>
                {user.loggedIn &&
                    <UserMenu
                        anchorEl={userButtonRef.current}
                        open={openUserMenu}
                        showMentoring={userData.settings.mentoring}
                        showParticipation={userData.settings.discovery}
                        onClose={() => setOpenUserMenu(false)}
                    />
                }
                <Notifications show={openNotifs} onClose={handleCloseNotifs} anchor={notifRef} notifications={notifs}
                    onRead={handleReadNotif} onAction={handleActionNotif} loading={loadingNotifs} onMarkAllAsRead={handleMarkAll} />
                <MentoringRequestDialog
                    user={user}
                    open={requestMentoring}
                    onClose={handleCloseMentoring}
                />
            </div>
        </AppBar>
    </>
}

LearningNavbar.propTypes = {
    handleDrawerToggle: PropTypes.func
}

export default withSnackbar(LearningNavbar);
