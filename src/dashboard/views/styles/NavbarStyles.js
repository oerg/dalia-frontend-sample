import { makeStyles } from "@material-ui/core";
import {
    container,
    dangerColor, defaultBoxShadow, defaultFont,
    grayColor, infoColor, primaryColor,
    successColor,
    warningColor,

    whiteColor
} from "./variables";

const headerStyle = makeStyles(theme => ({
    appBar: {
        // zIndex: "1029",
        color: grayColor[7],
        padding: "10px 0",
        minHeight: "50px",
        backgroundColor: "#fafafa",
    },
    container: {
        ...container,
        display: "flex",
        width: "100%",
        // justifyContent: "space-between",
        alignItems: "center",
        gap: 10,
    },
    logo: {
        height: 48,
        marginRight: 16,
        display: "block",
    },
    topMenuWrapper: {
        marginTop: 8,
        borderRadius: 4,
        overflow: "hidden",
        height: 50,
        flexGrow: 1,
        display: 'flex',
        justifyContent: 'center',
        [theme.breakpoints.only('md')]: {
            maxWidth: "60%",
        },
        [theme.breakpoints.only('sm')]: {
            maxWidth: "80%",
        },
        [theme.breakpoints.only('xs')]: {
            maxWidth: "100%",
        }
    },
    topMenu: {
        height: 50,
    },
    menuItem: {
        // backgroundColor: "#009d9d",
        background: "linear-gradient(60deg, #0000BA, #00006E)",
        color: "#EEE",
        // color: "white",
        minHeight: 50,
        padding: 0,
        "&:hover": {
            color: "#FFF",
        },
        "&:focus": {
            color: "#DDD"
        }
    },
    menuItemWrapper: {
        flexDirection: "row",
        "& :first-child": {
            marginRight: 8
        },
        [theme.breakpoints.only('xs')]: {
            "& :first-child": {
                display: "none",
            }
        }
    },
    menuIndicator: {
        backgroundColor: "#CCC",
        height: 5,
    },
    title: {
        ...defaultFont,
        letterSpacing: "unset",
        lineHeight: "30px",
        fontSize: "18px",
        borderRadius: "3px",
        textTransform: "none",
        color: "inherit",
        margin: "0",
        "&:hover,&:focus": {
            background: "transparent"
        }
    },
    appResponsive: {
        top: "8px"
    },
    primary: {
        backgroundColor: primaryColor[0],
        color: whiteColor,
        ...defaultBoxShadow
    },
    info: {
        backgroundColor: infoColor[0],
        color: whiteColor,
        ...defaultBoxShadow
    },
    success: {
        backgroundColor: successColor[0],
        color: whiteColor,
        ...defaultBoxShadow
    },
    warning: {
        backgroundColor: warningColor[0],
        color: whiteColor,
        ...defaultBoxShadow
    },
    danger: {
        backgroundColor: dangerColor[0],
        color: whiteColor,
        ...defaultBoxShadow
    },
    searchInput: {
        minWidth: '500px',
    },
    userButton: {
        maxWidth: 150,
    },
    userButtonLabel: {
        whiteSpace: "nowrap",
        overflow: "hidden",
        width: "100%",
        justifyContent: "left",
    },
    rightButton: {
        marginTop: 8,
        height: 48,
        "&> button": {
            background: "linear-gradient(60deg, #0000BA, #00006E)",
            color: "#EEE"
        }
    },
    menuButton: {
        margin: "8px 16px",
    },
    buttonsWrapper: {
        margin: "0px 8px",
    },
    appBarPart: {
        flexGrow: 1,
        margin: "8px 16px"
    },
    searchForm: {
        width: "60%",
        margin: "auto",
        [theme.breakpoints.down('sm')]: {
            width: "100%",
        }
    },
    discoveryNav: {
        flexGrow: 1,
        marginBottom: 6,
    },
    discoveryLogo: {
        height: 58,
        width: 111.83,
        marginRight: 16,
        display: "block",
    },
    backButton: {
        borderColor: "#00006E",
        backgroundColor: "#0000BA",
        color: "#FFF",
        opacity: 0.7,
        '&:hover': {
            opacity: 1,
            borderColor: "#00006E",
            backgroundColor: "#0000BA",
            color: "#FFF",
        }
    }
}));

export default headerStyle;
