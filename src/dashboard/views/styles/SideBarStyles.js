import { makeStyles } from "@material-ui/core";
import { grayColor, hexToRgb, infoColor } from "./variables";


export default makeStyles(theme => ({
    logoImage: {
        width: "100%",
        display: "inline-block",
        // maxHeight: "30px",
        // marginLeft: "10px",
        marginRight: "15px",
        backgroundColor: "white",
        borderRadius: 20,
        textAlign: "center",
        padding: 5
    },
    img: {
        width: 125,
        border: 0
    },
    logo: {
        position: "relative",
        padding: "15px 15px",
        zIndex: "4",
        "&:after": {
          content: '""',
          position: "absolute",
          bottom: "0",

          height: "1px",
          right: "15px",
          width: "calc(100% - 30px)",
          backgroundColor: "rgba(" + hexToRgb(grayColor[6]) + ", 0.3)"
        }
    },
    logoLink: {
        fontFamily: "'IBM Plex Mono', 'Roboto', 'Helvetica', 'Arial', sans-serif",
        padding: "5px 0",
        display: "block",
        fontSize: "18px",
        textAlign: "left",
        fontWeight: "400",
        lineHeight: "30px",
        textDecoration: "none",
        backgroundColor: "transparent",
        "&,&:hover": {
          color: "#FFFFFF"
        }
    },
    sidebarWrapper: {
        position: "relative",
        height: "calc(100vh - 75px)",
        overflow: "auto",
        width: "260px",
        zIndex: "4",
        overflowScrolling: "touch"
    },
    item: {
        position: "relative",
        display: "block",
        textDecoration: "none",
        "&:hover,&:focus,&:visited,&": {
          color: "#FFFFFF",
        },
    },
    itemLink: {
        width: "auto",
        transition: "all 300ms linear",
        margin: "10px 15px 0",
        borderRadius: "3px",
        position: "relative",
        display: "block",
        padding: "10px 15px",
        backgroundColor: "transparent",
    },
    itemText: {
        margin: "0",
        lineHeight: "30px",
        fontSize: "14px",
        color: "#FFFFFF"
    },
    itemIcon: {
        width: "24px",
        height: "30px",
        fontSize: "24px",
        lineHeight: "30px",
        float: "left",
        marginRight: "15px",
        textAlign: "center",
        verticalAlign: "middle",
        color: "rgba(255, 255, 255, 0.8)"
    },
    activeItem: {
        backgroundColor: infoColor[0],
        boxShadow:
          "0 12px 20px -10px rgba(0, 172, 193, .28), 0 4px 20px 0 rgba(0, 0, 0, .12), 0 7px 8px -5px rgba(0, 172, 193, .2)",
        "&:hover,&:focus": {
          backgroundColor: infoColor[0],
          boxShadow:
            "0 12px 20px -10px rgba(0, 172, 193, .28), 0 4px 20px 0 rgba(0, 0, 0,.12), 0 7px 8px -5px rgba(0, 172, 193, .2)"
        }
    },
    background: {
        position: "absolute",
        zIndex: "1",
        height: "100%",
        width: "100%",
        display: "block",
        top: "0",
        left: "0",
        backgroundSize: "cover",
        backgroundPosition: "top left",
        "&:after": {
          position: "absolute",
          zIndex: "3",
          width: "100%",
          height: "100%",
          content: '""',
          display: "block",
          background: "#000000",
          opacity: ".8"
        }
    },
    list: {
        marginTop: "20px",
        paddingLeft: "0",
        paddingTop: "0",
        paddingBottom: "0",
        marginBottom: "0",
        listStyle: "none",
        position: "unset"
    },
}));
