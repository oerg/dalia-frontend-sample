import { Grid, Paper, Typography } from '@material-ui/core';
import { withSnackbar } from 'notistack';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Card from '../../components/Card/Card';
import CardBody from '../../components/Card/CardBody';
import CardHeader from '../../components/Card/CardHeader';
import GridContainer from '../../components/Grid/GridContainer';
import GridItem from '../../components/Grid/GridItem';
import useStyles from '../styles/SkillsStyle';
import AcceptedRequest from './components/AcceptedRequest';
import RelatedRequest from './components/RelatedRequest';
import { decideOnRequest, finishRequest, getAcceptedRequests, getRelatedRequests } from './utils';

function Mentoring({ enqueueSnackbar }) {
    const classes = useStyles();

    const { t } = useTranslation(['dashboard']);

    const [loadingRelated, setLoadingRelated] = useState(false);
    const [loadingAccepted, setLoadingAccepted] = useState(false)
    const [criticalErrorRelated, setCriticalErrorRelated] = useState(null);
    const [criticalErrorAccepted, setCriticalErrorAccepted] = useState(null)
    const [relatedRequests, setRelatedRequests] = useState([]);
    const [acceptedRequest, setAcceptedRequest] = useState([])

    const loadRelatedRequests = useCallback(
        () => {
            setLoadingRelated(true);
            getRelatedRequests((data) => {
                setRelatedRequests(data);
                setLoadingRelated(false);
            });
        },
        [],
    )

    const loadAcceptedRequests = useCallback(
        () => {
            setLoadingAccepted(true);
            getAcceptedRequests((data) => {
                setAcceptedRequest(data);
                setLoadingAccepted(false);
            });
        },
        [],
    )

    const handleDecideOnRelatedRequest = useCallback(
        (decision, requestId) => {
            if (window.confirm(`${t('Is this your decision?')} ${decision}`)) {
                setLoadingRelated(true);
                decideOnRequest(decision, requestId, (data) => { // load
                    loadRelatedRequests()
                    loadAcceptedRequests()
                    setLoadingRelated(false);
                    enqueueSnackbar(
                        decision === "accept"?
                            t("The mentoring request has been successfully added to your Active Requests where you can directly contact the mentee.")
                            :
                            t("The mentoring request has been successfully removed from your dashboard."),
                        {
                            variant: "success"
                        }
                    )
                },
                    error => { // on error
                        if (error) {
                            if (error.status === 404) {
                                setCriticalErrorRelated(t("404: Request was not found!"));
                            } else if (error.status >= 500) {
                                setCriticalErrorRelated(error.status + ": Server error!")
                            } else
                                setCriticalErrorRelated(error.data);
                        }
                        setLoadingRelated(false);
                    })
            }
        },
        [decideOnRequest, enqueueSnackbar, t],
    )

    const handleFinishRequest = useCallback(
        (requestId) => {
            if (window.confirm(t("Do you want to close this mentoring request?"))) {
                setLoadingAccepted(true);
                finishRequest(requestId, (data) => { // load
                    loadAcceptedRequests()
                    setLoadingAccepted(false);
                    enqueueSnackbar(
                        t("The mentoring request has been successfully closed."),
                        {
                            variant: "success"
                        }
                    )
                },
                    error => { // on error
                        if (error) {
                            if (error.status === 404) {
                                setCriticalErrorAccepted(t("404: Request was not found!"));
                            } else if (error.status >= 500) {
                                setCriticalErrorAccepted(error.status + ": Server error!")
                            } else
                                setCriticalErrorAccepted(error.data);
                        }
                        setLoadingRelated(false);
                    })
            }
        },
        [finishRequest, t, enqueueSnackbar],
    )

    useEffect(() => {
        loadRelatedRequests()
    }, [loadRelatedRequests]);

    useEffect(() => {
        loadAcceptedRequests()
    }, [loadAcceptedRequests]);

    return (
        <GridContainer>
            <GridItem xs={12}>
                <Card>
                    <CardHeader color="rose" className={classes.titleWithButton}>
                        <div className={classes.cardTitleDiv}>
                            <h3 className={classes.cardTitleWhite}>
                                {t("Mentoring Requests")}
                            </h3>
                            <h4 className={classes.cardCategoryWhite}>
                                {t("Watch your active and eligible requests")}
                            </h4>
                        </div>
                    </CardHeader>
                    <CardBody>
                        <Paper>
                            <Grid item xs={12} className={classes.col} align="center">
                                <Typography variant="h5" component="h2" style={{ margin: "20px" }}>
                                    <i> {t("Active Requests")} </i>
                                </Typography>
                            </Grid>
                            <AcceptedRequest
                                requests={acceptedRequest}
                                loading={loadingAccepted}
                                criticalError={criticalErrorAccepted}
                                onFinishRequest={handleFinishRequest}
                            />
                            <Grid item xs={12} className={classes.col} align="center" style={{ marginTop: "20px" }}>
                                <Typography variant="h5" component="h2" style={{ margin: "20px" }}>
                                    <i> {t("Eligible Requests")} </i>
                                </Typography>
                            </Grid>
                            <RelatedRequest
                                requests={relatedRequests}
                                loading={loadingRelated}
                                criticalError={criticalErrorRelated}
                                onDecideOnRelatedRequest={handleDecideOnRelatedRequest}
                            />
                        </Paper>
                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer>
    )
}

export default withSnackbar(Mentoring)
