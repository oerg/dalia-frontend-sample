import Axios from 'axios';
import { format, parseISO } from 'date-fns';
import React from 'react';
import { requestAddHandlers } from '../helpers';

export function getRelatedRequests(onSuccess, onError) { // get job and suggestions from the server
    requestAddHandlers(
        Axios.get(`/mentoring/get-related-requests?status=PE&ordering=-created_at`),
        onSuccess,
        onError
    );
}

export function getAcceptedRequests(onSuccess, onError) { // get job and suggestions from the server
    requestAddHandlers(
        Axios.get(`/mentoring/get-accepted-requests?ordering=-created_at`),
        onSuccess,
        onError
    );
}

export function decideOnRequest(decision, requestId, onSuccess, onError) { // get job and suggestions from the server
    requestAddHandlers(
        Axios.post(`/mentoring/${decision}-request/`, {request_id: requestId}),
        onSuccess,
        onError
    );
}

export function finishRequest(requestId, onSuccess, onError) {
    requestAddHandlers(
        Axios.post(`/mentoring/finish-request/`, {request_id: requestId}),
        onSuccess,
        onError
    );
}

export const renderGeneralCell = (params) => {
    const text = params.row[params.field]
    return (
        <span title={text} style={{textOverflow: "ellipsis", overflow: "hidden"}}>
            {text}
        </span>
    )
}

export const renderDateCell = (params) => {
    const date = parseISO(params.row[params.field])
    if (!isNaN(date)) {
        return format(date, "yyyy-MM-dd H:mm:ss")
    }
    return ""
}