import { Box } from '@material-ui/core';
import { DataGrid } from '@mui/x-data-grid';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '../../../components/CustomButton/Button';
import { renderDateCell, renderGeneralCell } from '../utils';

function alwaysFalse() {
    return false;
}

function RelatedRequest({ requests, loading, criticalError, onDecideOnRelatedRequest }) {
    const { t } = useTranslation(['dashboard']);
    const [pageSize, setPageSize] = useState(10)

    const renderDecisionButtons = (params) => {
        const { t } = useTranslation(['dashboard']);
        return (
            <strong>
                <Button
                    variant="contained"
                    color="success"
                    size="sm"
                    style={{ marginLeft: 16 }}
                    onClick={() => {
                        onDecideOnRelatedRequest("accept", params.row.id)
                    }}
                >
                    {t("Accept")}
                </Button>
                <Button
                    variant="contained"
                    color="danger"
                    size="sm"
                    style={{ marginLeft: 16 }}
                    onClick={() => {
                        onDecideOnRelatedRequest("reject", params.row.id)
                    }}
                >
                    {t("Reject")}
                </Button>
            </strong>
        )
    }

    const columns = useMemo(
        () => [
            {
                field: "requester",
                headerName: t("Requester"),
                width: 130,
                renderCell: renderGeneralCell,
                sortable: false,
            },
            {
                field: "title",
                headerName: t("On"),
                width: 220,
                renderCell: renderGeneralCell,
                sortable: false,
            },
            {
                field: "created_at",
                headerName: t("Request Date"),
                sortable: true,
                flex: 1,
                renderCell: renderDateCell,
            },
            {
                field: "request_text",
                headerName: t("Request Text"),
                sortable: false,
                width: 400,
                renderCell: renderGeneralCell,
            },
            {
                field: 'decision',
                headerName: t('Your decision?'),
                width: 270,
                renderCell: renderDecisionButtons,
                disableClickEventBubbling: true,
                sortable: false,
            },
        ],
        [t]
    );

    const handlePageSizeChange = useCallback(
        (pageSize) => {
            setPageSize(pageSize)
        },
        [],
    )

    return (
        criticalError ?
            <Box p={10} textAlign="center">
                {criticalError}
            </Box>
            :
            <DataGrid
                style={{
                    width: "100%",
                    minHeight: 400,
                }}
                columns={columns}
                rows={requests}
                isCellEditable={alwaysFalse}
                loading={loading}
                onPageSizeChange={handlePageSizeChange}
                pageSize={pageSize}
                rowsPerPageOptions={[10, 25, 50]}
            // autoHeight={true}

            />
    )
}

export default RelatedRequest

