import { Box } from '@material-ui/core';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DataGrid } from '@mui/x-data-grid';
import Button from '../../../components/CustomButton/Button';
import { renderDateCell, renderGeneralCell } from '../utils';

function alwaysFalse() {
    return false;
}


function AcceptedRequest( {requests, loading, criticalError, onFinishRequest} ) {
    const { t } = useTranslation(['dashboard']);
    const [pageSize, setPageSize] = useState(10)

    const renderFinishButton = (params) => {
        const { t } = useTranslation(['dashboard']);
        return (
            <strong>
                <Button
                    variant="contained"
                    color="info"
                    size="sm"
                    style={{ marginLeft: 16 }}
                    onClick={() => {
                        onFinishRequest(params.row.request)
                    }}
                >
                    {t("Close Request")}
                </Button>  
            </strong>
        )
    }

    const Mailto = ({ email, subject, body, children }) => {
        return (
          <a href={`mailto:${email}?subject=${encodeURIComponent(subject) || ''}&body=${encodeURIComponent(body) || ''}`}>{children}</a>
        );
      };

    const renderContactCell = (params) => {
        const { t } = useTranslation(['dashboard']);
        const requester = params.row.requester.split(" ")[0]
        const firstName = requester.charAt(0).toUpperCase() + requester.slice(1)
        return (
            <span title={`${t("send mail to:")} ${params.row.requester_contact}`} style={{textOverflow: "ellipsis", overflow: "hidden"}}>
                <Mailto
                    email={params.row.requester_contact}
                    subject={t("[eDoer Mentoring] Welcome")}
                    body={t("Hello") + " " + firstName + ","}
                >
                    {params.row.requester_contact}
                </Mailto>
            </span>            
        )
    }

    const columns = useMemo(
        () => [
            {
                field: "requester",
                headerName: t("Requester"),
                width: 130,
                renderCell: renderGeneralCell,
                sortable: false,
            },
            {
                field: "request_title",
                headerName: t("On"),
                width: 220,
                renderCell: renderGeneralCell,
                sortable: false
            },
            {
                field: "request_updated_at",
                headerName: t("Accepting Date"),
                sortable: true,
                width: 150,
                renderCell: renderDateCell,
            },
            {
                field: "request_text",
                headerName: t("Request Text"),
                sortable: false,
                width: 250,
                renderCell: renderGeneralCell,
            },
            {
                field: "requester_contact",
                headerName: t("Contact"),
                sortable: false,
                width: 250,
                renderCell: renderContactCell,
            },        
            {
                field: 'action',
                headerName: t('Close Request'),
                width: 170,
                sortable: false,
                renderCell: renderFinishButton,
                disableClickEventBubbling: true,
            },        
        ],
        [t]
    );

    const handlePageSizeChange = useCallback(
        (pageSize) => {
            setPageSize(pageSize)
        },
        [],
    )

    return (
        criticalError ?
        <Box p={10} textAlign="center">
            {criticalError}
        </Box>
        :
        <DataGrid
            style={{
                width: "100%",
                minHeight: 400,
            }}
            columns={columns}
            rows={requests}
            isCellEditable={alwaysFalse}
            loading={loading}
            onPageSizeChange={handlePageSizeChange}
            pageSize={pageSize}
            rowsPerPageOptions={[10, 25, 50]} 
        />
    )
}

export default AcceptedRequest

