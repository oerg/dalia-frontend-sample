import axios from "axios";
import { requestAddHandlers } from "../helpers";

function questionsUrl(exam, skillId, topicId) {
    return exam ?
        "/members/get-skill-assessment?skill-id=" + skillId
    :
        topicId ?
            "/members/get-topic-assessment?topic-id=" + topicId
        :
            "/members/get-topic-assessment?skill-id=" + skillId
}

function answersUrl(exam) {
    return exam ?
        '/members/check-skill-assessment/'
    :
        '/members/check-topic-assessment/'
}

export function loadQuestions(exam, skillId, topicId, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.get(questionsUrl(exam, skillId, topicId)),
        onLoad,
        onError,
        onEnd
    )
}

export function skipAssessment(skill_id, topic_id, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.post('/members/skip-topic-assessment/', {skill_id, topic_id}),
        onLoad,
        onError,
        onEnd
    );
}

export function checkQuestions(questions, exam, examId, skillId, topicId, onLoad, onError, onEnd) {
    const data = {
        answers: questions.map(q => ({id: q.id, answer: q.answer}))
    }
    if (exam)
        data['assessment_id'] = examId;
    else if (topicId)
        data['topic_id'] = topicId;
    else
        data['skill_id'] = skillId;

    requestAddHandlers(
        axios.post(answersUrl(exam), data),
        onLoad,
        onError,
        onEnd
    );
}

export function shuffle(array) {
    var currentIndex = array.length,  randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
  
    return array;
}