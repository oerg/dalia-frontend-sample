
import { Box, Button, CircularProgress, Grid, Paper, Typography } from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/People';
import ShareIcon from '@material-ui/icons/Share';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { Link, useParams } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import Card from '../../components/Card/Card';
import CardBody from '../../components/Card/CardBody';
import CardHeader from '../../components/Card/CardHeader';
import GridContainer from '../../components/Grid/GridContainer';
import GridItem from '../../components/Grid/GridItem';
import Icons from '../../components/Icons';
import LangIcon from '../../components/LangIcon';
import LoginWarning from '../../components/LoginWarning';
import TopicLogo from '../../components/Logos/Topic';
import ShareButton from '../../components/ShareButton';
import { getTopic } from '../Discovery/Topics/utils';
import useStyles from '../styles/SkillsStyle';
import OergCard from './components/OergCard';

export default function Topics({ basePath, user, userData }) {
    const classes = useStyles();
    const { id: strTopicId } = useParams();
    
    const topicId = useMemo(
        () => {
            if (strTopicId) {
                return parseInt(strTopicId);
            }
            return null;
        },
        [strTopicId]
    )

    const [topic, setTopic] = useState(null);
    const [oergs, setOergs] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [criticalError, setCriticalError] = useState(null);

    const { t } = useTranslation([ 'dashboard' ]);
    const language = useLanguage();
    const history = useHistory();
    
    useEffect(() => { // load job and suggestions
        setLoading(true);
        getTopic(topicId, (data) => { // load
            setOergs(data.oer_groups);
            setTopic(data);
            setLoading(false);
        }, error => { // on error
            if (error) {
                if (error.status === 404) {
                    setCriticalError("404: Topic was not found!");
                } else if (error.status >= 500) {
                    setCriticalError(error.status + ": Server error!")
                } else
                    setCriticalError(error.data);
            }
            setLoading(false);
        });

    }, [topicId]);

    
    const [showLoginWarning, setShowLoginWarning] = useState(false);
    const handleLogin = useCallback((action) => {
        setShowLoginWarning(false);
        if (action) {
            history.push(action);
        }
    }, [history, topicId]);

    return (
        <>
        {isLoading ?
            <Box p={10} textAlign="center">
                <CircularProgress />
            </Box>
        :
        criticalError ?
        <Box p={10} textAlign="center">
            {criticalError}
        </Box>
        :
        <GridContainer>
            <GridItem xs={12}>
                <Card>
                    <CardHeader color="rose" className={classes.titleWithButton}>
                        <TopicLogo className={classes.logo} picture={topic.picture} size="large" />
                        <div className={classes.cardTitleDiv}>
                            <h3 className={classes.cardTitleWhite}>
                                {topic.title} <LangIcon language={topic.lang} small />
                            </h3>
                        </div>
                        <div className={classes.cardButtonDiv}>
                            { (user.loggedIn && userData.settings.discovery)  &&
                                <Button title={t("Participate in this Topic")} variant="outlined"
                                    component={Link} to={`/${language}/dashboard/discovery/topics/${topicId}`}
                                >
                                    <PeopleIcon />
                                </Button>
                            }
                            <ShareButton 
                                component={Button} 
                                variant="outlined"
                                url={`${window.location.href}`}
                                title={topic.title}
                                componentTitle={t("Share this Topic")}
                            >
                                <ShareIcon />
                            </ShareButton>
                            <Icons hideLearnable hideAddable assessmentable={topic.assessmentable} withBorder={true} />
                        </div>
                    </CardHeader>
                    <CardBody>
                        { topic.description &&
                        <Paper elevation={2} style={{ marginTop: 5, marginBottom: 25, padding:20 }}>
                            {topic.description}
                        </Paper>}
                        <Grid container spacing={3} className={classes.hasPadding} alignContent="stretch">
                            <Grid item xs={12} className={classes.col} align="center">
                                <Typography variant="h5" component="h2">
                                    {t("Sample Educational Packages on: ")} <i>{topic.title}</i>
                                </Typography>
                            </Grid>
                            { oergs.length === 0 ?
                                <i>{t("There are no educational content.")}</i>
                            :
                                oergs.map((oerg, ind) => (
                                    <Grid key={ind} item xs={12} md={6} lg={4}>
                                        <OergCard oerg={oerg} basePath={`${window.location.protocol}//${window.location.host}${basePath}`}  />
                                    </Grid>                
                                ))
                            }
                        </Grid>
                    </CardBody>                    
                </Card>
            </GridItem>
            <LoginWarning show={!user.loggedIn && showLoginWarning} onSelect={handleLogin} />
        </GridContainer>
        }
        </>
        )
}
