
import { Box, Button, CircularProgress, Grid, Paper, Typography } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import DoneIcon from '@material-ui/icons/Done';
import PeopleIcon from '@material-ui/icons/People';
import ShareIcon from '@material-ui/icons/Share';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { Link, useParams } from 'react-router-dom';
import { useLanguage } from '../../../i18n';
import Card from '../../components/Card/Card';
import CardBody from '../../components/Card/CardBody';
import CardHeader from '../../components/Card/CardHeader';
import GridContainer from '../../components/Grid/GridContainer';
import GridItem from '../../components/Grid/GridItem';
import Icons from '../../components/Icons';
import LangIcon from '../../components/LangIcon';
import LoginWarning from '../../components/LoginWarning';
import SkillLogo from '../../components/Logos/Skill';
import ShareButton from '../../components/ShareButton';
import { getSkillOnly } from '../Discovery/Skills/utils';
import { saveSkill } from '../Goals/utils';
import useStyles from '../styles/SkillsStyle';
import TopicCard from './components/TopicCard';

export default function Skills({ basePath, user, userData }) {
    const classes = useStyles();
    const { id: strSkillId } = useParams();
    
    const skillId = useMemo(
        () => {
            if (strSkillId) {
                return parseInt(strSkillId);
            }
            return null;
        },
        [strSkillId]
    )

    const [skill, setSkill] = useState(null);
    const [topics, setTopics] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [isSaving, setIsSaving] = useState(false)
    const [criticalError, setCriticalError] = useState(null);
    const [userReloadNeeded, setUserReloadNeeded] = useState(false);

    const { t } = useTranslation([ 'dashboard' ]);
    const language = useLanguage();
    const history = useHistory();

    useEffect(
        () => {
            if (userReloadNeeded)
                return () => user.reload()
        },
        [ userReloadNeeded ]
    )
    
    useEffect(() => { // load skill and its topics
        setLoading(true);
        getSkillOnly(skillId, (data) => { // load
            setTopics(data.data.skilltopic_set.sort((a, b) => a.order - b.order));
            setSkill(data.data);
            setLoading(false);
        }, error => { // on error
            if (error) {
                if (error.status === 404) {
                    setCriticalError("404: Course was not found!");
                } else if (error.status >= 500) {
                    setCriticalError(error.status + ": Server error!")
                } else
                    setCriticalError(error.data);
            }
            setLoading(false);
        });
    }, [skillId]);

    const handleAddSkill = useCallback( 
        () => {
            if (!user.loggedIn) { 
                setShowLoginWarning(true);
                return;
            }
    
            if (window.confirm(`${t("Do you want to learn this course?")} ${skill.title}`)) {
                setIsSaving(true);
                saveSkill(skill.id,
                    true,
                    null,
                    null,
                    null,
                    () => {
                        setIsSaving(false);
                        // setSkill(targetSkill => {
                        //     targetSkill.is_current = true
                        //     return targetSkill
                        // });
                        setUserReloadNeeded(true);
                        if (window.confirm(t("The course has been added successfully. Do you want to go to your learning dashboard?"))) {
                            history.push(`/${language}/dashboard/edu`)
                        }                        
                    }
                )
            }
        },
        [skill, user, t, language]
    );

    const [showLoginWarning, setShowLoginWarning] = useState(false);
    const handleLogin = useCallback((action) => {
        setShowLoginWarning(false);
        if (action) {
            history.push(action);
        }
    }, [history, skillId]);

    return (
        <>
        {isLoading ?
            <Box p={10} textAlign="center">
                <CircularProgress />
            </Box>
        :
        criticalError ?
        <Box p={10} textAlign="center">
            {criticalError}
        </Box>
        :
        <GridContainer>
            <GridItem xs={12}>
                <Card>
                    <CardHeader color="rose" className={classes.titleWithButton}>
                        <SkillLogo className={classes.logo} picture={skill.picture} size="large" />
                        <div className={classes.cardTitleDiv}>
                            <h3 className={classes.cardTitleWhite}>
                                {skill['title']}&nbsp;
                                <LangIcon language={skill.lang} small />&nbsp;
                                <Icons learnable={skill.learnable} assessmentable={skill.assessmentable} addable={skill.addable} withBorder={true} />
                            </h3>
                        </div>
                        <div className={classes.cardButtonDiv}>
                            { (user.loggedIn && userData.settings.discovery) &&
                                <Button title={t("Participate in this Course")} variant="outlined"
                                    disabled={isSaving} component={Link} to={`/${language}/dashboard/discovery/skills/${skillId}`}
                                >
                                    <PeopleIcon />
                                </Button>
                            }
                            <ShareButton 
                                component={Button} 
                                variant="outlined"
                                url={`${window.location.href}`}
                                title={`${t("Learn this course on eDoer.eu: ")} ${skill.title}`}
                                componentTitle={t("Share this course")}
                            >
                                <ShareIcon />
                            </ShareButton>
                            {
                                skill.addable &&                               
                                    <Button title={skill.is_my_skill ? t("This is already set as your target courses") : t("Add as my target course")} 
                                        variant="outlined" onClick={handleAddSkill} disabled={isSaving || skill.is_my_skill}
                                        classes={{
                                            disabled: classes.addButtonDisabled
                                        }}>
                                        { skill.is_my_skill ? 
                                            <DoneIcon />
                                        :
                                            <AddIcon />
                                        }
                                    </Button>
                            }
                        </div>
                    </CardHeader>
                    <CardBody>
                        { skill.description &&
                        <Paper elevation={2} style={{ marginTop: 5, marginBottom: 25, padding:20 }}>
                            {skill.description}
                        </Paper>}
                        <Grid container spacing={3} className={classes.hasPadding} alignContent="stretch">
                            <Grid item xs={12} className={classes.col} align="center">
                                <Typography variant="h5" component="h2">
                                    {t("You will learn these topics through achieving:")} <i>{skill['title']}</i>
                                </Typography>
                            </Grid>
                            { topics.map((topic, ind) => (
                                <Grid key={ind} item xs={12} md={6} lg={4}>                
                                    <TopicCard topic={topic}
                                        addDisabled={isSaving} basePath={`${window.location.protocol}//${window.location.host}${basePath}`} />
                                </Grid>                
                            ))}
                        </Grid>
                    </CardBody>                    
                </Card>
            </GridItem>
            <LoginWarning show={!user.loggedIn && showLoginWarning} onSelect={handleLogin} />
        </GridContainer>
        }
        </>
        )
}
