import { Tooltip, Typography } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import React from 'react';

export default function ReportItem({ image, alt, icon: Icon, tooltip, title, value, enabled=true, loading }) {

    return (
        <>
            {loading ?
                <>
                    <Skeleton variant="text" />
                    <Skeleton variant="rect" />
                </>
                :
                <>
                    <Tooltip
                        arrow
                        interactive
                        title={tooltip}
                    >
                        {image ?
                            <img
                                height={79}
                                src={process.env.PUBLIC_URL + `/images/levels/${image}`}
                                alt={alt}
                                style={{
                                    filter: enabled ? 'grayscale(0%)' : 'grayscale(100%)'
                                }}
                            />
                            :
                            <Icon style={{fontSize: 80, color: enabled ? "#00006E" : "#000"}}/>

                        }
                    </Tooltip>
                    <br />
                    {title && value &&
                        <Typography variant='caption'>
                            {title} : <em>{value}</em>
                        </Typography>
                    }
                </>
            }
        </>
    )
}
