import DateFnsUtils from '@date-io/date-fns';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

function DateDialog({ value: propValue, onChange, onClose, clearable,...props }) {
    const { t } = useTranslation(['dashboard'])
    const [value, setValue] = useState(null)

    useEffect(
        () => {
            setValue(propValue)
        },
        [propValue]
    )

    const handleChange = useCallback(
        (newDate) => {
            setValue(newDate)
        },
        []
    )

    const handleOk = useCallback(
        () => {
            onChange(value)
        },
        [onChange, value]
    )

    const handleClear = useCallback(
        () => {
            setValue(null)
            onChange(null)
        },
        [onChange]
    )

    return (
        <Dialog {...props} onClose={onClose}>
            <DialogTitle>
                {t("Select Deadline")}
            </DialogTitle>
            <DialogContent>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <DatePicker
                        autoOk
                        ampm={false}
                        variant="static"
                        value={value}
                        onChange={handleChange}
                        disablePast
                        clearable={clearable}
                    />
                </MuiPickersUtilsProvider>
            </DialogContent>
            <DialogActions>
                {clearable && <Button
                    color='secondary'
                    onClick={handleClear}
                >
                    {t("Clear Deadline")}
                </Button>}
                <Button
                    color='secondary'
                    onClick={onClose}
                >
                    {t("Cancel")}
                </Button>
                <Button
                    color='primary'
                    onClick={handleOk}
                >
                    {t("OK")}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default DateDialog