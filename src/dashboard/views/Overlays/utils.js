import axios from "axios";
import { requestAddHandlers } from "../helpers";

export function sendMenotringRequest(object_id, object_type, request_text, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.post(
            "/mentoring/",
            {
                object_id,
                object_type,
                request_text
            }
        ),
        onLoad,
        onError,
        onEnd
    );
}