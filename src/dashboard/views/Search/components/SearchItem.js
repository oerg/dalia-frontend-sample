import React from 'react'
import GoalCard from '../../../components/JobItem/GoalCard'
import SkillSearchCard from '../../../components/SkillItem/SkillSearchCard'
import TopicSearchCard from './TopicSearchCard'

function SearchItem({ result, ...props }) {
    return result.object_type === "SK" ?
        <SkillSearchCard skill={result} {...props} />
        : result.object_type === "TO" ?
        <TopicSearchCard topic={result} {...props} />
        :
        <GoalCard showFollowButton goal={result} {...props} />
}

export default SearchItem