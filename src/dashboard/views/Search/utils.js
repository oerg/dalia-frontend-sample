import { MultiCall } from "../helpers";

const SearchCode = "Search_MCCode"

export function doSearch(term, page, extraParams, isJourneyOnly, onLoad, onError, onEnd) {
    const mc = MultiCall.getInstance(SearchCode)
    let url = ''
    const params = new URLSearchParams(extraParams).toString()
    if (isJourneyOnly) {
        url = `/jobs/search/?search-term=${term}&${params}`
    } else {
        url = `/members/search/?search-term=${term}&page=${page}&${params}`
    }

    return mc.call(url, onLoad, onError, onEnd)
}

export function cancelDoSearch() {
    MultiCall.getInstance(SearchCode).cancel()
}

const TopicSearchCode = "Search_MCTCode_ASDSADSDADSDSAD"

export function doTopicSearch(term, onLoad, onError, onEnd) {
    const mc = MultiCall.getInstance(TopicSearchCode)
    return mc.call(`/skills/topics/?search=${term}`, onLoad, onError, onEnd)
}

export function cancelDoTopicSearch() {
    MultiCall.getInstance(TopicSearchCode).cancel()
}