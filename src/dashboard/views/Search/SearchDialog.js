import { Checkbox, Collapse, Dialog, DialogContent, FormControlLabel, IconButton, Input, InputAdornment, makeStyles } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import { Alert, Skeleton } from '@material-ui/lab'
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import SearchItem from './components/SearchItem'
import { cancelDoSearch, cancelDoTopicSearch, doSearch, doTopicSearch } from './utils'

const useStyles = makeStyles({
    form: {

    },
    results: {
        marginTop: 16,
        minHeight: 150,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center"
    }
})

function SearchDialog({ onClose, onAddRequest, onUnarchiveRequest, showTopicOption = false, journeyOnly = false, noActions = false, ...props }) {
    const classes = useStyles()
    const { t } = useTranslation(['dashboard'])
    const inputRef = useRef()

    const [term, setTerm] = useState('')
    const [loading, setLoading] = useState(false)
    const [searchStarted, setSearchStarted] = useState(false)
    const [results, setResults] = useState([])
    const [resultsCount, setResultsCount] = useState(0)
    const [currentFocus, setCurrentFocus] = useState(null)
    const [enableTopicSearch, setEnableTopicSearch] = useState(false)

    useEffect(
        () => {
            if (!showTopicOption)
                setEnableTopicSearch(false)
        },
        [showTopicOption]
    )

    useEffect(
        () => {
            if (currentFocus === 0) {
                if (inputRef.current)
                    inputRef.current.focus()
            }
        },
        [currentFocus]
    )

    const handleKeyDown = useCallback(
        (e) => {
            if (e.key === "ArrowDown") {
                setCurrentFocus(f => f === results.length ? f : f + 1)
            } else if (e.key === "ArrowUp") {
                setCurrentFocus(f => f === 0 ? f : f - 1)
            }
        },
        [results]
    )

    const search = useCallback(
        (term, topicOnly, journeyOnly) => {
            if (term.length <= 0) {
                return
            }

            setLoading(true)
            setSearchStarted(true)
            if (topicOnly) {
                cancelDoTopicSearch()
                doTopicSearch(
                    term,
                    ({ data }) => {
                        setResults(data.results.map(r => ({ ...r, object_type: "TO" })))
                        setResultsCount(data.count)
                    },
                    null,
                    () => setLoading(false)
                )
            } else {
                cancelDoSearch()
                doSearch(
                    term,
                    1,
                    {
                        addable: 1,
                    },
                    journeyOnly,
                    ({ data }) => {
                        if (journeyOnly) {
                            setResults(data.results.map(r => ({ ...r, object_type: "JO" })))
                        } else {
                            setResults(data.results)
                        }
                        setResultsCount(data.count)
                    },
                    null,
                    () => setLoading(false)
                )

            }
        },
        []
    )

    useEffect(
        () => {
            search(
                term,
                enableTopicSearch,
                journeyOnly
            )
        },
        [term, enableTopicSearch, journeyOnly, search]
    )

    const handleSearch = useCallback(
        (e) => {
            if (e) {
                e.preventDefault()
            }
            search(
                term,
                enableTopicSearch,
                journeyOnly
            )
        },
        [term, journeyOnly, enableTopicSearch, search]
    )

    const handleChangeTerm = useCallback(
        e => {
            setTerm(e.target.value)
            setSearchStarted(false)
        },
        []
    )

    const handleClose = useCallback(
        (e, reason) => {
            setTerm('')
            setLoading(false)
            setSearchStarted(false)
            setResults([])
            setResultsCount(0)
            setCurrentFocus(null)
            if (onClose)
                onClose(e, reason)
        },
        [onClose]
    )

    const handleTopicSearch = useCallback(
        e => {
            setEnableTopicSearch(e.target.checked)
            setResults([])
            setResultsCount(0)
            setSearchStarted(false)
            setCurrentFocus(0)
        },
        [handleSearch]
    )

    return <Dialog scroll='paper' onClose={handleClose} {...props} maxWidth='sm' fullWidth>
        <DialogContent className={classes.content}>
            <form onSubmit={handleSearch}>
                <Input
                    onKeyDown={handleKeyDown}
                    endAdornment={
                        <InputAdornment position='end'>
                            <IconButton type="submit">
                                <SearchIcon />
                            </IconButton>
                        </InputAdornment>
                    }
                    style={{
                        fontSize: "1.5rem",
                        padding: 8,
                    }}
                    variant="outlined"
                    autoFocus
                    placeholder={t("Type to search...")}
                    fullWidth
                    value={term}
                    onChange={handleChangeTerm}
                    inputRef={inputRef}
                />
                {showTopicOption &&
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={enableTopicSearch}
                                onChange={handleTopicSearch}
                                disabled={loading}
                                color="primary"
                            />
                        }
                        label={t("I am only looking for small learning topics.")}
                    />}
            </form>
            <div className={classes.results}>
                <Collapse in={!searchStarted}>
                    <Alert
                        severity='info'
                    >
                        {t("Type the term to start your search.")}
                    </Alert>
                </Collapse>
                <Collapse in={searchStarted && term.length <= 3 && results.length > 0}>
                    <Alert
                        severity='warning'
                    >
                        {t("Your term is too short. The results may be unrelated.")}
                    </Alert>
                </Collapse>
                <Collapse in={loading}>
                    <Skeleton animation="wave" height={120} />
                    <Skeleton animation="wave" height={120} />
                    <Skeleton animation="wave" height={120} />
                </Collapse>
                <Collapse in={!loading && results.length > 0}>
                    {results.map(
                        (result, index) => <SearchItem
                            key={index}
                            result={result}
                            selected={currentFocus === index + 1}
                            onKeyDown={handleKeyDown}
                            onAddRequest={onAddRequest}
                            onUnarchiveRequest={onUnarchiveRequest}
                            noActions={noActions}
                        />
                    )}
                </Collapse>
                <Collapse in={searchStarted && !loading && resultsCount === 0}>
                    <Alert
                        severity='warning'
                    >
                        {t("We couldn't find any results. Please try a different keyword.")}
                    </Alert>
                </Collapse>
            </div>
        </DialogContent>
    </Dialog>
}

export default SearchDialog