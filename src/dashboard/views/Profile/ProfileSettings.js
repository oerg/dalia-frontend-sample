import DateFnsUtils from '@date-io/date-fns';
import { Avatar, Box, Chip, CircularProgress, FormControl, FormHelperText, Grid, Input, InputLabel, MenuItem, Paper, TextField as MuiTextField, Typography, withWidth } from '@material-ui/core';
import Checkbox from "@material-ui/core/Checkbox";
import { green, red } from '@material-ui/core/colors';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { withStyles } from '@material-ui/core/styles';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import Axios from 'axios';
import { format } from 'date-fns';
import { Field, Form, Formik } from 'formik';
import { Select, TextField } from 'formik-material-ui';
import { DatePicker } from 'formik-material-ui-pickers';
import { withSnackbar } from 'notistack';
import React, { useCallback, useContext, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import * as Yup from 'yup';
import { UserContext } from '../../../landing/User';
import CitySelect from '../../components/CitySelect';
import CountrySelect from '../../components/CountrySelect';
import Button from '../../components/CustomButton/Button';
import SearchAutocomplete from '../../components/SearchAutocomplete/SearchAutocomplete';
import Snackbar from '../../components/Snackbar/Snackbar';
import ToggleDiv from '../../components/ToggleDiv';
import ReportItem from '../Education/components/ReportComponents/ReportItem';
import { cancelLoadAreas, changeSetting, loadAreas, formatDate } from './utils';
import SpeedIcon from '@material-ui/icons/Speed';
import AirplanemodeInactiveIcon from '@material-ui/icons/AirplanemodeInactive';
import LevelReport from '../Education/components/ReportComponents/LevelReport';
import GoalReport from '../Education/components/ReportComponents/GoalReport';
import InteractivePaper from '../../components/InteractivePaper';


const styles = theme => ({
    fieldPaper: {
        marginTop: theme.spacing(1),
    },

    formBox: {
        padding: theme.spacing(2),
    },

    wrapper: {
        width: '100%',
        backgroundColor: "#FFFFFF",
        marginTop: theme.spacing(1),
        paddingTop: theme.spacing(1),
    },

    errorButton: {
        backgroundColor: red[500],
        '&:hover': {
            backgroundColor: red[700],
        }
    },

    successButton: {
        backgroundColor: green[500],
        '&:hover': {
            backgroundColor: green[700],
        }
    },

    emailProgress: {
        color: green[500],
    },

    tosLabel: {
        textAlign: 'justify',
    },

    hiddenOption: {
        display: 'none',
    },
});

function ProfileSettings(rawPorps) {
    const { classes, enqueueSnackbar } = rawPorps;
    const user = useContext(UserContext);
    const userData = useMemo(() => user.data, [user]);
    const [message, setMessage] = useState('');
    const [checkedDiscoverySetting, setCheckedDiscoverySetting] = useState(userData.settings.discovery)
    const [checkedMentoringSetting, setCheckedMentoringSetting] = useState(userData.settings.mentoring)
    const [loadingSetting, setLoadingSetting] = useState(false);
    const [reportLevel, setReportLevel] = useState(userData.settings.report_level)
    const [reportSpeed, setReportSpeed] = useState(userData.settings.report_speed)
    const [reportDeadline, setReportDeadline] = useState(userData.settings.report_deadline)
    const [reportInactivity, setReportInactivity] = useState(userData.settings.report_inactivity)
    const [reportNextLevel, setReportNextLevel] = useState(userData.settings.report_next_level)
    const [loadingReport, setLoadingReport] = useState(false);
    const [areas, setAreas] = useState([])
    const [loadingAreas, setLoadingAreas] = useState(false)
    const showMessage = Boolean(message);
    const { t } = useTranslation(['dashboard']);

    const genders = useMemo(
        () => [
            { name: t('Male'), code: 'ML' },
            { name: t('Female'), code: 'FM' },
            { name: t('Other'), code: 'OT' },
        ],
        [t]
    );

    const startDate = useMemo(
        () => {
            const time = new Date().getTime() - 10 * 24 * 60 * 60 * 1000;
            return formatDate(new Date(time), "yyyy-MM-dd");
        },
        []
    )

    const deadline = useMemo(
        () => {
            const time = new Date().getTime() + 10 * 24 * 60 * 60 * 1000;
            return formatDate(new Date(time), "yyyy-MM-dd");
        },
        []
    )

    const changePassSchema = useMemo(
        () => Yup.object({
            current: Yup.string()
                .required(t('Current Password is required.')),
            newPass1: Yup.string()
                .required(t('You must choose a password.'))
                .min(8, t('Password must be at least 8 characters.')),
            newPass2: Yup.string()
                .required(t('Password confirmation is required.'))
                .oneOf([Yup.ref('newPass1'), null], t('Passwords do not match.')),
        }),
        [t]
    );


    const profileSchema = useMemo(
        () => Yup.object({
            birthDate: Yup.date().label('Birthdate').notRequired(),
        }),
        [t]
    )

    const handleDiscoverySetting = (event) => {
        setLoadingSetting(true)
        const checked = event.target.checked
        changeSetting(
            "discovery",
            event.target.checked,
            res => {
                setCheckedDiscoverySetting(checked)
                user.setData(data => {
                    const dataCopy = Object.assign({}, data);
                    dataCopy.settings.discovery = checked
                    return dataCopy
                })
            },
            null,
            () => setLoadingSetting(false)
        );
    };

    const handleMentoringSetting = (event) => {
        setLoadingSetting(true)
        const checked = event.target.checked
        changeSetting(
            "mentoring",
            event.target.checked,
            res => {
                setCheckedMentoringSetting(checked)
                user.setData(data => {
                    const dataCopy = Object.assign({}, data);
                    dataCopy.settings.mentoring = checked
                    return dataCopy
                })
            },
            null,
            () => setLoadingSetting(false)
        );
    };

    const handleReportSettingsLevelChanged = useCallback(
        (value) => {
            setLoadingReport(true)
            changeSetting(
                "report_level",
                value,
                res => {
                    setReportLevel(value)
                    user.setData(data => {
                        const dataCopy = Object.assign({}, data);
                        dataCopy.settings.report_level = value
                        return dataCopy
                    })
                },
                null,
                () => setLoadingReport(false)
            );
        },
        [userData, user],
    )

    const handleReportSettingsSpeedChange = useCallback(
        (value) => {
            setLoadingReport(true)
            changeSetting(
                "report_speed",
                value,
                res => {
                    setReportSpeed(value)
                    user.setData(data => {
                        const dataCopy = Object.assign({}, data);
                        dataCopy.settings.report_speed = value
                        return dataCopy
                    })
                },
                null,
                () => setLoadingReport(false)
            );
        },
        [userData, user],
    )

    const handleReportSettingsInactivityChange = useCallback(
        (value) => {
            setLoadingReport(true)
            changeSetting(
                "report_inactivity",
                value,
                res => {
                    setReportInactivity(value)
                    user.setData(data => {
                        const dataCopy = Object.assign({}, data);
                        dataCopy.settings.report_inactivity = value
                        return dataCopy
                    })
                },
                null,
                () => setLoadingReport(false)
            );
        },
        [userData, user],
    )

    const handleReportSettingsNextLevelChange = useCallback(
        (value) => {
            setLoadingReport(true)
            changeSetting(
                "report_next_level",
                value,
                res => {
                    setReportNextLevel(value)
                    user.setData(data => {
                        const dataCopy = Object.assign({}, data);
                        dataCopy.settings.report_next_level = value
                        return dataCopy
                    })
                },
                null,
                () => setLoadingReport(false)
            );
        },
        [userData, user],
    )

    const handleReportSettingsDeadlineChange = useCallback(
        (value) => {
            setLoadingReport(true)
            changeSetting(
                "report_deadline",
                value,
                res => {
                    setReportDeadline(value)
                    user.setData(data => {
                        const dataCopy = Object.assign({}, data);
                        dataCopy.settings.report_deadline = value
                        return dataCopy
                    })
                },
                null,
                () => setLoadingReport(false)
            );
        },
        [userData, user],
    )


    const handleDelete = useCallback(() => {
        if (window.confirm(t('This action will delete your data from our servers. PLEASE CONTINUE WITH CAUTION!'))) {
            if (window.confirm(t('Are you sure you want to DELETE ALL YOUR PROFILE from our servers? (There is no going back from here!)'))) {
                Axios.delete('/members/delete-profile/')
                    .then(() => {
                        user.logout();
                    })
            }
        }
    }, [t]);

    const handleFile = useCallback(
        e => {
            if (!e.target.files[0] || e.target.files[0].size > 2097152) { // 2MB
                enqueueSnackbar(
                    t("Please select a file which is not larger than 2 MB"),
                    {
                        varaint: "warning"
                    }
                )
                return;
            }
            var data = new FormData();
            data.append('file', e.target.files[0]);
            Axios.post('/members/upload-profile-picture/', data)
                .then(
                    res => user.reload()
                )
                .catch(
                    res => {
                        enqueueSnackbar(
                            t("Upload failed. Please try again."),
                            {
                                variant: "error"
                            }
                        )
                    }
                )
        },
        [user, t, enqueueSnackbar]
    );

    const handleAreasSearch = useCallback(
        (section, value) => {
            setLoadingAreas(false)
            if (value) {
                setAreas([{ text: value }])
                setLoadingAreas(section)
                loadAreas(
                    value,
                    (res) => {
                        const lowerValue = value.toLowerCase()
                        if (res.data.findIndex(i => i.toLowerCase() === lowerValue) === -1) {
                            res.data.push(value)
                        }
                        setAreas(res.data.map(i => ({ text: i })))
                    },
                    null,
                    () => setLoadingAreas(false)
                )
            } else {
                cancelLoadAreas()
                setAreas([])
            }
        },
        []
    )

    const handleSearchAreasExpertise = useCallback(
        (value) => handleAreasSearch("expertise", value),
        [handleAreasSearch]
    )

    const handleSearchAreasLearning = useCallback(
        (value) => handleAreasSearch("learning", value),
        [handleAreasSearch]
    )

    return (
        <>
            <Grid container justifyContent="center" className={classes.wrapper}>
                <Grid item xs={12} md={8} lg={6}>
                    <Formik
                        validationSchema={profileSchema}
                        onSubmit={(values, { setStatus, setSubmitting }) => {
                            setStatus(false);
                            // parsing values
                            const { birthDate: birthdateObj, first_name, last_name, ...restValues } = values;
                            const birthdateFormatted = format(birthdateObj, 'yyyy-MM-dd');
                            const neededValues = {
                                user: {
                                    first_name, last_name
                                },
                                birthdate: birthdateFormatted,
                                ...restValues
                            };
                            Axios.put('/members/' + userData.user.id + '/', neededValues)
                                .then(res => {
                                    user.reload();
                                })
                                .catch(err => {
                                    if (err.request) { // no response
                                        setStatus(t('No response from the server.'));
                                    } else { // something else
                                        setStatus(err.message);
                                    }
                                    setSubmitting(false);
                                })
                        }}
                        initialValues={{
                            first_name: userData.user.first_name,
                            last_name: userData.user.last_name,
                            gender: userData.gender,
                            country: userData.country,
                            city: userData.city,
                            birthDate: userData.birthdate ? new Date(userData.birthdate) : new Date(),
                            bio: userData.bio,
                            expertise_areas: userData.expertise_areas,
                            learning_areas: userData.learning_areas,
                        }}
                    >
                        {({ touched, errors, isSubmitting, status, values }) => (
                            <Form>
                                <Paper elevation={2} className={classes.fieldPaper}>
                                    <ToggleDiv title={t("Profile")}>
                                        <div className={classes.formBox}>
                                            <Grid container spacing={2}>
                                                <Grid item xs={12}>
                                                    <MuiTextField
                                                        label={t("Email")}
                                                        fullWidth
                                                        value={userData.user.email}
                                                        disabled
                                                        InputProps={{
                                                            readOnly: true
                                                        }}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} style={{ display: "flex" }}>
                                                    <Avatar src={userData.profile_picture} style={{ marginRight: 10 }} />
                                                    <Input
                                                        id="upload-button"
                                                        type="file"
                                                        onChange={handleFile}
                                                        fullWidth
                                                        inputProps={{
                                                            accept: "image/png, image/jpeg"
                                                        }}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm={6}>
                                                    <Field component={TextField} name="first_name" type="text" label={t("First Name")}
                                                        fullWidth />
                                                </Grid>
                                                <Grid item xs={12} sm={6}>
                                                    <Field component={TextField} name="last_name" type="text" label={t("Last Name")}
                                                        fullWidth />
                                                </Grid>

                                                <Grid item xs={12} sm={6}>
                                                    <FormControl error={touched.gender && !!errors.gender} fullWidth>
                                                        <InputLabel htmlFor="gender-inp">{t("Gender")}</InputLabel>
                                                        <Field component={Select} name="gender" inputProps={{ id: 'gender-inp' }}>
                                                            <MenuItem value="none" className={classes.hiddenOption}><em>Select</em></MenuItem>
                                                            {genders.map((g, index) => <MenuItem key={index} value={g.code}>{g.name}</MenuItem>)}
                                                        </Field>
                                                        {touched.gender && !!errors.gender && <FormHelperText error required>{errors.gender}</FormHelperText>}
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={12} sm={6}>
                                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                        <Field variant="inline" component={DatePicker} disableFuture label={t("Birthdate")} name="birthDate" autoOk
                                                            openTo="year" format="yyyy-MM-dd" views={["year", "month", "date"]} fullWidth />
                                                    </MuiPickersUtilsProvider>
                                                    {touched.birthDate && !!errors.birthDate && <FormHelperText error required>{errors.birthDate}</FormHelperText>}
                                                </Grid>
                                                <Grid item xs={12} sm={6}>
                                                    <FormControl error={touched.country && !!errors.country} fullWidth>
                                                        <Field name="country">
                                                            {({ field: { onChange, ...otherFields }, form }) => (
                                                                <CountrySelect disabled={isSubmitting} {...otherFields} onChange={
                                                                    (e, v) => {
                                                                        form.setFieldValue('country', v);
                                                                        form.setFieldValue('city', '');
                                                                        form.handleChange(e);
                                                                    }
                                                                } />
                                                            )}
                                                        </Field>
                                                        {touched.country && !!errors.country && <FormHelperText error required>{errors.country}</FormHelperText>}
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={12} sm={6}>
                                                    <FormControl error={touched.city && !!errors.city} fullWidth>
                                                        <Field name="city">
                                                            {({ field: { onChange, ...otherFields }, form }) => (
                                                                <CitySelect disabled={isSubmitting} {...otherFields} onChange={
                                                                    (e, v) => {
                                                                        form.setFieldValue('city', v);
                                                                        form.handleChange(e);
                                                                    }
                                                                } countryCode={values['country']} />
                                                            )}
                                                        </Field>
                                                        {touched.city && !!errors.city && <FormHelperText error required>{errors.city}</FormHelperText>}
                                                    </FormControl>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Field component={TextField} name="bio" type="text" label={t("Bio")}
                                                        fullWidth multiline rows={3} placeholder={t("Briefly introduce yourself")} />
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Field name="expertise_areas">
                                                        {({ field, form }) => (
                                                            <SearchAutocomplete
                                                                {...field}
                                                                disabled={isSubmitting}
                                                                multiple
                                                                limitTags={3}
                                                                options={areas}
                                                                loading={loadingAreas === "expertise"}
                                                                getOptionLabel={o => o.text}
                                                                getOptionSelected={((o, v) => o.text === v.text)}
                                                                freeSolo
                                                                onSearch={handleSearchAreasExpertise}
                                                                renderInput={
                                                                    params => <MuiTextField
                                                                        label={t("Expertise Areas")}
                                                                        {...params}
                                                                        InputProps={{
                                                                            ...params.InputProps,
                                                                            endAdornment: (
                                                                                <React.Fragment>
                                                                                    {loadingAreas === "expertise" ? <CircularProgress color="inherit" size={20} /> : null}
                                                                                    {params.InputProps.endAdornment}
                                                                                </React.Fragment>
                                                                            ),
                                                                        }}
                                                                    />
                                                                }
                                                                renderTags={(value, getTagProps) =>
                                                                    value.map((option, index) => (
                                                                        <Chip variant="outlined" label={option.text} {...getTagProps({ index })} />
                                                                    ))
                                                                }

                                                                onChange={
                                                                    (e, value, reason) => {
                                                                        if (reason === "create-option") {
                                                                            value[value.length - 1] = { text: value[value.length - 1] }
                                                                        }
                                                                        form.setFieldValue("expertise_areas", value)
                                                                        form.handleChange(e)
                                                                    }
                                                                }
                                                            />
                                                        )}
                                                    </Field>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Field name="learning_areas">
                                                        {({ field, form }) => (
                                                            <SearchAutocomplete
                                                                {...field}
                                                                disabled={isSubmitting}
                                                                multiple
                                                                limitTags={3}
                                                                options={areas}
                                                                loading={loadingAreas === "learning"}
                                                                getOptionLabel={o => o.text}
                                                                getOptionSelected={((o, v) => o.text === v.text)}
                                                                freeSolo
                                                                onSearch={handleSearchAreasLearning}
                                                                renderInput={
                                                                    params => <MuiTextField
                                                                        label={t("Learning Areas")}
                                                                        {...params}
                                                                        InputProps={{
                                                                            ...params.InputProps,
                                                                            endAdornment: (
                                                                                <React.Fragment>
                                                                                    {loadingAreas === "learning" ? <CircularProgress color="inherit" size={20} /> : null}
                                                                                    {params.InputProps.endAdornment}
                                                                                </React.Fragment>
                                                                            ),
                                                                        }}
                                                                    />
                                                                }
                                                                renderTags={(value, getTagProps) =>
                                                                    value.map((option, index) => (
                                                                        <Chip variant="outlined" label={option.text} {...getTagProps({ index })} />
                                                                    ))
                                                                }

                                                                onChange={
                                                                    (e, value, reason) => {
                                                                        if (reason === "create-option") {
                                                                            value[value.length - 1] = { text: value[value.length - 1] }
                                                                        }
                                                                        form.setFieldValue("learning_areas", value)
                                                                        form.handleChange(e)
                                                                    }
                                                                }
                                                            />
                                                        )}
                                                    </Field>
                                                </Grid>
                                            </Grid>
                                            <Box textAlign="right" m={1} p={1}>
                                                {!!status && <Grid item xs={12}><FormHelperText error required>{status}</FormHelperText></Grid>}
                                                <Button variant="contained" type="submit" color="rose" disabled={isSubmitting}>{t("Save")}</Button>
                                            </Box>
                                        </div>
                                    </ToggleDiv>
                                </Paper>
                            </Form>
                        )}
                    </Formik>
                </Grid>
            </Grid>
            <Grid container justifyContent="center" className={classes.wrapper}>
                <Grid item xs={12} md={8} lg={6}>
                    <Paper elevation={2}>
                        <ToggleDiv title={t("Settings")} defaultOpen>
                            <div className={classes.formBox}>
                                <FormControlLabel
                                    control={<Checkbox checked={checkedDiscoverySetting} onChange={handleDiscoverySetting} disabled={loadingSetting} />}
                                    label={t("I want to participate in the content curation process and provide content for other learners.")}
                                />
                            </div>
                            <div className={classes.formBox}>
                                <FormControlLabel
                                    control={<Checkbox checked={checkedMentoringSetting} onChange={handleMentoringSetting} disabled={loadingSetting} />}
                                    label={t("Show mentoring request for me. (In case your account has been approved as a mentor)")}
                                />
                            </div>
                        </ToggleDiv>
                    </Paper>
                </Grid>
            </Grid>
            <Grid container justifyContent="center" className={classes.wrapper}>
                <Grid item xs={12} md={8} lg={6}>
                    <Paper elevation={2} className={classes.fieldPaper}>
                        <ToggleDiv title={t("Reports")}>
                            {loadingReport ?
                                <CircularProgress />
                                :
                                <div className={classes.formBox} style={{ textAlign: 'center' }}>
                                    <Typography variant="caption">
                                        {t("Here you can customize the reports on your LEARN tab.")}
                                    </Typography>
                                    <Grid container justifyContent="center" spacing={1} className={classes.wrapper}>
                                        <Grid item container className={classes.reportLine} spacing={1}>
                                            <Grid item xs={12} md={6} lg={4} >
                                                <InteractivePaper
                                                    onChange={handleReportSettingsLevelChanged}
                                                    checked={reportLevel}
                                                >
                                                    <ReportItem
                                                        image={`level0.jpg`}
                                                        tooltip={"Newbie -> Freshman -> Junior -> Senior -> Specialist -> Master"}
                                                        title={t("level")}
                                                        alt={t("Level")}
                                                        enabled={reportLevel}
                                                    />
                                                </InteractivePaper>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={4}>
                                                <InteractivePaper
                                                    onChange={handleReportSettingsSpeedChange}
                                                    checked={reportSpeed}
                                                >
                                                    <ReportItem
                                                        icon={SpeedIcon}
                                                        title={t("Speed")}
                                                        alt={t("Speed")}
                                                        tooltip={t("Number of topics you have passed per week")}
                                                        enabled={reportSpeed}
                                                    />
                                                </InteractivePaper>
                                            </Grid>
                                            <Grid item xs={12} md={6} lg={4}>
                                                <InteractivePaper
                                                    onChange={handleReportSettingsInactivityChange}
                                                    checked={reportInactivity}
                                                >
                                                    <ReportItem
                                                        icon={AirplanemodeInactiveIcon}
                                                        tooltip={t("Number of days from your last passed topic")}
                                                        title={t("Inactive since")}
                                                        alt={t("Inactive since")}
                                                        enabled={reportInactivity}
                                                    />
                                                </InteractivePaper>
                                            </Grid>
                                        </Grid>
                                        <Grid item xs={12} className={classes.reportLine}>
                                            <InteractivePaper
                                                onChange={handleReportSettingsNextLevelChange}
                                                checked={reportNextLevel}
                                            >
                                                <LevelReport
                                                    reports={{
                                                        progress_level: 0,
                                                        topics_in_current_level: 10,
                                                        topics_to_next_level: 7,
                                                        passed_topics: 3,
                                                        ongoing_topics: 2,
                                                    }}
                                                    title={t("Your next level")}
                                                    alt={t("Your next level")}
                                                    width={400}
                                                    enabled={reportNextLevel}
                                                />
                                            </InteractivePaper>
                                        </Grid>
                                        <Grid item xs={12} className={classes.reportLine}>
                                            <InteractivePaper
                                                onChange={handleReportSettingsDeadlineChange}
                                                checked={reportDeadline}
                                            >
                                                <GoalReport
                                                    reports={{
                                                        finished_topics: 3,
                                                        all_topics: 10,
                                                        progress_at_deadline_creation: 0,
                                                        estimated_days: 20,
                                                        remaining_days: 10,
                                                        start_date: startDate,
                                                        deadline: deadline
                                                    }}
                                                    width={400}
                                                    enabled={reportDeadline}
                                                />
                                            </InteractivePaper>
                                        </Grid>
                                    </Grid>
                                </div>
                            }

                        </ToggleDiv>
                    </Paper>
                </Grid>
            </Grid>
            <Grid container justifyContent="center" className={classes.wrapper}>
                <Grid item xs={12} md={8} lg={6}>
                    <Formik validationSchema={changePassSchema}
                        onSubmit={(values, { setStatus, setSubmitting, setFieldError, resetForm }) => {
                            const data = { current: values.current, new: values.newPass1 }
                            Axios.put('/members/change-pass/', data)
                                .then((res) => {
                                    resetForm();
                                    setMessage(t('Password has been updated.'))
                                }).catch((error) => {
                                    if (error.response) { //error from server
                                        if (error.response.status === 417) { // data error
                                            setFieldError('current', error.response.data['result'])
                                        } else {
                                            setStatus('HTTP Error ' + error.response.status + ': ' + error.response.data);
                                        }
                                    } else if (error.request) { // no response
                                        setStatus(t('No response from the server.'));
                                    } else { // something else
                                        setStatus(error.message);
                                    }
                                    setSubmitting(false)
                                });
                        }}
                        initialValues={{
                            current: '',
                            newPass1: '',
                            newPass2: '',
                        }}
                    >
                        {({ status, isSubmitting }) => (
                            <Form>
                                <Paper elevation={2} className={classes.fieldPaper}>
                                    <ToggleDiv title={t("Change Password")}>
                                        <div className={classes.formBox}>
                                            <Grid container spacing={2}>
                                                <Grid item xs={12}>
                                                    <Field component={TextField} name="current" type="password" label={t("Current Password")}
                                                        fullWidth />
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Field component={TextField} name="newPass1" type="password" label={t("New Password")}
                                                        fullWidth />
                                                </Grid>
                                                <Grid item xs={12} md={6}>
                                                    <Field component={TextField} name="newPass2" type="password" label={t("Repeat New Password")}
                                                        fullWidth />
                                                </Grid>
                                            </Grid>
                                            <Box textAlign="right" m={1} p={1}>
                                                {!!status && <Grid item xs={12}><FormHelperText error required>{status}</FormHelperText></Grid>}
                                                <Button variant="contained" type="submit" color="rose" disabled={isSubmitting}>{t("Change Password")}</Button>
                                            </Box>
                                        </div>
                                    </ToggleDiv>
                                </Paper>
                            </Form>
                        )}
                    </Formik>
                </Grid>
            </Grid>
            <Grid container justifyContent="center" className={classes.wrapper}>
                <Grid item xs={12} md={8} lg={6}>
                    <Paper elevation={2} className={classes.fieldPaper}>
                        <ToggleDiv title={t("Delete Account")}>
                            <div className={classes.formBox} style={{ textAlign: 'center' }}>
                                <Button color="warning" startIcon={<DeleteForeverIcon />} onClick={handleDelete}>
                                    {t("Delete My Profile")}
                                </Button>
                            </div>
                        </ToggleDiv>
                    </Paper>
                </Grid>
            </Grid>
            <Snackbar color="rose" open={showMessage} message={message}
                closeNotification={() => setMessage(null)} close />
        </>
    )
}

export default withWidth()(
    withStyles(styles, { withTheme: true })(withSnackbar(ProfileSettings))
);
