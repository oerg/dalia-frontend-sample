import { Avatar, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText, makeStyles, Paper, Typography } from '@material-ui/core';
import clsx from 'classnames';
import React, { useMemo } from 'react';
import AnimatedNumber from "animated-number-react";
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(
    theme => ({
        positive: {
            background: "green",
        },
        infos: {
            display: "flex",
        },
        infoBox: {
            width: 55,
            height: 45,
            padding: "0 5px",
            textAlign: "center",
            marginLeft: theme.spacing(1),
            overflow: "hidden",
        },
        listRoot: {
            backgroundImage: ({ betterThan }) => `linear-gradient(90deg, ${betterThan < 50 ? '#fadbd8' : '#d5f5e3'} ${betterThan}%, #FFF ${betterThan}%)`,
            // "&:hover": {
            //     backgroundImage: ({ position }) => `linear-gradient(90deg, ${position < 50 ? '#f5b7b1' : '#abebc6'} ${position}%, #EEE ${position}%)`,
            // }
        },
    })
);

const intl = new Intl.NumberFormat('en-US', { minimumFractionDigits: 0, maximumFractionDigits: 0 });

function StatListItem({ title, link, points, pointStats, betterThan, className, ...props }) {
    const { t } = useTranslation(['dashboard']);
    const classes = useStyles({ betterThan });
    const topText = useMemo(
        () => {
            if (betterThan === 100) {
                return [
                    t('You are the best contributor.'),
                    t('The best')
                ]
            } else if (betterThan >= 75) {
                const top = intl.format(100 - betterThan);
                return [
                    t("You are among top contributors."),
                    `${t("Top")} ${top}${t("%")}`
                ]
            } else {
                const top = intl.format(betterThan);
                return [
                    `${t("Your contribution score is better than")} ${top}${t("%")}`,
                    `${t("Better than")} ${top}${t("%")}`
                ]
            }
        },
        [betterThan, t]
    );

    const formatValue = (value) => value.toFixed(0);

    return (
        <ListItem button component={Link} to={link} {...props}
            className={clsx(
                className,
            )}
        >
            <ListItemAvatar>
                <Avatar
                    title={`${t("Your score:")} ${points}`}
                    className={clsx({
                        [classes.positive]: points > 0
                    })}>
                    <AnimatedNumber
                        value={points}
                        formatValue={formatValue}
                    />
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary={title} />
            <ListItemSecondaryAction>
                <div className={classes.infos}>
                    <Paper className={clsx(classes.infoBox, classes.listRoot)} title={topText[0]}>
                        {
                            <Typography 
                                variant='caption' 
                                title={topText[0]}
                            >
                                {topText[1]}
                            </Typography>
                        }

                    </Paper>
                    <Paper className={classes.infoBox} title={`${t("Median score on eDoer:")} ${pointStats[2]}`}>
                        <Typography variant='caption'>
                            {t("median")}
                        </Typography> <br />
                        {pointStats[2]}
                    </Paper>
                </div>
            </ListItemSecondaryAction>
        </ListItem>
    )
}

export default StatListItem