import axios from "axios";
import { MultiCall, requestAddHandlers } from "../helpers";
import React from 'react';


export function getPublicReports(reportAddress, userId, page, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.get(
            `/members/get-public-profile/${reportAddress}/?user-id=${userId}&page=${page}`
        ),
        onLoad,
        onError,
        onEnd
    )
}

export function deleteRequest(requestId, onSuccess, onError) {
    requestAddHandlers(
        axios.post(`/mentoring/delete-request/`, {request_id: requestId}),
        onSuccess,
        onError
    );
}

export function changeSetting(key, value, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.post('/members/set-setting/', {
            "key": key,
            "value": value
        }),
        onLoad,
        onError,
        onEnd
    )
}

export function getPublicProfile(id, onLoad, onError, onEnd) {
    return requestAddHandlers(
        axios.get(`/members/public-profile-data/?id=${id}`),
        onLoad,
        onError,
        onEnd
    );
}

export function getMentoringRequests(onSuccess, onError) { // get job and suggestions from the server
    requestAddHandlers(
        axios.get(`/mentoring/get-my-requests?ordering=-created_at`),
        onSuccess,
        onError
    );
}

export const renderGeneralCell = (params) => {
    const text = params.row[params.field]
    return (
        <span title={text} style={{textOverflow: "ellipsis", overflow: "hidden"}}>
            {text}
        </span>
    )
}

const MC_KEY = "PRF_AREAS"
export function loadAreas(keyword, onLoad, onError, onEnd) {
    const mc = MultiCall.getInstance(MC_KEY, 500)
    return mc.call(
        `/skills/get-areas/?search=${keyword}&n=15`,
        onLoad,
        onError,
        onEnd
    )
}

export function cancelLoadAreas() {
    return MultiCall.getInstance(MC_KEY).cancel()
}


export function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}