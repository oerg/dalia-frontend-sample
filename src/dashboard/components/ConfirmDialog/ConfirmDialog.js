import { Button, Collapse, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, LinearProgress } from '@material-ui/core'
import React from 'react'
import { useTranslation } from 'react-i18next'

function ConfirmDialog({ title, children, onAccept, onReject, loading, isDangerous = false, ...props }) {
    const { t } = useTranslation(['dashboard'])

    return (
        <Dialog {...props}>
            {title &&
                <DialogTitle>{title}</DialogTitle>
            }
            <DialogContent>
                <DialogContentText>
                    <Collapse in={loading}>
                        <LinearProgress color="primary" />
                    </Collapse>
                    {children}
                </DialogContentText>
                <DialogActions>
                    <Button onClick={onReject} disabled={loading} variant="contained">
                        {t("No")}
                    </Button>
                    <Button onClick={onAccept} color={isDangerous ? "secondary" : "primary"} disabled={loading} variant="contained">
                        {t("Yes")}
                    </Button>
                </DialogActions>
            </DialogContent>
        </Dialog>
    )
}

export default ConfirmDialog