import DateFnsUtils from '@date-io/date-fns';
import { ButtonGroup, Checkbox, CircularProgress, Collapse, Divider, FormControl, FormControlLabel, Grid, IconButton, InputAdornment, InputLabel, LinearProgress, makeStyles, MenuItem, Select, TextField } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import InfoIcon from '@material-ui/icons/Info';
import RemoveIcon from '@material-ui/icons/Remove';
import { Alert } from '@material-ui/lab';
import { KeyboardTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import clsx from 'classnames';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { allFormatTypes, allStrategies } from '../../../data';
import Button from '../CustomButton/Button';
import { getOer, getUrlInfo, processLength, unprocessLength } from './utils';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        textAlign: 'left',
    },
    rootError: {

    },
    divider: {
        margin: `${theme.spacing(1)}px auto`,
    }
}));

function OerInput({ value: propValue, readOnly: propReadOnly, alertOnExist, onChange, className, disabled,
    showRemove, onRemove, onOpen, urlReadOnly = false, showCancel = true, saveLabel: saveLabelProp, persistentData: data,
    enqueueSnackbar, ...props }) {
    const classes = useStyles();

    // this is internal value and does not fire onChange event
    const [value, setRawValue] = useState({});
    const [isLoaded, setIsLoaded] = useState(false);
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(false);
    const myRef = useRef(null);
    const [internalReadOnly, setInternalReadOnly] = useState(false);
    const [showExistsWarning, setShowExistsWarning] = useState(false);
    const { t } = useTranslation(['dashboard']);

    const saveLabel = useMemo(
        () => saveLabelProp ? saveLabelProp : t("Add"),
        [ t ]
    );

    const readOnly = useMemo(
        () => propReadOnly || internalReadOnly,
        [propReadOnly, internalReadOnly]
    );

    const setValue = useCallback(
        (field, value) => {
            setRawValue(
                lastValue => ({ ...lastValue, [field]: value })
            )
        },
        [] // never changes
    );

    const getValue = useCallback(
        (field, defaultValue = '') => value && typeof value[field] !== 'undefined' ? value[field] : defaultValue,
        [value]
    );

    const checkExists = useCallback(
        (exists) => {
            if (exists) {
                setInternalReadOnly(true);
                if (alertOnExist)
                    setShowExistsWarning(true);
                else
                    setShowExistsWarning(false);
            } else {
                setInternalReadOnly(false);
                setShowExistsWarning(false);
            }
        },
        [propValue, t, alertOnExist]
    );

    useEffect(() => {
        if (propValue && propValue.autoload_url) { // we should auto load data
            setRawValue(
                {
                    url: propValue.autoload_url
                }
            );
            // Autoload
            setLoading(true);
            getUrlInfo(
                propValue.autoload_url,
                res => {
                    setIsLoaded(true);
                    setOpen(true);
                    checkExists(res.exists)
                    onChange({ ...res, data });
                },
                () => {
                    enqueueSnackbar(
                        t("This is an invalid URL"),
                        {
                            variant: "error",
                        }
                    );
                    if (onRemove) onRemove();
                },
                () => setLoading(false)
            )
        } else if (propValue && propValue.autoload_id) {
            // Autoload
            setLoading(true);
            getOer(
                propValue.autoload_id,
                res => {
                    setIsLoaded(true);
                    setOpen(true);
                    checkExists(res.exists)
                    onChange({ ...res, data });
                },
                null,
                () => setLoading(false)
            );
        } else if (propValue && propValue.defaultOpen) {
            const { defaultOpen, ...allPropsNoDefaultOpen} = propValue
            setRawValue(
                rawValue => ({
                    ...rawValue,
                    ...allPropsNoDefaultOpen,
                    length: processLength(propValue.length)
                })
            );
            setOpen(true)
        } else {
            setRawValue(
                rawValue => ({
                    ...rawValue,
                    ...propValue,
                    length: processLength(propValue.length)
                })
            );
            setIsLoaded(true);
        }
    }, [propValue, data, checkExists, onChange, t, enqueueSnackbar]);

    const fireOnChange = useCallback(
        value => onChange(
            {
                ...value,
                length: unprocessLength(value.length),
                data
            },
            true // save indication
        ),
        [onChange, data]);

    const handleLoad = useCallback(
        (shouldOpen = true) => {
            if (!isLoaded) {
                setLoading(true);
                getUrlInfo(
                    getValue('url'),
                    res => {
                        setIsLoaded(true);
                        setOpen(true);
                        onChange({ ...res, data });
                    },
                    null,
                    () => setLoading(false)
                )
            } else { // just edit
                setOpen(shouldOpen);
            }

        },
        [getValue, onChange, isLoaded, data]
    );

    const handleTitleChange = useCallback(e => {
        setValue('title', e.target.value);
    }, []);

    const handleDescriptionChange = useCallback(e => {
        setValue('description', e.target.value);
    }, []);

    const handleAddressChange = useCallback(e => {
        setValue('url', e.target.value);
        setIsLoaded(false);
    }, []);

    const handleFormatTypeChange = useCallback(e => {
        setValue('format_type', e.target.value);
    }, []);

    const handleLengthChange = useCallback((date) => {
        setValue('length', date)
    }, []);

    const handleStrategyChange = useCallback(e => {
        setValue('strategy', e.target.value);
    }, []);

    const handleResourceChange = useCallback(e => {
        setValue('resource', e.target.value);
    }, []);

    const handleDetailChange = useCallback(e => {
        setValue('detail', e.target.value);
    }, []);

    const handleIsClassChange = useCallback(
        e => setValue('is_class', e.target.checked),
        []
    );

    const handleAuthorChange = useCallback(
        e => setValue('author', e.target.value),
        []
    );

    const handleEnter = useCallback(
        e => {
            if (!isLoaded && e.keyCode === 13) {
                handleLoad();
            }
        },
        [isLoaded, handleLoad]
    )

    const handleSaveChanges = useCallback(
        () => {
            fireOnChange(value);
            setOpen(false);
        },
        [fireOnChange, value]
    );

    const handleCancelChanges = useCallback(
        () => {
            setOpen(false);
            if (!readOnly)
                onChange({ ...propValue, data }); // cancel edits
        },
        [propValue, data, readOnly]
    );

    // call on open
    useEffect(
        () => onOpen && onOpen(open, myRef.current),
        [open]
    );

    return (
        <div className={clsx(classes.root, className)} {...props}>
            <div className={classes.header}>
                <TextField
                    ref={myRef}
                    value={getValue('url')}
                    onChange={handleAddressChange}
                    fullWidth
                    label="URL"
                    placeholder="https://www.youtube.com/..."
                    type="url"
                    disabled={disabled || loading}
                    variant="outlined"
                    onKeyDown={handleEnter}
                    error={!open && (getValue('title').length === 0 || getValue('resource').length === 0)}
                    InputProps={{
                        readOnly,
                        endAdornment: (
                            <InputAdornment position="end">
                                {loading ?
                                    <CircularProgress />
                                    :
                                    <ButtonGroup size="small">
                                        <IconButton disabled={disabled || loading || open} onClick={handleLoad} title={(readOnly || internalReadOnly) ? t("Show information") : isLoaded ? t("Change information") : t("Load information")}>
                                            {readOnly ? <InfoIcon /> : <EditIcon />}
                                        </IconButton>
                                        {showRemove &&
                                            <IconButton onClick={onRemove} title="Remove">
                                                <RemoveIcon />
                                            </IconButton>
                                        }
                                    </ButtonGroup>
                                }
                            </InputAdornment>
                        )
                    }} />
                {loading && <LinearProgress />}
            </div>
            <Collapse in={open}>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <Divider className={classes.divider} />
                        {showExistsWarning &&
                            <Alert severity="warning" variant="filled">
                                {t("This educational content is already available in our database. The fields are read-only.")}
                            </Alert>}
                    </Grid>
                    <Grid item xs={12}>
                        <TextField value={getValue('title')} onChange={handleTitleChange} label={t("Title")} disabled={disabled || loading} fullWidth variant="outlined"
                            error={getValue('title').length === 0}
                            InputProps={{
                                readOnly,
                            }} />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField value={getValue('description')} onChange={handleDescriptionChange} label={t("Description")} disabled={disabled || loading} fullWidth
                            variant="outlined" multiline rows={3} InputProps={{ readOnly }} />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField value={getValue('author')} onChange={handleAuthorChange} label={t("Author")} disabled={disabled || loading} fullWidth variant="outlined"
                            InputProps={{
                                readOnly,
                            }} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormControl fullWidth variant="outlined" disabled={readOnly}>
                            <InputLabel id="format_type-label">{t("Format Type")}</InputLabel>
                            <Select labelId="format_type-label" label={t("Format Type")} value={getValue('format_type')} onChange={handleFormatTypeChange}>
                                {allFormatTypes.map((ft, i) =>
                                    <MenuItem key={i} value={ft.code}>{ft.label}</MenuItem>
                                )}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormControl fullWidth variant="outlined" disabled={readOnly}>
                            <InputLabel id="strategy-label">{t("Strategy")}</InputLabel>
                            <Select labelId="strategy-label" label={t("Strategy")} value={getValue('strategy')} onChange={handleStrategyChange} readOnly={readOnly}>
                                {allStrategies.map((ft, i) =>
                                    <MenuItem key={i} value={ft.code}>{ft.label}</MenuItem>
                                )}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormControl fullWidth variant="outlined" disabled={readOnly}>
                            <InputLabel id="detail-label">{t("Level of details")}</InputLabel>
                            <Select labelId="detail-label" label={t("Level of details")} value={getValue('detail')} onChange={handleDetailChange} readOnly={readOnly}>
                                <MenuItem value="LO">{t("Low")}</MenuItem>
                                <MenuItem value="ME">{t("Medium")}</MenuItem>
                                <MenuItem value="HI">{t("High")}</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardTimePicker
                                disabled={readOnly}
                                ampm={false}
                                openTo="hours"
                                views={["hours", "minutes", "seconds"]}
                                format="HH:mm:ss"
                                label={t("Length")}
                                value={getValue('length', new Date(1970, 0, 1))}
                                onChange={handleLengthChange}
                                fullWidth
                                inputVariant="outlined"
                            />
                        </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField value={getValue('resource')} onChange={handleResourceChange} label={t("Resource")} disabled={disabled || loading} fullWidth variant="outlined"
                            InputProps={{ readOnly }} error={getValue('resource').length === 0} />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <FormControlLabel control={
                            <Checkbox checked={getValue('is_class', false)} onChange={handleIsClassChange} disabled={readOnly} />
                        } label={t("This resource is a University Class")} />
                    </Grid>
                    <Grid item xs={12} style={{ textAlign: "right" }}>
                        {showCancel &&
                            <Button onClick={handleCancelChanges} color={(!readOnly && !internalReadOnly) ? "danger" : "info"}>
                                {!readOnly ? t("Cancel") : t("Ok")}
                            </Button>}
                        {!readOnly &&
                            <Button onClick={handleSaveChanges} disabled={getValue('title').length === 0 || getValue('resource').length === 0} color="rose">
                                {saveLabel}
                            </Button>}
                    </Grid>
                </Grid>
            </Collapse>
        </div>
    )
}

OerInput.propTypes = {
    value: PropTypes.object.isRequired,
    readOnly: PropTypes.bool,
    className: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
    showRemove: PropTypes.bool,
}

OerInput.defaultProps = {
    readOnly: false,
    disabled: false,
    showRemove: false,
    alertOnExist: true,
}

export default withSnackbar(OerInput)
