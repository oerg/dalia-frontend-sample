import { blue, green, orange, yellow } from '@material-ui/core/colors';
import AssignmentIcon from '@material-ui/icons/Assignment';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import DonutLargeIcon from '@material-ui/icons/DonutLarge';
import NextWeekIcon from '@material-ui/icons/NextWeek';
import React from 'react';

function TopicStatusIcon(props) {
    return props.status === 'FI' ?
            <CheckCircleOutlineIcon style={{ color: green[600] }} />
        : props.status === 'ON' ?
            <DonutLargeIcon style={{ color: yellow[700] }} />
        : props.status === 'EX' ?
            <AssignmentIcon style={{ color: orange[700] }} />
        :
            <NextWeekIcon style={{ color: blue[700] }} />
}

export default TopicStatusIcon;