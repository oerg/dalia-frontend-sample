import React, { useEffect, useMemo, useState } from 'react'
import PropTypes from 'prop-types';
import { Autocomplete } from '@material-ui/lab';
import Axios from 'axios';
import { TextField } from '@material-ui/core';
import { useTranslation } from 'react-i18next';

function CitySelect({ value, required, countryCode, label: propLabel, onChange, showGeneral, ...props}) {
    const [cities, setCities] = useState([]);
    const [isLoading, setLoading] = useState(false);
    const { t } = useTranslation([ 'dashboard' ]);
    const label = useMemo(
        () => propLabel ? propLabel : t('City'),
        [propLabel, t]
    );

    useEffect(() => {
        if (!countryCode || countryCode === '') {
            setCities([]);
            setLoading(false);
        } else {
            setLoading(true);
            Axios.get('/jobs/get-countries-cities/?is-country=false&country=' + countryCode)
            .then(res => {
                if (res.status === 200) {
                    setCities(res.data);
                }
            })
            .finally(() => setLoading(false));
        }
    }, [countryCode])
    
    return (
        <Autocomplete value={value} loading={isLoading} autoHighlight onChange={onChange} 
         options={cities} getOptionSelected={(o, v) => v === "" || o === v }
         renderInput={params => (
            <TextField {...params} required={required} label={label} variant="standard"
                inputProps={{
                    ...params.inputProps,
                    autoComplete: 'new-password', // disable autocomplete and autofill
                }}
                placeholder={showGeneral ? t("General"): ""}
                InputLabelProps={{
                    shrink: showGeneral || params.value,
                }}
            />
        )} {...props} />
    )
}

CitySelect.propTypes = {
    value: PropTypes.string,
    countryCode: PropTypes.string.isRequired,
    label: PropTypes.string,
    onChange: PropTypes.func,
    required:PropTypes.bool,
    showGeneral: PropTypes.bool,
}

CitySelect.defaultProps = {
    onChange: () => {},
    required: false,
    showGeneral: false,
}

export default CitySelect
