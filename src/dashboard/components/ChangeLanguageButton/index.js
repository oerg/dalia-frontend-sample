import { Button, Menu, MenuItem, Portal } from '@material-ui/core';
import LanguageIcon from '@material-ui/icons/Language';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, useRouteMatch } from 'react-router';
import { useLanguage } from '../../../i18n';
import { countryToFlag } from '../CountrySelect';

export default function ChangeLanguageButton({white, ...props}) {
    const [anchorEl, setAnchorEl] = useState(null);
    const language = useLanguage();
    const history = useHistory();
    const match = useRouteMatch('/(\\w{2})?/(.*)');

    const { t, i18n } = useTranslation(['dashboard']);

    const availableLanguages = useMemo(
        () => {
            let languageNames = new Intl.DisplayNames([language], { type: 'language' });
            var langs = [{
                code: "en",
                flag: "GB",
                label: languageNames.of("en")
            }]
            for (const lang in i18n.options.resources) {
                if (lang !== "en") {
                    langs.push(
                        {
                            code: lang,
                            flag: "de",
                            label: languageNames.of(lang)
                        }
                    )
                }
            }
            return langs.sort((a, b) => a.code < b.code)
        },
        [language, i18n]
    );

    const handleClose = useCallback(
        () => setAnchorEl(null),
        []
    );

    const handleOpen = useCallback(
        e => {
            e.stopPropagation();
            setAnchorEl(e.currentTarget);
        },
        []
    );

    const handleChangeLanguage = useCallback(
        lang => () => {
            setAnchorEl(null);
            history.push(`/${lang}/${match.params[1]}`);
        },
        [history, match]
    )


    return (<>
        <Button color="primary" variant="outlined" {...props} title={t("Change Language")} onClick={handleOpen}>
            <LanguageIcon style={white && { color: '#FFF' }} />
        </Button>
        <Portal>
            <Menu
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={handleClose}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                {
                    availableLanguages.map(
                        (lang, index) =>
                            <MenuItem key={index} onClick={handleChangeLanguage(lang.code)} disabled={language === lang.code}>
                                <span style={{marginRight: 8}}>{countryToFlag(lang.flag)}</span>
                                {lang.label}
                            </MenuItem>
                    )
                }
            </Menu>
        </Portal>
    </>)
}
