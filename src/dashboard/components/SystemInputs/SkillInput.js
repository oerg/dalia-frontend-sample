import { TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { cancelLoadSkills, loadSkills, loadSystemSkills } from './utils';

function SkillInput({ skills, label, onSelect, clearOnSelect, loading, inputProps, newValue, ...props }) {
    const [skills1, setSkills1] = useState([]); // esco skills
    const [skills2, setSkills2] = useState([]); // system skills
    const [loadingSkills, setLoadingSkills] = useState(false);
    const [skillInput, setSkillInput] = useState('');
    const [acOpen, setAcOpen] = useState(false);
    const [value, setValue] = useState('');

    const selectableSkills = useMemo(
        () => (skills1.length > 0 || skills2.length > 0) ?
            [...skills1, ...skills2.map(s => s.title), skillInput]
            :
            [...skills1, ...skills2.map(s => s.title)],
        [skills1, skills2, skillInput]
    );

    useEffect(
        () => {
            if (newValue)
                setValue(newValue)
        },
        [newValue]
    )

    // load skills
    useEffect(
        () => {
            if (skillInput.length > 0) {
                setSkills1([]);
                setSkills2([]);
                setLoadingSkills(true);
                if (skills === 'external' || skills === 'both')
                    loadSkills(
                        skillInput,
                        (res) => {
                            setSkills1(res.data);
                        },
                        null,
                        () => setLoadingSkills(false)
                    )

                if (skills === 'system' || skills === 'both')
                    loadSystemSkills(
                        skillInput,
                        res => {
                            setSkills2(res.data.results);
                        },
                        null,
                        () => setLoadingSkills(false)
                    )
            } else {
                cancelLoadSkills();
                setSkills1([]);
                setSkills2([]);
                setLoadingSkills(false);
            }
            return () => cancelLoadSkills()
        },
        [skills, skillInput]
    )

    const handleEnter = useCallback(
        e => {
            if (!acOpen && e.keyCode === 13 && skillInput.length > 0) { // enter is pressed
                onSelect(skillInput);
            }
        },
        [acOpen, onSelect, skillInput]
    );

    const handleSkillInput = useCallback(
        (e, value) => {
            setSkillInput(value);
        },
        []
    );

    const handleChange = useCallback(
        (e, value) => {
            if (!clearOnSelect)
                setValue(value);
            else {
                setValue('');
            }
            onSelect(value);
        },
        [onSelect, clearOnSelect]
    );

    const handleBlur = useCallback(
        () => {
            setValue(skillInput)
            onSelect(skillInput)
        },
        [onSelect, skillInput]
    )

    const handleOpen = useCallback(() => setAcOpen(true), []);

    const handleClose = useCallback(() => setAcOpen(false), []);

    return (
        <Autocomplete
            options={selectableSkills}
            renderInput={
                (params) =>
                    <TextField
                        label={label}
                        onKeyDown={handleEnter}
                        margin="normal"
                        autoFocus
                        fullWidth
                        {...inputProps}
                        {...params}
                    />
            }
            onBlur={handleBlur}
            loading={loadingSkills}
            freeSolo
            selectOnFocus
            autoSelect
            handleHomeEndKeys
            disableClearable
            inputValue={skillInput}
            onInputChange={handleSkillInput}
            disabled={loading}
            open={acOpen}
            onOpen={handleOpen}
            onClose={handleClose}
            value={value}
            onChange={handleChange}
            {...props}
        />
    )
}

SkillInput.propTypes = {
    skills: PropTypes.oneOf(['system', 'external', 'both']),
    label: PropTypes.string,
    onSelect: PropTypes.func,
    loading: PropTypes.bool,
    clearOnSelect: PropTypes.bool,
    inputProps: PropTypes.object,
}

SkillInput.defaultProps = {
    skills: 'both',
    label: 'Search skills',
    onSelect: () => { },
    loading: false,
    clearOnSelect: true,
}

export default SkillInput

