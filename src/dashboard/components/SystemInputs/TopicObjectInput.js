import { TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import PropTypes from 'prop-types';
import React, { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { getTopicLabel, loadTopicObjs, openFilter } from './utils';

function TopicObjectInput({ onSelect }) {
    const [options, setOptions] = useState([]);
    const [selectedTopic, setSelectedTopic] = useState({ title: '' });
    const [topicText, setTopicText] = useState('');
    const [loading, setLoading] = useState(false);
    const { t } = useTranslation(['dashboard']);

    const updateOptions = useCallback(
        (search) => {
            setLoading(true)
            setOptions([])
            loadTopicObjs(
                search,
                res => {
                    setOptions(res.data.results);
                },
                null,
                () => setLoading(false)
            )
        },
        []
    );

    const handleSelect = useCallback(
        (e, newValue) => {
            onSelect && onSelect(newValue);
            setSelectedTopic(newValue);
        },
        [onSelect]
    );

    const handleTopicText = useCallback(
        (e, newValue) => {
            setTopicText(newValue);
            updateOptions(newValue);
        },
        [updateOptions]
    );

    return (
        <Autocomplete
            options={options}
            getOptionLabel={getTopicLabel}
            value={selectedTopic}
            onChange={handleSelect}
            inputValue={topicText}
            onInputChange={handleTopicText}
            loading={loading}
            autoComplete
            filterOptions={openFilter}
            noOptionsText={t("No topics.")}
            renderInput={(params) => <TextField {...params} label={t("Topic")} variant="outlined" autoFocus />}
        />
    )
}

TopicObjectInput.propTypes = {
    onSelect: PropTypes.func,
}

export default TopicObjectInput
