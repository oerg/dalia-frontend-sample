import { TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { cancelLoadSkills, cancelLoadTopics, loadTopicObjs } from './utils';

function TopicInput({ label: propLabel, onSelect, clearOnSelect, loading, inputProps, clearValue, newValue, ...props }) {
    const [selectableTopics, setSelectableTopics] = useState([]);
    const [loadingTopics, setLoadingTopics] = useState(false);
    const [topicInput, setTopicInput] = useState('');
    const [acOpen, setAcOpen] = useState(false);
    const [value, setValue] = useState('');
    const { t } = useTranslation([ 'dashboard' ]);

    const label = useMemo(
        () => propLabel ? propLabel : t("Search"),
        [t, propLabel]
    );

    useEffect(
        () => {
            if (clearValue)
                setValue('');
        },
        [ clearValue ]
    )

    useEffect(
        () => {
            if (newValue)
                setValue(newValue)
        },
        [ newValue ]
    )

    // load topics
    useEffect(
        () => {
            if (topicInput.length > 0) {
                setLoadingTopics(true);
                setSelectableTopics([])
                loadTopicObjs(
                    topicInput,
                    (res) => {
                        setSelectableTopics([...res.data.results.map(t => t['title']), topicInput]);
                    },
                    null,
                    () => setLoadingTopics(false)
                )
            } else {
                cancelLoadTopics();
                setSelectableTopics([]);
                setLoadingTopics(false);
            }
            return () => cancelLoadSkills()
        },
        [ topicInput ]
    );

    const handleEnter = useCallback(
        e => {
            if (!acOpen && e.keyCode === 13 && topicInput.length > 0) { // enter is pressed
                onSelect(topicInput);
            }
        }, 
        [ acOpen, onSelect, topicInput ]
    );

    const handleTopicInput = useCallback(
        (e, value) => {
            setTopicInput(value);
            if (value === "")
                onSelect("")
        }, 
        [onSelect]
    );

    const handleChange = useCallback(
        (e, value) => {
            if (!clearOnSelect) 
                setValue(value);
            else 
                setValue('');
            onSelect(value);
        },
        [ onSelect, clearOnSelect ]
    )

    const handleBlur = useCallback(
        () => {
            setValue(topicInput)
            onSelect(topicInput)
        },
        [ onSelect, topicInput ]
    )

    const handleOpen = useCallback(() => setAcOpen(true), []);

    const handleClose = useCallback(() => setAcOpen(false), []);

    return (
        <Autocomplete 
            options={selectableTopics} 
            renderInput={
                (params) => 
                    <TextField 
                        label={label}
                        onKeyDown={handleEnter} 
                        margin="normal" 
                        autoFocus
                        fullWidth
                        {...inputProps}
                        {...params} 
                    />
            }
            onBlur={handleBlur}
            loading={loadingTopics} 
            freeSolo  
            handleHomeEndKeys
            disableClearable
            inputValue={topicInput} 
            onInputChange={handleTopicInput} 
            disabled={loading} 
            open={acOpen}
            onOpen={handleOpen}
            onClose={handleClose}
            value={value}
            onChange={handleChange}
            {...props}
        />
    )
}

TopicInput.propTypes = {
    label: PropTypes.string,
    onSelect: PropTypes.func,
    loading: PropTypes.bool,
    clearOnSelect: PropTypes.bool,
    inputProps: PropTypes.object,
}

TopicInput.defaultProps = {
    onSelect: () => {},
    loading: false,
    clearOnSelect: false,
}

export default TopicInput

