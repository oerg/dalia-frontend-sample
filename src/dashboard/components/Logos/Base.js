import { Avatar, makeStyles } from '@material-ui/core';
import clsx from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

const useStyles = makeStyles(theme => ({
    root: {

    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    normal: {
        width: theme.spacing(5),
        height: theme.spacing(5)
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7)
    }
}))

function BaseLogo({ content, size, className, ...props }) {
    const classes = useStyles();
    return (
        <Avatar title={content} variant="rounded" className={clsx(classes.root, className, {
            [classes.small]: size === "small",
            [classes.normal]: size === "normal",
            [classes.large]: size === "large",
        })} {...props}>
            {content}
        </Avatar>
    )
}

BaseLogo.propTypes = {
    size: PropTypes.oneOf(['small', 'normal', 'large']),
    content: PropTypes.string.isRequired,
    className: PropTypes.string,
}

BaseLogo.defaultProps = {
    size: "normal"
}

export default BaseLogo
