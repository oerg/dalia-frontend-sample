import {makeStyles} from '@material-ui/core';
import clsx from 'classnames';
import PropTypes from 'prop-types';
import React, {useMemo} from 'react';
import {useTranslation} from 'react-i18next';
import BaseLogo from './Base';

const useStyles = makeStyles({
    skillLogo: {
        // backgroundColor: green[200],
        // fontSize: 19,
    }
})

function SkillLogo({size, className, title: propTitle, picture, ...props}) {
    const classes = useStyles();
    const {t} = useTranslation(['dashboard']);
    const title = useMemo(
        () => propTitle ? propTitle : t('Course'),
        [t]
    )

    return (
        <BaseLogo size={size} content="CRS" title={title} className={clsx(classes.skillLogo, className)}
                  src={picture} {...props}
        />
    )
}

SkillLogo.propTypes = {
    size: PropTypes.oneOf(['small', 'normal', 'large'])
}

SkillLogo.defaultProps = {
    size: "normal"
}

export default SkillLogo
