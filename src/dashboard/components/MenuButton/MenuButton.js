import { Badge, Button, ClickAwayListener, Collapse, Divider, ListItemIcon, ListItemText, MenuItem, MenuList, Paper, Popper } from '@material-ui/core'
import React, { useCallback, useMemo, useRef, useState } from 'react'

function MenuButton({ component: propComponent, PopperProps, menus, onClick, MenuListProps, ...props }) {
    const ref = useRef(null)
    const Component = useMemo(
        () => propComponent ? propComponent : Button,
        [propComponent]
    )

    const [open, setOpen] = useState(false)

    const hanldeMenuClose = useCallback(
        () => setOpen(false),
        []
    )

    const handleMenuToggle = useCallback(
        (e) => {
            setOpen(o => !o)
            onClick && onClick(e)
        },
        [onClick]
    )

    return (<>
        <Component onClick={handleMenuToggle} {...props} ref={ref} />
        <Popper
            anchorEl={ref.current}
            open={open}
            role={undefined}
            placement="bottom-end"
            {...PopperProps}
            style={{
                zIndex: 99,
            }}
            transition
        >
            {({ TransitionProps }) => (
                <Collapse {...TransitionProps}>
                    <Paper>
                        <ClickAwayListener onClickAway={hanldeMenuClose}>
                            <MenuList style={{ padding: 0 }} {...MenuListProps}>
                                {menus.map(
                                    ({ label, icon, TextProps, showBadge, ...props }, index) => (
                                        label === '-' ?
                                            <Divider />
                                            :
                                            <MenuItem key={index} {...props}>
                                                {icon &&
                                                    <ListItemIcon
                                                        style={{
                                                            minWidth: 30
                                                        }}
                                                    >
                                                        <Badge
                                                            variant="dot"
                                                            invisible={!showBadge}
                                                            color="secondary"
                                                            overlap="circular"
                                                        >
                                                            {icon}
                                                        </Badge>
                                                    </ListItemIcon>
                                                }
                                                <ListItemText primary={label} inset={!Boolean(icon)} />
                                            </MenuItem>
                                    )
                                )}
                            </MenuList>
                        </ClickAwayListener>
                    </Paper>
                </Collapse>
            )}
        </Popper>
    </>)
}

export default MenuButton