import { Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import React from 'react';
import { useTranslation } from 'react-i18next';



function OerIconButton({ type, resource, ...props }) {
    const { t } = useTranslation(['dashboard']);
    // const typeIcon = useMemo(() => (
    //     type === 'WE'? <WebIcon />
    //     : type === 'SL' ? <SlideshowIcon />
    //     : type === 'BC' ? <ImportContactsIcon />
    //     : type === 'LN' ? <ImportContactsIcon />
    //     : <VideoLibraryIcon />
    // ), [type]);

    // const resourceIcon = useMemo(() => (
    //     resource === 'Wikibooks' ?
    //         Wikibooks
    //         : resource === 'Bookdown' ?
    //             Bookdown
    //             : resource === 'GeeksForGeeks' ?
    //                 GeeksForGeeks
    //                 : resource === 'Open textbook library' ?
    //                     OpenTextbookLibrary
    //                     : resource === 'Scikit-learn' ?
    //                         ScikitLearn
    //                         : resource === 'TowardsDataScience' ?
    //                             TowardsDataScience
    //                             : resource === 'Tutorialspoint' ?
    //                                 Tutorialspoint
    //                                 : resource === 'W3Schools' ?
    //                                     W3Schools
    //                                     : resource === 'Wikipedia' ?
    //                                         Wikipedia
    //                                         : resource === 'Youtube' ?
    //                                             Youtube
    //                                             :
    //                                             DefaultIcon
    // ), [resource]);

    return (
        <Button title={t("View Content")} {...props}>
            {t("View Content")}
        </Button>
    )
}

OerIconButton.propTypes = {
    type: PropTypes.oneOf(['VI', 'BC', 'WE', 'SL', 'LN', 'MI']),
    resource: PropTypes.string,
}

export default OerIconButton
