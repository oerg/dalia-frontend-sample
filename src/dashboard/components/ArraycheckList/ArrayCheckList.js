import { Checkbox, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import PropTypes from 'prop-types'
import React, { useCallback, useEffect, useState } from 'react'

function ArrayCheckList({ list, getLabel, onChange, updateObjects, defaultSelected, disabled, ...props }) {
    const [checked, setChecked] = useState([])

    useEffect(() => {
        setChecked(list.map(o => 'selected' in o ? o.selected : defaultSelected))
    }, [list, defaultSelected])

    const handleClick = useCallback((value, index) => () => {
        setChecked((prevChecked => {
            prevChecked[index] = !prevChecked[index];
            if (updateObjects) list[index].selected = prevChecked[index]
            onChange(value, prevChecked[index], list)
            return [...prevChecked]
        }))
    }, [onChange, setChecked, updateObjects, list]);

    return (
        <List>
            {list.map((lo, ind) => (
                <ListItem key={ind} button dense onClick={handleClick(lo, ind)} role={undefined}>
                    <ListItemIcon>
                        <Checkbox checked={checked[ind] === true} tabIndex={-1} edge="start" disableRipple
                            disabled={disabled} />
                    </ListItemIcon>
                    <ListItemText primary={getLabel(lo)} />
                </ListItem>
            ))
            }
        </List>
    )
}

ArrayCheckList.propTypes = {
    list: PropTypes.array.isRequired,
    updateObjects: PropTypes.bool,
    getLabel: PropTypes.func,
    onChange: PropTypes.func,
    defaultSelected: PropTypes.bool,
    disabled: PropTypes.bool,
}

ArrayCheckList.defaultProps = {
    getLabel: (obj) => obj.title,
    onChange: (obj, selected) => { },
    updateObjects: true,
    defaultSelected: false,
    disabled: false,
}

export default ArrayCheckList
