import { Button, Checkbox, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, List, ListItem, ListItemAvatar, ListItemText } from '@material-ui/core';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

function TopicsDialog({ topics, title: propTitle, titleField = "title", idField = "id", onClose, ...props }) {
    const { t } = useTranslation(['dashboard']);
    const [selectedTopics, setSelectedTopics] = useState([]);

    const title = useMemo(
        () => propTitle ? propTitle : t('Select topics'),
        [propTitle]
    );

    const isSelected = useCallback(
        (topic) => selectedTopics.some(t => t[idField] === topic[idField]),
        [selectedTopics, idField]
    );

    const handleToggle = useCallback(
        (topic) => () => {
            if (isSelected(topic)) {
                setSelectedTopics(
                    topics => topics.filter(t => t[idField] !== topic[idField])
                );
            } else {
                setSelectedTopics(
                    topics => [...topics, topic]
                );
            }
        },
        [isSelected, idField]
    );

    const handleCancel = useCallback(
        (e) => {
            onClose(e, "cancel");
        },
        [onClose]
    );

    const handleSave = useCallback(
        (e) => {
            onClose(e, "add", selectedTopics);
        },
        [onClose, selectedTopics]
    )


    return <Dialog onClose={onClose} {...props}>
        <DialogTitle>{title}</DialogTitle>
        {!topics || topics.length === 0 ?
            <DialogContent>
                <DialogContentText>
                    <i>{t("No topics.")}</i>
                </DialogContentText>
            </DialogContent>
            :
            <List>
                {topics.map(
                    (topic, index) => (
                        <ListItem role={undefined} key={index} dense button onClick={handleToggle(topic)}>
                            <ListItemAvatar>
                                <Checkbox
                                    edge="start"
                                    tabIndex={-1}
                                    disableRipple
                                    checked={isSelected(topic)}
                                />
                            </ListItemAvatar>
                            <ListItemText
                                primary={topic[titleField]}
                            />
                        </ListItem>
                    )
                )}
            </List>
        }
        <DialogActions>
            <Button onClick={handleCancel}>
                {t("Cancel")}
            </Button>
            <Button onClick={handleSave}>
                {t("Add Question")}
            </Button>
        </DialogActions>
    </Dialog>;
}

export default TopicsDialog;
