import { Chip } from '@material-ui/core';
import React, { useMemo } from 'react';
import { useLanguage } from '../../../i18n';

function LangIcon({ language, small, ...props}) {
    const systemLanguage = useLanguage();
    const languageName = useMemo(
        () => {
            if (language) {
                const formatter = new Intl.DisplayNames([systemLanguage], {type: 'language'});
                return formatter.of(language);
            }
            return null;
        },
        [language, systemLanguage]
    );

    return language ?
    <Chip
        variant='default'
        color='default'
        size='small'
        {...props}
        label={small ? language ? language.toUpperCase() : "" : languageName}
        title={languageName}
    />
    :
    null;
}

export default LangIcon;
