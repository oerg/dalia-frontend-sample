import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

function TitleConfirmDialog(rawProps) {
    const {
        title: propTitle,
        label: propLabel,
        open,
        onSelect,
        onCancel,
        okButtonLabel: propOkButtonLabel,
        isCourse,
        ...props
    } = rawProps;
    const { t } = useTranslation(['dashboard']);
    const [title, setTitle] = useState('');
    
    const okButtonLabel = useMemo(
        () => propOkButtonLabel ? propOkButtonLabel : t("Add"),
        [propOkButtonLabel, t]
    )

    const label = useMemo(
        () => propLabel ? propLabel : t("Please enter the title: "),
        [t, propLabel]
    );

    useEffect(
        () => {
            setTitle(propTitle)
        },
        [propTitle]
    );

    const handleChange = useCallback(
        e => setTitle(e.target.value),
        []
    );

    const handleClose = useCallback(
        (e, reason) => {
            if (reason)
                return false;
            onCancel();
        },
        [onCancel]
    );

    const handleOk = useCallback(
        () => {
            onSelect(title);
        },
        [onSelect, title]
    );

    const handleEnter = useCallback(
        e => {
            if (e.keyCode === 13) {
                handleOk();
            }
        },
        [handleOk]
    );
    
    return (
        <Dialog {...props} open={open} onClose={handleClose} fullWidth maxWidth="sm">
            <DialogTitle>{label}</DialogTitle>
            <DialogContent dividers>
                <TextField
                    value={title}
                    onChange={handleChange}
                    variant="outlined"
                    autoFocus
                    label={isCourse ? t("Course Title") : t("Topic Title")}
                    fullWidth
                    onKeyDown={handleEnter}
                />
            </DialogContent>
            <DialogActions>
                <Button color="secondary" variant="outlined" onClick={onCancel}>{t("Cancel")}</Button>
                <Button color="primary" variant="contained" onClick={handleOk}>{okButtonLabel}</Button>
            </DialogActions>
        </Dialog>
    )
}

TitleConfirmDialog.propTypes = {
    title: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
    onSelect: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    label: PropTypes.string,
}

export default TitleConfirmDialog
