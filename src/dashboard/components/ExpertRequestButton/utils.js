import axios from "axios";
import { requestAddHandlers } from "../../views/helpers";

export function sendExpertRequest(object_type, object_id, onLoad, onError, onEnd) {
    requestAddHandlers(
        axios.post(
            '/admin-services/add-expert-request/',
            {
                object_type,
                object_id
            }
        ),
        onLoad,
        onError,
        onEnd
    );
}