import IconButton from "@material-ui/core/IconButton";
import Snack from "@material-ui/core/Snackbar";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import Close from "@material-ui/icons/Close";
import classNames from "classnames";
import PropTypes from "prop-types";
import React from "react";
// core components
import styles from "./styles";

const useStyles = makeStyles(styles);

export default function Snackbar(props) {
    const classes = useStyles();
    const { message, color, close, icon, place, open, rtlActive, autoHideDuration } = props;
    var action = [];
    const messageClasses = classNames({
        [classes.iconMessage]: icon !== undefined
    });
    if (close !== undefined) {
        action = [
            <IconButton
                className={classes.iconButton}
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={() => props.closeNotification()}
            >
                <Close className={classes.close} />
            </IconButton>
        ];
    }
    return (
        <Snack
            anchorOrigin={{
                vertical: place.indexOf("t") === -1 ? "bottom" : "top",
                horizontal:
                    place.indexOf("l") !== -1
                        ? "left"
                        : place.indexOf("c") !== -1
                            ? "center"
                            : "right"
            }}
            open={open}
            message={
                <div>
                    {icon !== undefined ? <props.icon className={classes.icon} /> : null}
                    <span className={messageClasses}>{message}</span>
                </div>
            }
            autoHideDuration={autoHideDuration}
            onClose={props.closeNotification}
            action={action}
            ContentProps={{
                classes: {
                    root: classes.root + " " + classes[color],
                    message: classes.message,
                    action: classNames({ [classes.actionRTL]: rtlActive })
                }
            }}
        />
    );
}

Snackbar.propTypes = {
    message: PropTypes.node.isRequired,
    color: PropTypes.oneOf(["info", "success", "warning", "danger", "primary", "rose"]),
    close: PropTypes.bool,
    icon: PropTypes.object,
    place: PropTypes.oneOf(["tl", "tr", "tc", "br", "bl", "bc"]),
    open: PropTypes.bool,
    rtlActive: PropTypes.bool,
    closeNotification: PropTypes.func,
    autoHideDuration: PropTypes.number,
};

Snackbar.defaultProps = {
    color: "primary",
    place: "bc",
    rtlActive: false,
    autoHideDuration: 6000
}