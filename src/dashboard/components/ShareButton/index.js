import { IconButton, makeStyles, Paper, Popover } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { useCallback, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { EmailIcon, EmailShareButton, FacebookIcon, FacebookShareButton, TwitterIcon, TwitterShareButton, 
    TelegramShareButton, WhatsappShareButton, TelegramIcon, WhatsappIcon } from "react-share";

const useStyles = makeStyles( theme => ({
    popperRoot: {
        padding: theme.spacing(1),
        paddingBottom: 0,
    }
}))

function ShareButton(props) {
    const {
        component: Component = IconButton, 
        children,
        url,
        title,
        componentTitle,
        ...otherProps
    } = props;
    const { t } = useTranslation(['dashboard']);

    const finalTitle = useMemo(
        () =>  `${t("Learn on eDoer.eu:")} ${title}`, 
    [title, t])

    const body = useMemo(
        () =>
`${t("eDoer.eu helps you achieve your learning journeys through personalized education.")}
${finalTitle}
`,
        [title, t, finalTitle])

    const classes = useStyles();
    const mainRef = useRef(null);
    const [showButtons, setShowButtons] = useState(false);

    const handleOpenMenu = useCallback(
        () => setShowButtons(true),
        []
    );

    const handleCloseMenu = useCallback(
        () => setShowButtons(false),
        []
    );

    return (<>
        <Component {...otherProps} onClick={handleOpenMenu} title={componentTitle} ref={mainRef}>
            {children}
        </Component>
        <Popover
            anchorEl={mainRef.current}
            open={showButtons}
            onClose={handleCloseMenu}
        >
            <Paper className={classes.popperRoot}>
                <FacebookShareButton
                    url={url}
                    quote={body}
                >
                    <FacebookIcon size={32} />
                </FacebookShareButton>
                <TwitterShareButton
                    url={url}
                    title={body}
                >
                    <TwitterIcon size={32} />
                </TwitterShareButton>
                <WhatsappShareButton
                    url={url}
                    title={body}
                    separator=":: "
                >
                    <WhatsappIcon size={32} />
                </WhatsappShareButton>
                <TelegramShareButton
                    url={url}
                    title={body}
                >
                    <TelegramIcon size={32} />
                </TelegramShareButton>
                <EmailShareButton
                    url={url}
                    subject={finalTitle}
                    body={body}
                >
                    <EmailIcon size={32} />
                </EmailShareButton>
            </Paper>
        </Popover>
        </>
    )
}

ShareButton.propTypes = {
    component: PropTypes.elementType,
    url: PropTypes.string.isRequired,
    componentTitle: PropTypes.string,
}


export default ShareButton
