import { green, red, yellow } from '@material-ui/core/colors';
import SentimentDissatisfiedIcon from '@material-ui/icons/SentimentDissatisfied';
import SentimentSatisfiedIcon from '@material-ui/icons/SentimentSatisfied';
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAlt';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import PropTypes from 'prop-types';
import React from 'react';



function Face(props) {
    const score = props.score > 5 ? 5 : props.score < 0 ? 0 : Math.floor(props.score);

    return (
        score === 1 || score === 0 ?
            <SentimentVeryDissatisfiedIcon style={{ color: red[900] }} />
            : score === 2 ?
                <SentimentDissatisfiedIcon style={{ color: red[400] }} />
                : score === 3 ?
                    <SentimentSatisfiedIcon style={{ color: yellow[700] }} />
                    : score === 4 ?
                        <SentimentSatisfiedAltIcon style={{ color: green[400] }} />
                        : <SentimentVerySatisfiedIcon style={{ color: green[900] }} />
    )
}

Face.propTypes = {
    score: PropTypes.number.isRequired,
}

export default Face
