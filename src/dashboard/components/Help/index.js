import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import PropTypes from 'prop-types';
import React from 'react';
import HtmlTooltip from '../HtmlTooltip';

function Help(rawProps) {
    const {
        icon: IconComponent = HelpOutlineIcon,
        tooltipProps,
        children,
        ...props
    } = rawProps;

    return (
        <HtmlTooltip
            arrow
            placement="bottom"
            interactive
            {...tooltipProps}
            title={children}
        >
            <IconComponent {...props} />
        </HtmlTooltip>
    )
}

Help.propTypes = {
    icon: PropTypes.elementType,
    tooltipProps: PropTypes.object,
}

export default Help
