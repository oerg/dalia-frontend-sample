import { Button, makeStyles, Paper, Typography } from '@material-ui/core'
import SpaceBarIcon from '@material-ui/icons/SpaceBar'
import clsx from "classnames"
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { preAddress } from '../../../App'
import { useLanguage } from '../../../i18n'
import SkillLogo from '../Logos/Skill'

const useStyles = makeStyles({
    root: {
        cursor: "pointer",
        position: "relative",
        display: "flex",
        margin: "8px 8px 16px 8px",
        padding: 10
    },
    deleteIcon: {
        position: 'absolute',
        top: 4,
        right: 4
    },
    selected: {
        backgroundColor: "#CCC",
    },
    followButton: {
        position: "absolute",
        right: 10,
    }
})

function SkillSearchCard({ skill, noActions, onRemove, className, selected: propSelected, onKeyDown, onAddRequest, onUnarchiveRequest, ...props }) {
    const { t } = useTranslation(['dashboard'])

    const rootRef = useRef(null)
    const systemLanguage = useLanguage()
    const classes = useStyles()
    const [isFocused, setIsFocused] = useState(false)

    useEffect(
        () => {
            if (propSelected && rootRef.current)
                rootRef.current.focus()
        },
        [propSelected]
    )

    const handleAdd = useCallback(
        (e) => {
            if (e)
                e.preventDefault()
            if (onAddRequest)
                onAddRequest(skill)
        },
        [onAddRequest, skill]
    )

    const handleUnarchive = useCallback(
        (e) => {
            if (e)
                e.preventDefault()
            if (onUnarchiveRequest)
                onUnarchiveRequest(skill)
        },
        [onAddRequest, skill]
    )

    const handleKeyDown = useCallback(
        e => {
            if (e.key === " " && !skill.is_my_skill) {
                handleAdd()
            }
            if (onKeyDown) {
                onKeyDown(e)
            }
        },
        [handleAdd, onKeyDown]
    )

    return (
        <Paper
            {...props}
            className={clsx(classes.root, className, { [classes.selected]: isFocused })}
            component={Button}
            href={`${preAddress}/${systemLanguage}/dashboard/skills/${skill.id}`}
            target="_blank"
            ref={rootRef}
            onFocus={() => setIsFocused(true)}
            onBlur={() => setIsFocused(false)}
            onKeyDown={handleKeyDown}
        >
            {!noActions &&
                <div className={classes.followButton}>
                    {skill.is_my_skill ?
                        skill.is_archived ?
                            <Button
                                variant='outlined'
                                onClick={handleUnarchive}
                                color={isFocused ? "primary" : "default"}
                                startIcon={isFocused ? <SpaceBarIcon /> : null}
                            >
                                {t("Unarchive")}
                            </Button>
                            :
                            <Button
                                variant='outlined'
                                disabled
                            >
                                {t("Added")}
                            </Button>

                        :
                        <Button
                            variant='outlined'
                            onClick={handleAdd}
                            color={isFocused ? "primary" : "default"}
                            startIcon={isFocused ? <SpaceBarIcon /> : null}
                        >
                            {t("Add")}
                        </Button>
                    }
                </div>
            }
            <SkillLogo size="large" />
            <div style={{
                flexGrow: 1,
                marginLeft: 8
            }}
            >
                <Typography
                    title={skill['title']}
                    component={"div"}
                    style={{
                        width: 400,
                        textOverflow: "ellipsis",
                        overflow: "hidden",
                        whiteSpace: "nowrap"
                    }}
                >
                    {skill['title']}
                </Typography>
            </div>
        </Paper>
    )
}

export default SkillSearchCard