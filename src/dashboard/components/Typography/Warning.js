import React from "react";
import PropTypes from "prop-types";
import clsx from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import styles from "./styles";

const useStyles = makeStyles(styles);

export default function Warning(props) {
  const classes = useStyles();
  const { children, className } = props;
  return (
    <div className={clsx(classes.defaultFontStyle, classes.warningText, className)}>
      {children}
    </div>
  );
}

Warning.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

Warning.defaultProps = {
  className: '',
}
