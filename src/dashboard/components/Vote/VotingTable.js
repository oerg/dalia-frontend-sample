import { IconButton, makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, withStyles } from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import clsx from 'classnames';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Vote from '.';


const useStyles = makeStyles({
    vote: {
        minWidth: '60px',
        float: 'right'
    },
    clickable: {
        cursor: 'pointer',
    },
    root: ({ height }) => ({
        maxHeight: height,
    }),
    hidden: {
        display: 'none',
    },
    comment: {
        display: 'inline-block',
        float: 'right'
    },
    headRow: {
        height: 60,
    }
})

const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);


function VotingTable({ data, onVote, title, height, collapsable, defaultOpen, className, formatTitle, disabled, idField, 
        headClass, objectType, onComment, formatLink, showVote, emptyText: propEmptyText, ...props }) {
    const classes = useStyles({ height });
    const [open, setOpen] = useState(defaultOpen);
    const { t } = useTranslation([ 'dashboard' ]);

    const emptyText = useMemo(
        () => propEmptyText ? propEmptyText : t("No Suggestions."),
        [t, propEmptyText]
    );

    const handleShow = useCallback(() => {
        setOpen(p => !p)
    }, [])

    return (<>
        <TableContainer component={Paper} className={clsx(classes.root, className)} {...props}>
            <Table size="small" stickyHeader>
                <TableHead>
                    <TableRow className={clsx(classes.headRow, {
                            [classes.clickable]: collapsable,
                        })} onClick={handleShow}>
                        <TableCell component="th" className={headClass}>
                            {collapsable && <IconButton size="small">
                                { open ? <KeyboardArrowDownIcon /> : <KeyboardArrowRightIcon /> }
                            </IconButton>}
                            {title}
                        </TableCell>
                        {showVote && <TableCell component="th" align="right" className={headClass}>{t("Votes")}</TableCell>}
                    </TableRow>
                </TableHead>
                <TableBody className={clsx({ [classes.hidden]: collapsable && !open})}>
                { data.length === 0 ?
                    <TableRow>
                        <TableCell colSpan={2}>
                            <em>{t(emptyText)}</em>
                        </TableCell>
                    </TableRow>
                :
                data.map((i, index) => (
                    <StyledTableRow key={index}>
                        <TableCell>
                            {formatLink ? 
                                <Link to={formatLink(i, idField)}>
                                    {formatTitle ? formatTitle(i) : i.title}
                                </Link>
                                :
                                formatTitle ? formatTitle(i) : i.title
                            }
                        </TableCell>
                        {showVote &&
                            <TableCell align="right">
                                <Vote selfVote={i.vote} up={i.upvote_count} down={i.downvote_count} onChange={onVote(i[idField])} className={classes.vote} disabled={disabled} />
                            </TableCell>
                        }
                    </StyledTableRow>
                ))}
                </TableBody>
            </Table>
        </TableContainer>
    </>)
}

VotingTable.defaultProps = {
    height: '200px',
    collapsable: true,
    defaultOpen: false,
    disabled: false,
    idField: 'id',
    showVote: true,
}

export default VotingTable
