import { Button, IconButton, makeStyles, Paper, Typography } from '@material-ui/core'
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline'
import SpaceBarIcon from '@material-ui/icons/SpaceBar'
import clsx from "classnames"
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { preAddress } from '../../../App'
import { useLanguage } from '../../../i18n'
import { saveGoalJob } from '../../views/Goals/utils'
import { getIndustryLabel } from '../../views/helpers'
import GoalLogo from '../Logos/Goal'
import { getLocation } from './utils'

const useStyles = makeStyles({
    root: {
        cursor: "pointer",
        position: "relative",
        display: "flex",
        margin: "8px 8px 16px 8px",
        padding: 10
    },
    deleteIcon: {
        position: 'absolute',
        top: 4,
        right: 4
    },
    selected: {
        backgroundColor: "#CCC",
    },
    followButton: {
        position: "absolute",
        right: 10,
    },
    wrapped: {
        textOverflow: "ellipsis",
        overflow: "hidden",
        whiteSpace: "nowrap"
    }
})

function GoalCard({ goal, noActions, onRemoveJourney, className, selected: propSelected, showFollowButton = false, onKeyDown, onAddRequest, ...props }) {
    const { t } = useTranslation(['dashboard'])

    const rootRef = useRef(null)
    const systemLanguage = useLanguage()
    const classes = useStyles()
    const [isFocused, setIsFocused] = useState(false)

    useEffect(
        () => {
            if (propSelected && rootRef.current)
                rootRef.current.focus()
        },
        [propSelected]
    )

    const handleRemove = useCallback(
        (goal) => (e) => {
            e.preventDefault()
            e.stopPropagation()
            if (window.confirm(t("Are you sure you want to delete this journey?"))) {
                saveGoalJob(
                    goal,
                    false,
                    res => {
                        // if (res.skill_to_be_removed.length > 0) { // ask about related skills
                        onRemoveJourney([...res.skill_to_be_removed], goal)
                        // }
                    },
                    null,
                    null
                );
            }
        },
        [onRemoveJourney],
    )

    const handleFollow = useCallback(
        (e) => {
            if (e)
                e.preventDefault()
            if (onAddRequest)
                onAddRequest(goal)
        },
        [onAddRequest, goal]
    )

    const handleKeyDown = useCallback(
        e => {
            if (e.key === " " && showFollowButton && !goal.is_my_goal) {
                handleFollow()
            }
            if (onKeyDown) {
                onKeyDown(e)
            }
        },
        [handleFollow, onKeyDown, showFollowButton, goal]
    )

    return (
        <Paper
            {...props}
            className={clsx(classes.root, className, { [classes.selected]: isFocused && showFollowButton })}
            component={Button}
            href={`${preAddress}/${systemLanguage}/dashboard/goals/${goal.id}`}
            target="_blank"
            ref={rootRef}
            onFocus={() => setIsFocused(true)}
            onBlur={() => setIsFocused(false)}
            onKeyDown={handleKeyDown}
        >
            {!noActions &&
                (showFollowButton &&
                <div className={classes.followButton}>
                    {goal.is_my_goal ?
                        <Button
                            variant='outlined'
                            disabled
                        >
                            {t("Followed")}
                        </Button>
                        :

                        <Button
                            variant='outlined'
                            onClick={handleFollow}
                            color={isFocused ? "primary" : "default"}
                            startIcon={isFocused ? <SpaceBarIcon /> : null}
                        >
                            {t("Add")}
                        </Button>

                    }
                </div>
            )}
            {onRemoveJourney &&
                <IconButton
                    onClick={handleRemove(goal)}
                    size="small"
                    title={t("Remove journey")}
                    className={classes.deleteIcon}
                >
                    <RemoveCircleOutlineIcon />
                </IconButton>
            }
            <GoalLogo size="large" />
            <div style={{
                flexGrow: 1,
                marginLeft: 8
            }}
            >
                <Typography
                    title={goal['title']}
                    component={"div"}
                    style={{
                        width: 200,
                    }}
                    className={classes.wrapped}
                >
                    {goal['title']}
                </Typography>
                <Typography
                    variant="caption"
                    title={`${t("Industry, Location")}: ${t(getIndustryLabel(goal.industry))}${getLocation(goal['country'], goal['city']) ? `, ${getLocation(goal['country'], goal['city'])}` : `, ${t("General")}`}`}
                    component={"div"}
                    style={{
                        width: 225,
                    }}
                    className={classes.wrapped}
                >
                    <em>{t(getIndustryLabel(goal.industry))}</em>
                    {getLocation(goal['country'], goal['city']) &&
                        (
                            <>
                                <em>
                                    {", " + getLocation(goal['country'], goal['city'])}
                                </em>
                            </>
                        )
                    }
                </Typography>
                {goal.company &&
                    <Typography 
                        variant="caption" 
                        title={t("Company")}
                        component={"div"}
                        style={{
                            width: "70%",
                        }}
                        className={classes.wrapped}
                    >
                        {t("by")} {goal.company}
                    </Typography>
                }
            </div>
        </Paper>
    )
}

export default GoalCard