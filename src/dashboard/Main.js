import { Backdrop, Box, CircularProgress, Container, CssBaseline } from '@material-ui/core';
import React, { Suspense, useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';
import { useLanguage } from '../i18n';
import Explore from '../landing/components/explore/Explore';
import { UserContext } from '../landing/User';
import './MainStyles.css';
import { routes } from './routes';
import Footer from './views/Footer';
import Navbar from './views/Navbar';
import Overlays from './views/Overlays';
import useStyles from './views/styles/MainStyles';

function LoadingBox() {
    return <Box width="100%" textAlign="center">
        <CircularProgress />
    </Box>
}

function showContent(content, props) {
    return React.isValidElement(content) ?
        content
        :
        content(props)
}

function Main() {
    const classes = useStyles();
    const match = useRouteMatch();
    const user = useContext(UserContext);
    const [discoveryHistory, setDiscoveryHistory] = useState(null);
    const [exploreOpen, setExploreOpen] = useState(false);

    const { t } = useTranslation(['dashboard']);
    const language = useLanguage();
    const backdropRef = useRef(null);

    const handleDiscoveryNav = useCallback(
        nav => {
            setDiscoveryHistory(p => nav === null ? {} : { ...p, ...nav })
        },
        []
    );

    useEffect(() => {
        document.title = t('eDoer Educational Portal - Dashboard');
    }, [t]);

    const childProps = useMemo(
        () => ({
            user,
            userData: user.data,
            handleDiscoveryNav,
            discoveryHistory,
            basePath: '/edoer' + match.url,
        }),
        [user, handleDiscoveryNav, discoveryHistory]
    )

    const handleCloseBackdrop = useCallback(
        (e) => {
            if (e.target === backdropRef.current) {
                setExploreOpen(false);
            }
        },
        []
    )

    return (
        user.loading ?
            <Box m="auto" p={2} textAlign="center" minHeight='150px'>
                <CircularProgress />
            </Box>
            :
            <div style={{ height: "100%" }}>
                <CssBaseline />
                <Navbar basePath={match.url} userData={user.data} user={user} path={match.path} exploreOpen={exploreOpen} onExploreOpenChange={setExploreOpen} />
                <div style={{ position: "relative", height: "100%" }}>
                    <Backdrop
                        style={{
                            position: "absolute",
                            zIndex: 100,
                            justifyContent: 'flex-start',
                        }}
                        ref={backdropRef}
                        open={exploreOpen}
                        onClick={handleCloseBackdrop}
                        unmountOnExit
                    >
                        <Explore
                            open={exploreOpen}
                            defaultSelectedItem={0}
                            isDiscovery
                        />
                    </Backdrop>
                    <Container maxWidth="lg" className={classes.mainPanel}>
                        <div style={{ flex: "1 1 auto" }}>
                            {user.loggedIn ?
                                <Switch>
                                    <Redirect exact from={`${match.path}`} to={`${match.path}edu`} />
                                    {routes.filter(r => r.loginRequired || r.loginRequired === null).map(
                                        (route, index) =>
                                            <Route key={index} path={typeof route.path === "function" ? route.path(match.path) : route.path}>
                                                {route.suspense ?
                                                    <Suspense fallback={<LoadingBox />}>
                                                        {showContent(route.content, childProps)}
                                                    </Suspense>
                                                    :
                                                    showContent(route.content, childProps)
                                                }
                                            </Route>
                                    )}
                                </Switch>
                                :
                                <Switch>
                                    {routes.filter(r => !r.loginRequired).map(
                                        (route, index) =>
                                            <Route key={index} path={typeof route.path === "function" ? route.path(match.path) : route.path}>
                                                {route.suspense ?
                                                    <Suspense fallback={<LoadingBox />}>
                                                        {showContent(route.content, childProps)}
                                                    </Suspense>
                                                    :
                                                    showContent(route.content, childProps)
                                                }
                                            </Route>
                                    )}
                                    <Redirect to={`/${language}/`} />
                                </Switch>
                            }
                        </div>
                        <Footer />
                    </Container>
                </div>
                <Overlays user={user} />
            </div>
    )
}

export default Main
