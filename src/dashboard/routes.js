import React from 'react';

const SelfAssess = React.lazy(() => import('./views/Discovery/SelfAssess'));
const Profile = React.lazy(() => import('./views/Profile'));
const Goals = React.lazy(() => import('./views/Goals'));
const Skills = React.lazy(() => import('./views/Skills'));
const OerGroups = React.lazy(() => import('./views/OerGroups'));
const EducationalContent = React.lazy(() => import('./views/Education'));
const History = React.lazy(() => import('./views/History'));
const Journey = React.lazy(() => import('./views/Journey'));
const Assessment = React.lazy(() => import('./views/Assessment'));
const Discovery = React.lazy(() => import('./views/Discovery'));
const AddJob = React.lazy(() => import('./views/Discovery/Jobs/Add'));
const EditJob = React.lazy(() => import('./views/Discovery/Jobs/Edit'));
const AddSkill = React.lazy(() => import('./views/Discovery/Skills/Add'));
const EditSkill = React.lazy(() => import('./views/Discovery/Skills/Edit'));
const AddOerGroup = React.lazy(() => import('./views/Discovery/OerGroups/Add'));
const OerGroupsDiscovery = React.lazy(() => import('./views/Discovery/OerGroups'));
const EditOerGroups = React.lazy(() => import('./views/Discovery/OerGroups/Edit'));
const EditOer = React.lazy(() => import('./views/Discovery/Oers/Edit'));
const Oers = React.lazy(() => import('./views/Discovery/Oers'));
const MyAreas = React.lazy(() => import('./views/Discovery/MyAreas'));
const SkillsDiscovery = React.lazy(() => import('./views/Discovery/Skills'));
const AddTopic = React.lazy(() => import('./views/Discovery/Topics/Add'));
const EditTopic = React.lazy(() => import('./views/Discovery/Topics/Edit'));
const TopicsDiscovery = React.lazy(() => import('./views/Discovery/Topics'));
const Topics = React.lazy(() => import('./views/Topics'));
const Jobs = React.lazy(() => import('./views/Discovery/Jobs'));
const Questions = React.lazy(() => import('./views/Discovery/Questions'));
const AddQuestion = React.lazy(() => import('./views/Discovery/Questions/Add'));
const LogoutComponent = React.lazy(() => import('./components/LogoutComponent'));
const MentoringRequests = React.lazy(() => import('./views/Mentoring'));
const PublicSelfAssessment = React.lazy(() => import('../landing/components/selfassessment/PublicSelfAssessment'))

// {
//     loginRequired: [Boolean|null for both],
//     path: [string|(mathedPath) => {}],
//     content: [React Node|(props) => {}],
//     suspense: [boolean],
// }

export const routes = [
    {
        loginRequired: true,
        path: p => `${p}logout`,
        content: <LogoutComponent />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}profile/:section?`,
        content: props => <Profile user={props.user} userData={props.user.data} />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}edu/`,
        content: props => <EducationalContent user={props.user} userData={props.userData} />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}history/:tab?`,
        content: props => <History />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}my-journeys/`,
        content: props => <Journey />,
        suspense: true,
    },    
    {
        loginRequired: true,
        path: p => `${p}assess/`,
        content: <Assessment />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}discovery/jobs/edit/:id(\\d+)`,
        content: props => <EditJob navHistory={props.discoveryHistory} />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}discovery/jobs/add`,
        content: props => <AddJob navHistory={props.discoveryHistory} />,
        suspense: true,
    },
    {
        loginRequired: null,
        path: p => `${p}discovery/jobs/:id(\\d+)`,
        content: props => <Jobs navHistory={props.discoveryHistory} onNav={props.handleDiscoveryNav} requireLogin={!props.user.loggedIn} />,
        suspense: true,
    },
    {
        loginRequired: null,
        path: p => `${p}goals/:id(\\d+)`,
        content: props => <Goals userData={props.userData} basePath={props.basePath} user={props.user} navHistory={props.discoveryHistory} onNav={props.handleDiscoveryNav} />,
        suspense: true,
    },
    {
        loginRequired: null,
        path: p => `${p}skills/:id(\\d+)`,
        content: props => <Skills userData={props.userData} basePath={props.basePath} user={props.user} navHistory={props.discoveryHistory} onNav={props.handleDiscoveryNav} />,
        suspense: true,
    },
    {
        loginRequired: null,
        path: p => `${p}topics/:id(\\d+)`,
        content: props => <Topics userData={props.userData} basePath={props.basePath} user={props.user} navHistory={props.discoveryHistory} onNav={props.handleDiscoveryNav} />,
        suspense: true,
    },
    {
        loginRequired: null,
        path: p => `${p}oer-groups/:id(\\d+)`,
        content: props => <OerGroups userData={props.userData} basePath={props.basePath} user={props.user} navHistory={props.discoveryHistory} onNav={props.handleDiscoveryNav} />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}mentoring-requests`,
        content: props => <MentoringRequests userData={props.userData} user={props.user} />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}self-assessment-reports/:id(\\d+)`,
        content: props => <PublicSelfAssessment inDashboard/>,
        suspense: true,
    },    
    {
        loginRequired: true,
        path: p => `${p}discovery/skills/edit/:id(\\d+)`,
        content: props => <EditSkill navHistory={props.discoveryHistory} />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}discovery/skills/add`,
        content: props => <AddSkill navHistory={props.discoveryHistory} />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}discovery/skills/:id(\\d+)/self-assessment`,
        content: props => <SelfAssess navHistory={props.discoveryHistory} onNav={props.handleDiscoveryNav} 
            user={props.user} />,
        suspense: true,
    },
    {
        loginRequired: null,
        path: p => `${p}discovery/skills/:id(\\d+)`,
        content: props => <SkillsDiscovery navHistory={props.discoveryHistory} onNav={props.handleDiscoveryNav} 
            requireLogin={!props.user.loggedIn} user={props.user} />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}discovery/topics/edit/:id(\\d+)`,
        content: props => <EditTopic navHistory={props.discoveryHistory} />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}discovery/topics/add`,
        content: props => <AddTopic navHistory={props.discoveryHistory} onNav={props.handleDiscoveryNav} />,
        suspense: true,
    },
    {
        loginRequired: null,
        path: p => `${p}discovery/topics/:id(\\d+)`,
        content: props => <TopicsDiscovery navHistory={props.discoveryHistory} onNav={props.handleDiscoveryNav} requireLogin={!props.user.loggedIn} />,
        suspense: true,
    },
    {
        loginRequired: null,
        path: p => `${p}discovery/oer-groups/:id(\\d+)`,
        content: props => <OerGroupsDiscovery navHistory={props.discoveryHistory} onNav={props.handleDiscoveryNav} requireLogin={!props.user.loggedIn} />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}discovery/oer-groups/edit/:id(\\d+)`,
        content: props => <EditOerGroups navHistory={props.discoveryHistory} />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}discovery/oer-groups/add`,
        content: <AddOerGroup />,
        suspense: true,
    },
    {
        loginRequired: null,
        path: p => `${p}discovery/oers/:id(\\d+)`,
        content: props => <Oers navHistory={props.discoveryHistory} onNav={props.handleDiscoveryNav}  />,
        suspense: true,
    },
    {
        loginRequired: null,
        path: p => `${p}discovery/oers/edit/:id(\\d+)`,
        content: props => <EditOer navHistory={props.discoveryHistory} />,
        suspense: true,
    },
    {
        loginRequired: null,
        path: p => `${p}my-area`,
        content: <MyAreas />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}discovery/questions/add`,
        content: props => <AddQuestion />,
        suspense: true,
    },
    {
        loginRequired: true,
        path: p => `${p}discovery/questions/:id(\\d+)`,
        content: props => <Questions />,
        suspense: true,
    },
    {
        loginRequired: null,
        path: p => `${p}discovery/:section?`,
        content: props => <Discovery onNav={props.handleDiscoveryNav} />,
        suspense: true,
    },
]
