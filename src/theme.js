import { createTheme } from "@material-ui/core";

const theme = createTheme({
    overrides: {
        MuiCssBaseline: {
            '@global': {
                a: {
                    textDecoration: "none",
                },
            },
        },
    },
    palette: {
        primary: {
            main: '#00006E',
        },
        secondary: {
            main: '#00F0F0',
        },
        common: {
            darkBlack: "rgb(36, 40, 44)",
        }
    },
    typography: {
        fontFamily: '"IBM Plex Mono", "Roboto", "Helvetica", "Arial", sans-serif',
    }
})

export default theme
