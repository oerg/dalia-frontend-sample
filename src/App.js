import {CircularProgress, CssBaseline, makeStyles, MuiThemeProvider, Slide, Snackbar} from '@material-ui/core';
import {Alert} from '@material-ui/lab';
import axios from 'axios';
import {SnackbarProvider} from 'notistack';
import React, {Suspense, useCallback, useEffect, useMemo, useState} from 'react';
import {useTranslation} from 'react-i18next';
import {Route, Switch, useRouteMatch} from 'react-router-dom';
import SearchProvider from './dashboard/views/Search/SearchProvider';
import {detectLanguage} from './i18n';
import UserProvider from './landing/User';
import {backend_base_url} from './settings';
import theme from './theme';
import Tracker from './Tracker';

const Dashboard = React.lazy(() => import('./dashboard/Main'));
const Landing = React.lazy(() => import('./landing/Main'));
const PublicProfile = React.lazy(() => import('./landing/components/user'))
const PublicSelfAssessment = React.lazy(() => import('./landing/components/selfassessment/PublicSelfAssessment'))
const UserActivator = React.lazy(() => import('./landing/components/user_activator'))
const PasswordReseter = React.lazy(() => import('./landing/components/password_reseter'))
export const preAddress = process.env.REACT_APP_PRE_ADDRESS

// set defaults
axios.defaults.baseURL = backend_base_url;


const useStyles = makeStyles(
    {
        snackRoot: {
            marginTop: 75,
        }
    }
)

function App() {
    const {i18n} = useTranslation();
    const match = useRouteMatch("/:lang(\\w{2})?");
    const classes = useStyles();
    const [width, setWidth] = useState(() => window.innerWidth)

    const handleWindowSizeChange = useCallback(
        () => {
            setWidth(window.innerWidth);
        },
        []
    )

    const isTooSmall = useMemo(
        () => width < 768,
        [width]
    )

    useEffect(() => {
        window.addEventListener('resize', handleWindowSizeChange);
        return () => {
            window.removeEventListener('resize', handleWindowSizeChange);
        }
    }, []);

    useEffect(
        () => {
            detectLanguage(i18n, match)
        },
        [match.params.lang]
    )

    return (
        <MuiThemeProvider theme={theme}>
            <Tracker/>
            <SnackbarProvider
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                TransitionComponent={Slide}
                TransitionProps={{
                    directioin: "right"
                }}
                maxSnack={5}
                preventDuplicate
                classes={{
                    root: classes.snackRoot
                }}

            >
                <CssBaseline/>
                <UserProvider>
                    <SearchProvider>
                        <Switch>
                            <Route path='/:lang(\w{2})?/dashboard/'>
                                {isTooSmall &&
                                    <Snackbar open>
                                        <Alert severity="warning" variant="filled">
                                            "Currently, for better experience of eDoer, please use it on laptops and
                                            other monitor screens"
                                        </Alert>
                                    </Snackbar>
                                }
                                <Suspense fallback={<CircularProgress/>}>
                                    <Dashboard/>
                                </Suspense>
                            </Route>
                            <Route exact path='/:lang(\w{2})?/activate'>
                                <Suspense fallback={<CircularProgress/>}>
                                    <UserActivator location={{
                                        pathname: '/',
                                        state: {
                                            login: true
                                        }
                                    }}/>
                                </Suspense>
                            </Route>
                            <Route exact path='/:lang(\w{2})?/forgot-password'>
                                <Suspense fallback={<CircularProgress/>}>
                                    <PasswordReseter location={{
                                        pathname: '/',
                                        state: {
                                            login: true
                                        }
                                    }}/>
                                </Suspense>
                            </Route>
                            <Route path='/:lang(\w{2})?/users/:id(\d+)'>
                                <Suspense fallback={<CircularProgress/>}>
                                    <PublicProfile/>
                                </Suspense>
                            </Route>
                            <Route path='/:lang(\w{2})?/self-assessment-reports/:id(\d+)/:token(.+)'>
                                <Suspense fallback={<CircularProgress/>}>
                                    <PublicSelfAssessment/>
                                </Suspense>
                            </Route>
                            <Route path='/:lang(\w{2})?/'>
                                {isTooSmall &&
                                    <Snackbar open>
                                        <Alert severity="warning" variant="filled">
                                            "Currently, for better experience of eDoer, please use it on laptops and
                                            other monitor screens"
                                        </Alert>
                                    </Snackbar>
                                }
                                <Suspense fallback={<CircularProgress/>}>
                                    <Landing/>
                                </Suspense>
                            </Route>
                        </Switch>
                    </SearchProvider>
                </UserProvider>
            </SnackbarProvider>
        </MuiThemeProvider>
    );
}

export default App;
